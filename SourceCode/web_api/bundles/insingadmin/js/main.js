$(document).ready(function(){
	$("#preview_template").colorbox({iframe:true, width:"1200px", height:"650px"});
  
  $("#preview").colorbox({iframe:true, width:"1200px", height:"650px"});
  $(".upload_photo").colorbox({
    iframe:true,
    width:"600px",
    height:"300px"
  });
  
  $(".upload_photo").colorbox({
    iframe:true,
    width:"600px",
    height:"300px"
  });
  
  $('.use-this-photo').click('cbox_close', function(){
    parent.$.fn.colorbox.close();
    var uploaded_url = $('.uploaded_url').val();
    $('.uploaded_photo').val( uploaded_url );    
  });
  
  // Show Add Module box  
  $(".add_module_trigger").colorbox({
    inline:true,
    width:"400px",
    height:"220px"
  });
  
  // Get colorpicker
  /*
  $('.colorpicker_trigger').ColorPicker({
		onSubmit: function(hsb, hex, rgb, el) {
			$(el).val('#' + hex);
			$(el).ColorPickerHide();
		},
		onBeforeShow: function () {
			$(this).ColorPickerSetColor(this.value);
		}
	})
	.bind('keyup', function(){
		$(this).ColorPickerSetColor(this.value);
	});
  
  
  // Delete Module
  $('.delete_module').click(function(){
    $(this).parent().remove();
  });
  */
  
  // Show confirm box
  $(".delete_module").colorbox({
    inline:true,
    width:"400px",
    height:"150px"
  });
  
  $(".cancel_delete").click(function(){
    $.colorbox.close();
  });
  
  // Calculation for content population
  var popuLayoutH = $('.population-layout .bd').height();
  var popuLeftNum = $('.population-layout .left .mod').length;
  var popuRightNum = $('.population-layout .right .mod').length;
  
  $('.population-layout .left .mod').each(function(){
    var itemH = popuLayoutH/popuLeftNum - 1;
    $(this).css({'height': itemH, 'line-height':itemH + 'px'})
  });
  
  $('.population-layout .right .mod').each(function(){
    var itemH = popuLayoutH/popuRightNum - 1;
    $(this).css({'height': itemH, 'line-height':itemH + 'px'})
  });
  
  var popuLayoutHeadW = $('.population-layout .hd').width();
  var popuHeadNum = $('.population-layout .hd .mod').length;
  $('.population-layout .hd .mod').each(function(){
    var itemW = popuLayoutHeadW/popuHeadNum - 21;
    $(this).css({'width': itemW});
  });
  
  var popuLayoutFootW = $('.population-layout .ft').width();
  var popuFootNum = $('.population-layout .ft .mod').length;
  $('.population-layout .ft .mod').each(function(){
    var itemW = popuLayoutFootW/popuFootNum - 21;
    $(this).css({'width': itemW});
  });
  
  $('.edit_ranking').click(function(){
    $(this).parent().find('.ranking_entry').toggle();
  });
  
  $('.ranking_save').click(function(){
    var rankingValue = $(this).siblings('.ranking_value').val();
    if(rankingValue > 0){
      $(this).parent().parent().find('.ranking').text('Ranking ' + rankingValue);
      $(this).parent().toggle();
    }else{
      $(this).parent().toggle();
    }
  });
  
});

function applyFuntions() {  
}

$(window).load(function() {
	applyFuntions();
});

$(window).resize(function() {
	applyFuntions();
});