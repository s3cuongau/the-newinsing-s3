<?php
namespace API\ApiBundle\Listener;

use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use API\ApiBundle\Util\APIConstantCode;
use API\ApiBundle\Util\APIConstantMsg;

class TokenListener {
    const PERM_API_ACCESS = 'API ACCESS';
    const PERM_API_READ = 'API READ';

    protected $container;
    protected $logger;
    protected $session_object;

    /**
     * Constructor with container and logger
     */
    public function __construct($container = null, $logger = null) {
        $this->container = $container;
        $this->logger = $logger;
    }

    /**
     * Getter/Setter for container
     */
    public function getContainer() {
        return $this->container;
    }

    public function setContainer($container) {
        $this->container = $container;
    }

    /**
     * Getter/Setter for logger
     */
    public function getLogger() {
        return $this->logger;
    }

    public function setLogger($logger) {
        $this->logger = $logger;
    }

    /**
     * Event listener function for kernel.controller
     */
    public function onKernelController(FilterControllerEvent $event) {
        $controllers = $event->getController();

        // Get the first controller if it's array
        if (is_array($controllers)) {
            $controller = $controllers[0];
        } else {
            $controller = $controllers;
        }

        if (is_object($controller) && method_exists($controller, 'tokenAuthenticated') && $controller->tokenAuthenticated()) {
            // First, check the session_token validity
            $session_object = $this->checkSessionToken($controller);
            $this->logger->debug(sprintf("Session token %s is ok.", $session_object->getSessionToken()));
        }
    }

    protected function checkSessionToken($controller) {
        // Attempt to get 'session_token' from query. Then, from post if it's empty.
        $session_token = $controller->get('request')->get('session_token');

        if ($session_token) {
            $session_object = $this->getSessionTokenObject($session_token);

            if (($session_object) && $session_object->getExpiredAt()->getTimestamp() > time()) {
                return $session_object;
            } else {
                throw new \Exception('The session_token is invalid or has expired.', APIConstantCode::CODE_SESSION_TOKEN_TIMEOUT);
            }
        } else {
            throw new \Exception(APIConstantMsg::API_MSG_CODE_UNAUTHORIZED, APIConstantCode::CODE_UNAUTHORIZED);
        }
    }

    protected function checkSignature($controller, $secret) {
        // Attempt to get 'sig' from query, from post if it's empty and finally from json.
        $sig_to_check = $controller->get('request')->get('sig');
        $getsig = $controller->get('request')->get('getsig');
        if (!$sig_to_check) {
            // TODO: Add support for application/json
            if (strpos($controller->get('request')->headers->get('Content-Type'), 'application/json') !== false) {
                $params = $this->container->get('request')->getContent();
                $sig_to_check = (isset($params['sig']) ? $params['sig'] : false);

                $getsig = (isset($params['getsig']) ? $params['getsig'] : false);
            } else {
                $sig_to_check = false;
            }
        }

        if ($sig_to_check) {
            $sig_generated = '';

            // Get all the params
            // TODO: Add support for application/json
            if (strpos($controller->get('request')->headers->get('Content-Type'), 'application/json') !== false) {
                $params = $this->container->get('request')->getContent();

                $sig_generated = self::genSignatureJson($secret, $controller->get('request')->getPathInfo(), $params);
            } else {
                $params = array_merge($controller->get('request')->request->all(), $controller->get('request')->query->all());

                // Remove sig param
                unset($params['sig']);

                // Remove getsig param
                unset($params['getsig']);

                $sig_generated = $this->genSig($secret, $controller->get('request')->getPathInfo(), $params);
            }

            // EditStart - sang.nguyen - return sig if has param getsig = 1
            if ($getsig && $getsig == "1") {
                throw new \Exception("sig=" . $sig_generated, APIConstantCode::CODE_SUCCESSFUL);
            }
            // EditEnd
            echo $sig_to_check . "<br/>";
            echo $sig_generated;
            if ($sig_to_check === $sig_generated) {
                return true;
            } else {
                throw new \Exception('The sig is invalid.', APIConstantCode::CODE_FAILURE);
            }
        } else {
            throw new \Exception('A sig is needed to access this API.', APIConstantCode::CODE_UNAUTHORIZED);
        }
    }

    protected function getSessionTokenObject($session_token) {
        $cache = $this->container->get('insing.util.cache')->getDefault();
        $cache_key = md5(implode('-', array(__METHOD__, $session_token)));
        $session_object = $cache->getCache($cache_key);

        if (!$session_object) {
            $this->logger->debug("Session_token $session_token not found in cache. Querying database for it.");
            $session_object = $this->container->get('SesstionTokenTable')
            ->getSesstionToken($session_token);

            if ($session_object) {
                if ($cache->set($cache_key, $session_object, intval($this->container->getParameter('ttl_session_token')))) {
                    $this->logger->debug("Stored session_token $session_token into cache.");
                } else {
                    $this->logger->warn("Error storing session_token $session_token into cache. Error code = " . $cache->getResultCode());
                }
            }
        }

        return $session_object;
    }

    /**
     * Generate the signature:
     *  md5(<secret><uri with slashes><param1><value1><param2><value2>)
     */
    protected function genSig($secret, $uri, array $params) {
        // First, add the secret
        $pre_hash = $secret;

        // Then, add the uri without slashes
        $pre_hash .= preg_replace('/\//', '', $uri);

        // After that, add the key-value of params in ascending order of keys
        ksort($params);
        foreach ($params as $k => $v) {
            $pre_hash .= $k . urlencode($v);
        }
        $this->logger->debug("Pre hash sig = $pre_hash");

        // Lastly, do a MD5 hash and return
        $post_hash = md5($pre_hash);
        $this->logger->debug("Post hash sig = $post_hash");
        return $post_hash;
    }

    /**
     * Generate API Signature for Content-Type : application/json
     *
     * @author Tin Nguyen
     * @param $secret
     * @param $uri
     * @param string $params
     * @return string
     */
    private static function genSignatureJson($secret, $uri, $params = '') {
        $uri = preg_replace('/\//', '', $uri);

        $params = str_replace("\n", '', $params);
        $params = str_replace("\r", '', $params);

        $uri = $secret . $uri;

        return md5($uri . $params);
    }

    /**
     * Check user' full permission or not
     * @author Minh.Ton
     * @return boolean
     */
    public function hasFullPermision($ssoRoles = null) {
        $passed = false;
        if ($ssoRoles && substr_count($ssoRoles, self::PERM_API_ACCESS) > 0) {
            $passed = true;
        } else {
            $session_token = $this->container->get('request')->get('session_token');
            $this->session_object = $this->getSessionTokenObject($session_token);
            if (is_object($this->session_object) && substr_count($this->session_object->getRoles(), self::PERM_API_ACCESS) > 0) {
                $passed = true;
            }
        }
        return $passed;
    }

    /**
     * Check user' read permission or not
     * @author Minh.Ton
     * @return boolean
     */
    public function hasReadPermission($ssoRoles = null) {
        $passed = false;
        if ($ssoRoles && (substr_count($ssoRoles, self::PERM_API_READ) > 0 || substr_count($ssoRoles, self::PERM_API_ACCESS) > 0)) {
            $passed = true;
        } else {
            $session_token = $this->container->get('request')->get('session_token');
            $this->session_object = $this->getSessionTokenObject($session_token);
            if (is_object($this->session_object) && (substr_count($this->session_object->getRoles(), self::PERM_API_READ) > 0 || substr_count($this->session_object->getRoles(), self::PERM_API_ACCESS) > 0)) {
                $passed = true;
            }
        }
        return $passed;
    }
}