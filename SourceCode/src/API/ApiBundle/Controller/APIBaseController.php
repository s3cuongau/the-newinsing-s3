<?php

namespace API\ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller; 
use Symfony\Component\HttpFoundation\Request;
use    FOS\RestBundle\View\View; 
use FOS\RestBundle\Util\Codes;
use API\CoreBundle\Util\Helper as APIHelper;
use API\ApiBundle\Util\Constant;
use API\ApiBundle\Util\UserMessage;

/**
 * The base controller is used by all API Systems
 * 
 * @author Tung Co (improved by Trung Nguyen)
 */
class APIBaseController extends Controller
{
        
    /**
     * @var Route
     */
    protected $currentRoute = null; // store route object of this request
    
    /**
     * @var Cache
     */
    protected $cache = null; // store cache object

    protected $_countryCode = null;

    /**
     * Do common tasks (e.g valid signature, session token, ...)
     * check session token and sig
     * @author tri.van
     * @param  Request $request
     */
    public function preExecute(Request $request)
    {
          // get current route, fetch route's options then 
        $this->currentRoute = $this->get('router')->getRouteCollection()->get($request->get('_route'));
        $routeOptions = $this->currentRoute->getOptions();

        // check to see if this service is available 
        if ($routeOptions['disable']) {
            $this->get('insing.util.logger')->addInfoToFile('Call disabled api : ' . $request->get('_route'));
            throw new \Exception("Service unavailable", Codes::HTTP_SERVICE_UNAVAILABLE);  
        }

        $contentType = '';
        if (strpos($request->headers->get('Content-Type'), 'application/json') === 0) {
            $contentType = 'json';
            $params = json_decode($request->getContent(), true);
            if ( !empty($params) ) {
                $params = array_merge($params, $request->query->all());
            }
            else {
                $params = $request->query->all();
            }
        } 
        else {
            $params = ($request->getMethod() == 'POST') ? $request->request->all() : $request->query->all();
        }

        // check sig
        $this->get('insing.services.signature')
        ->checkSignature($params, $contentType, $request->getPathInfo(), $request->getContent());

        // check session token
        $this->checkSessionToken($params);

        // inject cache object
        if ($routeOptions['cache']) {
            $this->cache = $this->get('insing.util.cache')->getInstanceByName($routeOptions['cache_type']);
        }
    }

    /**
     * check session token
     * @param unknown $params
     * @throws \Exception
     * @return boolean
     * @author tri.van
     */
    protected function checkSessionToken($params) {
        if (!isset($params['session_token']) || $params['session_token'] == '') {
            throw new \Exception(UserMessage::ERROR_SESSION_TOKEN_INVALID_MSG, 
                    UserMessage::ERROR_SESSION_TOKEN_INVALID_CODE);
        }

        // get session token
        $token = $this->container->get('SesstionTokenTable')
        ->getSesstionToken($params['session_token']);

        if (is_null($token)) {
            throw new \Exception(UserMessage::ERROR_SESSION_TOKEN_INVALID_MSG, 
                    UserMessage::ERROR_SESSION_TOKEN_INVALID_CODE);
        }
        if ( isset($token['expired_at']) ) {        
            $createdAt = strtotime($token['expired_at']);
            if (time() > $createdAt) {
                throw new \Exception(UserMessage::ERROR_SESSION_TOKEN_TIME_OUT_MSG, 
                        UserMessage::ERROR_SESSION_TOKEN_INVALID_CODE);
            }
        }
        else {
            throw new \Exception(UserMessage::ERROR_SESSION_TOKEN_TIME_OUT_MSG,
                    UserMessage::ERROR_SESSION_TOKEN_INVALID_CODE);
        }
        return true;
    }

    /**
     * get Country Code
     * @return string (ex: Constant::COUNTRY_CODE_SINGAPORE or  
     * Constant::COUNTRY_CODE_MALAYSIA...)
     * @author tri.van
     */
    public function getCountry()
    {
        $xp = explode('/', $_SERVER['REQUEST_URI']);
        if(in_array('4.0', $xp)) {
            $version = '4.0';
            $countryCode = 'sg';
            foreach ($xp as $key=>$item){
                if($item == $version){
                    $countryCode = $xp[$key+1];
                }
            }
            switch ($countryCode){
                case Constant::COUNTRY_CODE_SINGAPORE_URL:
                    $this->_countryCode = Constant::COUNTRY_CODE_SINGAPORE;
                    break;
                case Constant::COUNTRY_CODE_MALAYSIA_URL:
                    $this->_countryCode = Constant::COUNTRY_CODE_MALAYSIA;
                    break;
/*                 case Constant::COUNTRY_CODE_AUSTRALIA_URL:
                    $this->_countryCode = Constant::COUNTRY_CODE_AUSTRALIA;
                    break; */
                default:
                    throw new \Exception("API does not support for country: ".$countryCode, Codes::HTTP_BAD_REQUEST);
                    break;
            }
        }
        return $this->_countryCode;
    }

    /**
     * Output result to clients
     * 
     * @author Tung.Co
     * @param  int   $code
     * @param  array $data
     * @return string
     */
    protected function output($code, $data)
    {
        if (!is_array($data)) {
            $data = array();
        }

        return View::create()->setStatusCode($code)->setData($data);
    }

    /**
     * @author Dat Dao <dat.dao@s3corp.com.vn>
     * @return \API\HgwV4Bundle\User\SgUser
     */
    public function getUserSg() {
        return $this->container->get("hgwr.user.sg");
    }
    
    /**
     * @author Dat Dao <dat.dao@s3corp.com.vn>
     * @return \API\HgwV4Bundle\User\MyUser
     */
    public function getUserMy() {
        return $this->container->get("hgwr.user.my");
    }
    
    /**
     * @author Dat Dao <dat.dao@s3corp.com.vn>
     * @return \API\HgwV4Bundle\Util\TableDBApi
     */
    public function getTableDBApiObject() {
        return $this->container->get("TableDBApi");
    }
    
    /**
     * @author Dat Dao <dat.dao@s3corp.com.vn>
     * @return \API\HgwV4Bundle\Util\BusinessApiUtils
     */
    public function getBusinessApiObject() {
        return $this->container->get("BusinessApiUtils");
    }
    
    /**
     * @author Dat Dao <dat.dao@s3corp.com.vn>
     * @return \API\HgwV4Bundle\Util\RnRApiUtils
     */
    public function getRnrApiObject() {
        return $this->container->get("RnRApiUtils");
    }
    
    /**
     * @author Dat Dao <dat.dao@s3corp.com.vn>
     * @param \API\HgwV4Bundle\Util\CdnService
     */
    public function getCdnServiceObject() {
        return $this->container->get('CdnService');
    }
    
    /**
     * @author Dat Dao <dat.dao@s3corp.com.vn>
     * @return \API\HgwV4Bundle\Util\GimmieWorld
     */
    private function getGimmieWorldObject() {
        return $this->container->get("insing.gimmieworld");
    }
    
}
