<?php

namespace API\ApiBundle\Util;

use API\ApiBundle\Util\UserMessage;

class Signature{
    
    private $secret;
    
    public function __construct($secret){
        $this->secret    = $secret;
    }
    
    /**
     * Check signature
     *
     * @author  Co Vu Thanh Tung
     * @param   string $params
     * @param   string $contentType
     * @param   array  $pathInfo
     * @return  string
     */
    public function checkSignature($params, $contentType, $pathInfo, $content)
    {

        if(!isset($params['sig']) || $params['sig'] == ''){
            throw new \Exception(UserMessage::ERROR_SIGNATURE_INCORRECT_MSG, UserMessage::ERROR_PARAMS_INVALID_CODE);
        }

        $getSig = false;
        if(isset($params['create-sig']) && $params['create-sig']) {
            $getSig = true;
        }

        $pathInfo = $this->secret . $pathInfo;
        $pathInfo = str_replace('/', '', $pathInfo);
        $param['sig'] = $params['sig'];
        unset($params['sig']);

        if($contentType == 'json'){
            $sig = $this->genSignatureJson($content, $pathInfo);
        } else {
            $sig = $this->genSignature($params, $pathInfo);
        }

        if($getSig) {
            echo 'Sig: <b>'.$sig.'</b>';die;
        }

        if( $param['sig'] != $sig ){
            throw new \Exception(UserMessage::ERROR_SIGNATURE_INCORRECT_MSG, UserMessage::ERROR_PARAMS_INVALID_CODE);
        }
    
        return true;
    }
    
    
    /**
     * Generate API Signature for Content-Type : application/json
     *
     * @author  Co Vu Thanh Tung
     * @param   string $params
     * @param   array  $pathInfo
     * @return  string
     */
    public function genSignatureJson($params, $pathInfo)
    {
        $params = str_replace("\n", '', $params);
        $params = str_replace("\r", '', $params);
    
        //echo md5($pathInfo . $params)."\n";
    
        return md5($pathInfo . $params);
    }
    
    /**
     * Generate API Signature
     *
     * @author  Co Vu Thanh Tung
     * @param   string $params
     * @param   array  $pathInfo
     * @return  string
     */
    public function genSignature($params, $pathInfo)
    {
        $result = $pathInfo;
    
        if(isset($params['sig'])){
            unset($params['sig']);
        }
        
        if(isset($params['create-sig'])){
            unset($params['create-sig']);
        }


        $result = $pathInfo;

        ksort($params);
        foreach ($params as $key => $value) {
            $result .= urlencode($key . $value);
        }

        return md5($result);

    }
    
}
