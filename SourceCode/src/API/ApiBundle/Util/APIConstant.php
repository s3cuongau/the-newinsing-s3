<?php
namespace API\ApiBundle\Util;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of APIConstant
 *
 * @author minh.ton
 */
class APIConstant
{
    const DATETIME_DB_FORMAT = 'Y-m-d H:i:s';
    const DEFAULT_PAGE = 1;
    const DEFAULT_PER_PAGE = 10;

}