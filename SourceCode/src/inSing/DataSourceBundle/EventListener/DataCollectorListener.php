<?php

namespace InSing\DataSourceBundle\EventListener;

use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * 
 * Collec timer data for each action
 * @author khuong.phan
 *
 */
class DataCollectorListener
{
    private $_startTime = null;
    private $_endTime = null;
    private $_action = null;
    private static $_container = null;
    private static $before = 1;
    private static $after = 1;
    
    public function __construct(ContainerInterface $container) {
      self::$_container = $container;
    }
    
    public function onKernelBeforeAction(FilterControllerEvent $event)
    {
        $startTime = microtime(true);
        $action = $event->getRequest()->get('_controller');
        
        $arrAction = explode(':', $action);
        unset($arrAction[0]);
        $action = implode('][', $arrAction);

        if (in_array(self::$_container->get('kernel')->getEnvironment(), array('frontend_dev', 'admin_dev'))) {            
            self::$_container->get('data_collector.timer')->setData(array($action => array('start' => $startTime)));
        }
    }
    
    public function onKernelAfterAction(FilterResponseEvent $event)
    {
        $endTime = microtime(true);
        $action = $event->getRequest()->get('_controller');
        
        $arrAction = explode(':', $action);
        unset($arrAction[0]);
        $action = implode('][', $arrAction);
                
        if (in_array(self::$_container->get('kernel')->getEnvironment(), array('frontend_dev', 'admin_dev'))) {
            self::$_container->get('data_collector.timer')->setData(array($action => array('end' => $endTime )));
        }
    }    
}