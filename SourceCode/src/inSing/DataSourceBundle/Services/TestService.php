<?php

namespace inSing\DataSourceBundle\Services;

/**
 * TestService
 */
use inSing\DataSourceBundle\Lib\Constant;
use inSing\DataSourceBundle\Lib\UtilHelper;


class TestService extends BaseService{

    protected $_repository_name = 'inSingDataSourceBundle:Content';

    public function getDataTestFromConfig()
    {
       $data = $this->_config; // param config
       return $data;
    }
    public function getDataTestFromDB()
    {
        $params = array(
            'search' => array(
                'channel' => Constant::FEATURED_CHANNEL
            )
        );
        $contentList = $this->_repository->getContentListByCondition($params);
        $stories = array();
        foreach ($contentList as $item) {
            $tmp = array();
            $tmp['title'] = $item->getTitle();
            $stories[] = $tmp;
        }
        return $stories;
    }
    public function getDataTestFromApi()
    {

        $id = 20019838;
        $cms = $this->_data_source->get('cms');

        $result = $cms->getCmsDetailsByArticleId($id);

        if (count($result)) {
            $details = $result;

            if ($details['status'] == UtilHelper::PUBLISH_STATUS) {
                $title = isset($details['title']) ? $details['title'] : '';

                $contentList =array(
                    'title'              => $title,
                );
                return $contentList;
            }
        }
        return NULL;
    }
}