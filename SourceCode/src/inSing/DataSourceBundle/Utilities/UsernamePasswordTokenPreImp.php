<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Minh Tong PreImplement from Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 * Add new field $userName , $userId
 */

namespace inSing\DataSourceBundle\Utilities ;

use Symfony\Component\Security\Core\Role\RoleInterface;
use Symfony\Component\Security\Core\Authentication\Token\AbstractToken ;
/**
 * UsernamePasswordToken implements a username and password token.
 *
 * @author Minh Tong
 */
class UsernamePasswordTokenPreImp extends AbstractToken
{
    private $credentials;
    private $providerKey;
    private $userName;
    private $userId;
    private $displayName;
    /**
     * Constructor.
     *
     * @param string          $user        The username (like a nickname, email address, etc.), or a UserInterface instance or an object implementing a __toString method.
     * @param string          $credentials This usually is the password of the user
     * @param string          $providerKey The provider key
     * @param RoleInterface[] $roles       An array of roles
     *
     * @throws \InvalidArgumentException
     */
    public function __construct($user,$userName,$userId,$credentials,$providerKey, array $roles = array())
    {
        parent::__construct($roles);
        
        if (empty($providerKey)) {
            throw new \InvalidArgumentException('$providerKey must not be empty.');
        }

        $this->setUser($user);
        $this->credentials = $credentials;
        $this->providerKey = $providerKey;
        $this->userName = $userName;
        $this->userId = $userId;
        $this->displayName = $user;
        parent::setAuthenticated(count($roles) > 0);
    }

    /**
     * {@inheritdoc}
     */
    public function setAuthenticated($isAuthenticated)
    {
        if ($isAuthenticated) {
            throw new \LogicException('Cannot set this token to trusted after instantiation.');
        }

        parent::setAuthenticated(false);
    }

    /**
     * Returns the user name.
     *
     * @return string The user name
     */
    public function getUsername()
    {
        return $this->userName;
    }

    /**
     * Returns the user id.
     *
     * @return string The user id
     */
    public function getUserId()
    {
        return $this->userId;
    }
    /**
     * Returns the display name.
     *
     * @return string The display name
     */
    public function getDisplayName()
    {
        return $this->displayName;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getCredentials()
    {
        return $this->credentials;
    }

    /**
     * Returns the provider key.
     *
     * @return string The provider key
     */
    public function getProviderKey()
    {
        return $this->providerKey;
    }
    
    /**
     * {@inheritdoc}
     */
    public function eraseCredentials()
    {
        parent::eraseCredentials();

        $this->credentials = null;
    }

    /**
     * {@inheritdoc}
     */
    public function serialize()
    {
        return serialize(array($this->credentials, $this->providerKey, $this->userId, $this->userName,$this->displayName, parent::serialize()));
    }

    /**
     * {@inheritdoc}
     */
    public function unserialize($serialized)
    {
        list($this->credentials, $this->providerKey, $this->userId, $this->userName, $this->displayName, $parentStr) = unserialize($serialized);
        parent::unserialize($parentStr);
    }
}
