<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Api
 *
 * @author BienTran
 * @copyright (c) 2012, SingTel Digital Media Pte Ltd. All Rights Reserved.
 */

namespace inSing\DataSourceBundle\Utilities;

use Symfony\Component\DependencyInjection\ContainerInterface;
use \SoapClient;

class ApiSoap {
    const SOAP_API_SESSION_TOKEN_CACHE_KEY = "SOAP_API_SESSION_TOKEN";
    
    const RESULT_CODE_USER_ALREADY_FOLLOWED = '618';
    
    protected $container;
    protected $urlAuthenticate;
    protected $apiUserID;
    protected $apiPassword;
    protected $cache;
    protected $apiSearchUser;

    /**
     * container to get parameter
     * @param ContainerInterface $container
     */
    public function __construct($container) {
        if ($container instanceof ContainerInterface) {
            $this->container = $container;
            $this->urlAuthenticate = $this->getParameter('api_url_authenticate');
            $this->apiUserID = $this->getParameter('api_client_id');
            $this->apiPassword = $this->getParameter('api_password');
            $this->apiSearchUser = $this->getParameter('api_url_user');
            $this->cache = $this->container->get('hgw.cache');
        } else {
            throw new \Exception('Variable container is not ContainerInterface instance.', 404, null);
        }
    }

    /**
     * author: bientran
     * Get parameter
     * @param string $key
     */
    private function getParameter($key) {
        return $this->container->getParameter($key);
    }

    /**
     * author: bientran
     * getSessionToken
     * @return string $sessionToken
     */
    public function getSessionToken() {
        $sessionToken = '';
        try {
            $sessionToken = $this->cache->fetch(self::SOAP_API_SESSION_TOKEN_CACHE_KEY);
            if (!$sessionToken) {
                $sessionToken = $this->apiLogin();
                $this->container->get('data_collector.api')->setDataUrls(array('soap' => "get Session Token = $sessionToken"));
            }
        } catch (\Exception $exc) {
            if ('dev' === $this->container->get('kernel')->getEnvironment()) {
                echo $exc->getMessage();
                echo $exc->getTraceAsString();
            }
        }

        return $sessionToken;
    }

    /**
     * author: bientran
     * @return TRUE/FASE if login success/failed
     */
    function apiLogin() {

        try {
            $client = $this->getApiFromUrl($this->urlAuthenticate);
            if ($client) {
                $login = $client->login(array('deviceIdentifier' => array('type' => '0', 'identifier' => '0'), 'clientId' => $this->apiUserID, 'password' => $this->apiPassword));
                if ($login) {
                    $login = get_object_vars($login);
                    $this->container->get('monolog.logger.soapapi')->info($this->urlAuthenticate, $login);
                    if ($login['resultCode'] == 200) {
                        $sessionToken = $login['sessionToken'];
                        $this->cache->save(self::SOAP_API_SESSION_TOKEN_CACHE_KEY, $sessionToken, 1200); // cache in 30 minutes*/
                        return $sessionToken;
                    }
                } else {
                  //  $this->container->get('monolog.logger.soapapi')->info($this->urlAuthenticate);
                } 
            }
        } catch (\Exception $exc) {
            if ('dev' === $this->container->get('kernel')->getEnvironment()) {
                echo $exc->getMessage();
                echo $exc->getTraceAsString();
            }
        }

        return '';
    }

    /**
     * author: bientran
     * @param string $url
     * @return SoapClient
     */
    public function getApiFromUrl($url, $trace = false) {
        try {
            if ('dev' === $this->container->get('kernel')->getEnvironment()) {
//                $this->container->get('monolog.logger.soapapi')->info($url);
//                  $this->container->get('data_collector.api')->setDataUrls(array('soap' => $url));
            }
            $api = new \SoapClient($url, array('trace' => $trace));
        } catch (\SoapFault $ex) {
            $this->container->get('monolog.logger.soapapi')->info("SOAP Fault: (faultcode: {$ex->faultcode}, faultstring: {$ex->faultstring})");
            return null;
        } catch (\Exception $exc) {
            if ('dev' === $this->container->get('kernel')->getEnvironment()) {
                echo $exc->getMessage();
            }
            return null;
        }

        return $api;
    }

    /**
     * Get users details from userId
     * @param array or string $userIds
     * @return array Users Details
     */
    public function getUsersDetailApi($userIds) {
        $results = array();
        try {

            $soapApi = $this->getApiFromUrl($this->apiSearchUser);
            if (!$soapApi) {
                return $results;
            }

            $token = $this->getSessionToken();

            $users = $soapApi->getUserDetails(array(
                'sessionToken'  => $token,
                'userId'        => $userIds
            ));

            if ($users->resultCode == 200) {
                if ($users->users && count($users->users) ) {
                    $results = $users->users->user;
                }
            } else {
                $this->container->get('monolog.logger.soapapi')->info(print_r($users, true));
            }            
            $this->container->get('data_collector.api')->setDataUrls(array('soap' => 'get User Details' . json_encode($userIds)));
            
        } catch (\SoapFault $ex) {
            $this->container->get('monolog.logger.soapapi')->info("SOAP Fault: (faultcode: {$ex->faultcode}, faultstring: {$ex->faultstring})");
            return $results;
        } catch (\Exception $exc) {
            if ('dev' === $this->container->get('kernel')->getEnvironment()) {
                $this->container->get('monolog.logger.soapapi')->info($exc->getMessage());
                echo $exc->getMessage();
                echo $exc->getTraceAsString();
            }
        }
        return $results;
    }

    /**
     * Get users profile from inSingUserName
     * @param String $inSingUserName inSing userName
     * @return Object userDetailsType
     * @author Dat Tran
     */
    public function getUserProfile($inSingUserName) {
        $results = array();
        try {
            $soapApi = $this->getApiFromUrl($this->apiSearchUser);
            if (!$soapApi) {
                return $results;
            }
            $token = $this->getSessionToken();


            $apiData = $soapApi->getUserProfile(array(
                'sessionToken' => $token,
                'insingUserName' => $inSingUserName));
            $results = get_object_vars($apiData);
            $this->container->get('monolog.logger.soapapi')->info($this->urlAuthenticate, $results);
            $this->container->get('data_collector.api')->setDataUrls(array('soap' => 'get User Profile'. $inSingUserName));
        } catch (\Exception $exc) {
            if ('dev' === $this->container->get('kernel')->getEnvironment()) {
                echo $exc->getMessage();
                echo $exc->getTraceAsString();
            }
        }
        return $results;
    }

    /**
     * Edit user profile
     *
     * @author Vu Tran
     * @param  array $userInfo
     * @return array
     */
    public function editUserProfile($userInfo) {
        $resultArray = array();
        try {
            $token = self::getSessionToken();

            $api = $this->getApiFromUrl($this->apiSearchUser);

            if (!$api) {
                return $resultArray;
            }

            $userInfo['sessionToken'] = $token;

            $resultArray = $api->EditUserProfile($userInfo);

            $resultArray = get_object_vars($resultArray);
            $this->container->get('monolog.logger.soapapi')->info($this->urlAuthenticate, $resultArray);
        } catch (\Exception $exc) {
            if ('dev' === $this->container->get('kernel')->getEnvironment()) {
                echo $exc->getMessage();
                echo $exc->getTraceAsString();
            }
            return $resultArray;
        }

        return $resultArray;
    }

    /**
     * Create bookmarks
     * @param int $userId
     * @param string $entityType
     * @param int $entityId
     * @param string $note
     * @author Tin Nguyen
     */
    public function bookmarkCreate($userId, $entityType, $entityId, $note = '') {
        try {
            $token = $this->getSessionToken();
            $api = $this->getApiFromUrl($this->apiSearchUser);
            if ($api && $token) {
                $response = $api->createBookmark(array(
                    'sessionToken' => $token,
                    'userId' => $userId,
                    'entityType' => $entityType,
                    'entityId' => $entityId,
                    'note' => $note,
                ));
                
                $this->container->get('monolog.logger.soapapi')->info($this->urlAuthenticate, get_object_vars($response));
                return $response;
            }
        } catch (\Exception $exc) {
            if ('dev' === $this->container->get('kernel')->getEnvironment()) {
                echo $exc->getMessage();
                echo $exc->getTraceAsString();
            }
        }
        return false;
    }

    /**
     * checkUserBookmarksItem
     * @param int $userId
     * @param string $entityType
     * @param int $entityId
     * @author BienTran
     */
    public function checkUserBookmarksItem($userId, $bookmarkType = 'business', $entityId = 0) {
        try {
            $token = $this->getSessionToken();
            $api = $this->getApiFromUrl($this->apiSearchUser);
            $params = array(
                'sessionToken' => $token,
                'userId' => $userId,
                'bookmarkType' => $bookmarkType,
            );
            if ($entityId) {
                $params['entityId'] = $entityId;
            }
            if ($api && $token) {
                $response = $api->getUserBookmarks($params);
                $this->container->get('monolog.logger.soapapi')->info($this->urlAuthenticate, get_object_vars($response));
                return $response;
            }
        } catch (\Exception $exc) {
            if ('dev' === $this->container->get('kernel')->getEnvironment()) {
                echo $exc->getMessage();
                echo $exc->getTraceAsString();
            }
        }
        return false;
    }

    /**
     * deleteUserBookmarks
     * @param int $bookmarkId
     * @author BienTran
     */
    public function deleteUserBookmarks($bookmarkId) {
        try {
            $token = $this->getSessionToken();
            $api = $this->getApiFromUrl($this->apiSearchUser);
            $params = array(
                'sessionToken' => $token,
                'bookmarkId' => $bookmarkId
            );

            if ($api && $token) {
                $response = $api->deleteBookmark($params);
                $this->container->get('monolog.logger.soapapi')->info($this->urlAuthenticate, get_object_vars($response));
                return $response;
            }
        } catch (\Exception $exc) {
            if ('dev' === $this->container->get('kernel')->getEnvironment()) {
                echo $exc->getMessage();
                echo $exc->getTraceAsString();
            }
        }
        return false;
    }
 
    /**
     * Get user bookmarks
     * @param int $userId
     * @param string $bookmarkType 
     * Optional type to filter. Accepted values are
     * as follows: article, business, event, movie, review, hotdeal, gallery, celebrity, product
     * (This  is  optional,  but  will  be  required  if entityId has a value)
     * 
     * @param int $entityId : The entity IDs to filter to. Optional.
     * @return boolean
     */
    public function getUserBookmarks($userId, $bookmarkType = 'business', $entityId = null) {
        try {
            $token = $this->getSessionToken();
            $api = $this->getApiFromUrl($this->apiSearchUser);
            $params = array(
                'sessionToken'  => $token,
                'userId'        => $userId,
            );
            
            if (!empty($bookmarkType)) {
                $params['bookmarkType'] = $bookmarkType;
            }
            
            if (!empty($entityId)) {
                $params['entityId'] = $entityId;
            }

            if ($api && $token) {
                $response = $api->getUserBookmarks($params);
                if (!empty($response) && $response->resultCode == '200' ) {
//                    $this->container->get('monolog.logger.soapapi')->info($this->urlAuthenticate, get_object_vars($response));
                    return $response->bookmarks->bookmark;
                } else {
                    return false;
                }
            }
        } catch (\Exception $exc) {
            if ('dev' === $this->container->get('kernel')->getEnvironment()) {
                echo $exc->getMessage();
                echo $exc->getTraceAsString();
            }
        }
        return false;
    }    
    
    /**
     * 
     * @param int $followerId: User ID of the user following.
     * @param int $followeeId: User ID of the user to be followed
     * @return string
     */
    public function followUser($followerId, $followeeId) {
        try {
            $token = $this->getSessionToken();
            $api = $this->getApiFromUrl($this->apiSearchUser);
            
            $oAuthToken = $this->container->get('request')->cookies->get('oauth_token');
            
            $params = array(
                'sessionToken'  => $token,
                'oAuthToken'    => $oAuthToken,
                'followerId'    => $followerId,
                'followeeId'    => $followeeId,
            );
            
            if ($api && $token) {
                $response = $api->followUser($params);
//                $this->container->get('monolog.logger.soapapi')->info($this->urlAuthenticate, get_object_vars($response));
                return $response;
            }
        } catch (\Exception $exc) {
            $this->container->get('monolog.logger.soapapi')->error('FollowUser:' . $exc->getMessage(), $params);
            if ('dev' === $this->container->get('kernel')->getEnvironment()) {
                echo $exc->getMessage();
                echo $exc->getTraceAsString();
            }
        }
        return false;
    }
    
    /**
     * Unfollow user
     * @param int $followerId: User ID of the user following.
     * @param int $followeeId: User ID of the user to be unfollowed
     * @return string
     */
    public function unFollowUser($followerId, $followeeId) {
        try {
            $token = $this->getSessionToken();
            $api = $this->getApiFromUrl($this->apiSearchUser);
            
            $oAuthToken = $this->container->get('request')->cookies->get('oauth_token');
            
            $params = array(
                'sessionToken'  => $token,
                'oAuthToken'    => $oAuthToken,
                'followerId'    => $followerId,
                'followeeId'    => $followeeId,
            );
            
            if ($api && $token) {
                $response = $api->unFollowUser($params);
//                $this->container->get('monolog.logger.soapapi')->info($this->urlAuthenticate, json_encode($response));
                return $response;
            }
        } catch (\Exception $exc) {
            $this->container->get('monolog.logger.soapapi')->error('unFollowUser:' . $exc->getMessage(), $params);
            if ('dev' === $this->container->get('kernel')->getEnvironment()) {
                echo $exc->getMessage();
                echo $exc->getTraceAsString();
            }
        }
        return false;
    }
}
