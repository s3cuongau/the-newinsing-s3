<?php

namespace inSing\DataSourceBundle\Utilities\GimmieWorldSDK;

include_once('Gimmie_OAuth.php');
  
class Gimmie {
  
  private static $instance;
  private $gimmie_root = 'https://api.gimmieworld.com';
  
  public static function getInstance($key, $secret) {
    if (!self::$instance) {
      self::$instance = new Gimmie($key, $secret);
    }
    return self::$instance;
  }
  
  function __construct($key, $secret) {
    $this->key = $key;
    $this->secret = $secret;
  }
  
  public function set_user($user_id) {
    $this->user_id = $user_id;
  }

  public function categories() {
    $parameters = array(
    );
    return $this->invoke('categories', $parameters);
  }
  
  public function rewards($reward_id) {
    $parameters = array(
      'reward_id' => $reward_id
    );
    return $this->invoke('rewards', $parameters);
  }  
  
  public function profile() {
    $parameters = array(
    );
    return $this->invoke('profile', $parameters);
  }

  public function claims($claim_id) {
    $parameters = array(
      'claim_id' => $claim_id
    );
    return $this->invoke('claims', $parameters);
  }

  public function events($event_id = "") {
    $parameters = array(
    );
    
    if ($event_id != "")
    {
      $parameters = array_merge("event_id", $event_id);
    }
    
    return $this->invoke('events', $parameters);
  }
  
  public function badges($progress = 0) {
    $parameters = array(
      'progress' => $progress
    );
    return $this->invoke('badges', $parameters);
  }  

  public function trigger($event_name, $source_uid = "", $params = "") {
    $parameters = array(
      'event_name' => $event_name,
      'source_uid' => $source_uid
    );
    
    if ($params != "")
    {
      parse_str($params, $additional_params);

      if (is_array($additional_params))
      {
        $parameters = array_merge($parameters, $additional_params);
      }      
    }

    return $this->invoke('trigger', $parameters);
  }

  public function check_in($mayorship_id, $venue) {
    $parameters = array(
      'venue' => $venue
      
    );
    return $this->invoke('check_in/'.$mayorship_id, $parameters);
  }

  public function redeem($reward_id) {
    $parameters = array(
      'reward_id' => $reward_id
    );
    return $this->invoke('redeem', $parameters);
  }

  public function gift($reward_id) {
    $parameters = array(
      'reward_id' => $reward_id
    );
    return $this->invoke('gift', $parameters);
  }

  public function top20points($this_game = 0) {
    $parameters = array(
      'this_game' => $this_game
    );
    return $this->invoke('top20points', $parameters);
  }

  public function top20prices($this_game = 0) {
    $parameters = array(
      'this_game' => $this_game      
    );
    return $this->invoke('top20prices', $parameters);
  }

  public function top20redemptions_count($this_game = 0) {
    $parameters = array(
      'this_game' => $this_game        
    );
    return $this->invoke('top20redemptions_count', $parameters);
  }

  public function top20points_past_7_days($this_game = 0) {
    $parameters = array(
      'this_game' => $this_game  
    );
    return $this->invoke('top20points/past_7_days', $parameters);
  }
  
  public function top20prices_past_7_days($this_game = 0) {
    $parameters = array(
      'this_game' => $this_game  
    );
    return $this->invoke('top20prices/past_7_days', $parameters);
  }  
  
  public function top20redemptions_count_past_7_days($this_game = 0) {
    $parameters = array(
      'this_game' => $this_game  
    );
    return $this->invoke('top20redemptions_count/past_7_days', $parameters);
  }  

  public function top20points_past_week($this_game = 0) {
    $parameters = array(
      'this_game' => $this_game  
    );
    return $this->invoke('top20points/past_week', $parameters);
  }
  
  public function top20prices_past_week($this_game = 0) {
    $parameters = array(
      'this_game' => $this_game  
    );
    return $this->invoke('top20prices/past_week', $parameters);
  }  
  
  public function top20redemptions_count_past_week($this_game = 0) {
    $parameters = array(
      'this_game' => $this_game  
    );
    return $this->invoke('top20redemptions_count/past_week', $parameters);
  }
  
  public function top20points_this_week($this_game = 0) {
    $parameters = array(
      'this_game' => $this_game  
    );
    return $this->invoke('top20points/this_week', $parameters);
  }
  
  public function top20prices_this_week($this_game = 0) {
    $parameters = array(
      'this_game' => $this_game  
    );
    return $this->invoke('top20prices/this_week', $parameters);
  }  
  
  public function top20redemptions_count_this_week($this_game = 0) {
    $parameters = array(
      'this_game' => $this_game  
    );
    return $this->invoke('top20redemptions_count/this_week', $parameters);
  }  
  
  public function top20points_today($this_game = 0) {
    $parameters = array(
      'this_game' => $this_game  
    );
    return $this->invoke('top20points/today', $parameters);
  }
  
  public function top20prices_today($this_game = 0) {
    $parameters = array(
      'this_game' => $this_game  
    );
    return $this->invoke('top20prices/today', $parameters);
  }  
  
  public function top20redemptions_count_today($this_game = 0) {
    $parameters = array(
      'this_game' => $this_game  
    );
    return $this->invoke('top20redemptions_count/today', $parameters);
  }  
  
  public function top20points_past_30_days($this_game = 0) {
    $parameters = array(
      'this_game' => $this_game  
    );
    return $this->invoke('top20points/past_30_days', $parameters);
  }
  
  public function top20prices_past_30_days($this_game = 0) {
    $parameters = array(
      'this_game' => $this_game  
    );
    return $this->invoke('top20prices/past_30_days', $parameters);
  }  
  
  public function top20redemptions_count_past_30_days($this_game = 0) {
    $parameters = array(
      'this_game' => $this_game  
    );
    return $this->invoke('top20redemptions_count/past_30_days', $parameters);
  }  
  
  public function top20points_past_month($this_game = 0) {
    $parameters = array(
      'this_game' => $this_game  
    );
    return $this->invoke('top20points/past_month', $parameters);
  }
  
  public function top20prices_past_month($this_game = 0) {
    $parameters = array(
      'this_game' => $this_game  
    );
    return $this->invoke('top20prices/past_month', $parameters);
  }  
  
  public function top20redemptions_count_past_month($this_game = 0) {
    $parameters = array(
      'this_game' => $this_game  
    );
    return $this->invoke('top20redemptions_count/past_month', $parameters);
  } 
  
  public function top20points_this_month($this_game = 0) {
    $parameters = array(
      'this_game' => $this_game  
    );
    return $this->invoke('top20points/this_month', $parameters);
  }
  
  public function top20prices_this_month($this_game = 0) {
    $parameters = array(
      'this_game' => $this_game  
    );
    return $this->invoke('top20prices/this_month', $parameters);
  }  
  
  public function top20redemptions_count_this_month($this_game = 0) {
    $parameters = array(
      'this_game' => $this_game  
    );
    return $this->invoke('top20redemptions_count/this_month', $parameters);
  }   

  public function change_points($change, $description = "") {
    $parameters = array(
      'points' => $change,
      'description' => $description
    );
    return $this->invoke('change_points', $parameters);
  }
  
  public function recent_activites() {
    $parameters = array(
    );
    return $this->invoke('recent_activities', $parameters);
  }

  public function login($old_uid = "", $country = "", $name = "", $email = "") {
    
    $parameters = array(
      'old_uid' => $old_uid,
      'country' => $country,
      'name' => $name,
      'email' => $email
    );
    return $this->invoke('login', $parameters);
  }

  public function around_points($country = "", $n = "") {
    
    $parameters = array(
    );
        
    if ($country != "") {
      $parameters = array_merge($parameters, array("country" => $country));
    }
    
    if ($n != "") {
      $parameters = array_merge($parameters, array("n" => $n));
    }    
    
    return $this->invoke('around_points', $parameters);
  }

  private function fetch_web_data($url, $post_data = '', $keep_alive = false, $redirection_level = 0)
  {
  	static $keep_alive_dom = null, $keep_alive_fp = null;

  	preg_match('~^(http)(s)?://([^/:]+)(:(\d+))?(.+)$~', $url, $match);

  	if (empty($match[1]))
  		return false;

  	elseif (isset($match[1]) && $match[1] == 'http')
  	{
  		if ($keep_alive && $match[3] == $keep_alive_dom)
  			$fp = $keep_alive_fp;
  		if (empty($fp))
  		{
  			$fp = @fsockopen(($match[2] ? 'ssl://' : '') . $match[3], empty($match[5]) ? ($match[2] ? 443 : 80) : $match[5], $err, $err, 5);
  			if (!$fp)
  				return false;
  		}

  		if ($keep_alive)
  		{
  			$keep_alive_dom = $match[3];
  			$keep_alive_fp = $fp;
  		}

  		if (empty($post_data))
  		{
  			fwrite($fp, 'GET ' . $match[6] . ' HTTP/1.0' . "\r\n");
  			fwrite($fp, 'Host: ' . $match[3] . (empty($match[5]) ? ($match[2] ? ':443' : '') : ':' . $match[5]) . "\r\n");
  			fwrite($fp, 'User-Agent: PHP/SMF' . "\r\n");
  			if ($keep_alive)
  				fwrite($fp, 'Connection: Keep-Alive' . "\r\n\r\n");
  			else
  				fwrite($fp, 'Connection: close' . "\r\n\r\n");
  		}
  		else
  		{
  			fwrite($fp, 'POST ' . $match[6] . ' HTTP/1.0' . "\r\n");
  			fwrite($fp, 'Host: ' . $match[3] . (empty($match[5]) ? ($match[2] ? ':443' : '') : ':' . $match[5]) . "\r\n");
  			fwrite($fp, 'User-Agent: PHP/SMF' . "\r\n");
  			if ($keep_alive)
  				fwrite($fp, 'Connection: Keep-Alive' . "\r\n");
  			else
  				fwrite($fp, 'Connection: close' . "\r\n");
  			fwrite($fp, 'Content-Type: application/x-www-form-urlencoded' . "\r\n");
  			fwrite($fp, 'Content-Length: ' . strlen($post_data) . "\r\n\r\n");
  			fwrite($fp, $post_data);
  		}

  		$response = fgets($fp, 768);

  		if ($redirection_level < 3 && preg_match('~^HTTP/\S+\s+30[127]~i', $response) === 1)
  		{
  			$header = '';
  			$location = '';
  			while (!feof($fp) && trim($header = fgets($fp, 4096)) != '')
  				if (strpos($header, 'Location:') !== false)
  					$location = trim(substr($header, strpos($header, ':') + 1));

  			if (empty($location))
  				return false;
  			else
  			{
  				if (!$keep_alive)
  					fclose($fp);
  				return fetch_web_data($location, $post_data, $keep_alive, $redirection_level + 1);
  			}
  		}

  		elseif (preg_match('~^HTTP/\S+\s+20[01]~i', $response) === 0)
  			return false;

  		while (!feof($fp) && trim($header = fgets($fp, 4096)) != '')
  		{
  			if (preg_match('~content-length:\s*(\d+)~i', $header, $match) != 0)
  				$content_length = $match[1];
  			elseif (preg_match('~connection:\s*close~i', $header) != 0)
  			{
  				$keep_alive_dom = null;
  				$keep_alive = false;
  			}

  			continue;
  		}

  		$data = '';
  		if (isset($content_length))
  		{
  			while (!feof($fp) && strlen($data) < $content_length)
  				$data .= fread($fp, $content_length - strlen($data));
  		}
  		else
  		{
  			while (!feof($fp))
  				$data .= fread($fp, 4096);
  		}

  		if (!$keep_alive)
  			fclose($fp);
  	}
  	else
  	{
  		$data = false;
  	}

  	return $data;
  }
  
  private function invoke($action, $parameters) {
    // Don't run anything if user doesn't login
    if (!isset($this->user_id)) return;
  
    $gimmie_root = $this->gimmie_root;
    $endpoint = "$gimmie_root/1/$action.json?";
    foreach ($parameters as $name => $value) {
      $endpoint .= "$name=$value&";
    }
    $endpoint = rtrim($endpoint, '&');
    
    $key = $this->key;
    $secret = $this->secret;
    
    $access_token = $this->user_id;
    $access_token_secret = $secret;
    
    $sig_method = new \OAuthSignatureMethod_HMAC_SHA1();
    $consumer = new \OAuthConsumer($key, $secret, NULL);
    $token = new \OAuthConsumer($access_token, $access_token_secret);
    
    $acc_req = \OAuthRequest::from_consumer_and_token($consumer, $token, 'GET', $endpoint, $parameters);
    $acc_req->sign_request($sig_method, $consumer, $token);
    
    $json = $this->fetch_web_data($acc_req->to_url());
    
    $json_output = json_decode($json, TRUE);
    
    return $json_output;
  }
  
}
