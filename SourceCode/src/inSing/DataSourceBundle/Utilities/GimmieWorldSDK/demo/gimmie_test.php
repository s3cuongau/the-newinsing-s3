<pre>
<?php
 include_once('../Gimmie.sdk.php');

 define("GIMMIE_CONSUMER_KEY", "<CONSUMER KEY>"); //Get this from Gimmie portal after creating Game
 define("GIMMIE_SECRET_KEY", "<SECRET KEY>"); //Get this from Gimmie portal after creating Game

 $gimmie = new Gimmie(GIMMIE_CONSUMER_KEY, GIMMIE_SECRET_KEY);
 $gimmie->set_user('demo_user');

 print_r($gimmie->categories());
?>
</pre>
