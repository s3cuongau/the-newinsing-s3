<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of HtmlCutting
 *
 * @author bien.tran
 */
namespace inSing\DataSourceBundle\Utilities;

class HtmlCutStringUseDom {
    
    private $end = '...';
    private $ext_str = '';
    private $tempDiv = null;
    private $charCount = 0;
    private $encoding = 'UTF-8';
    private $limit = 0;
    private $string = null;
    private $is_cut = false;
            
    function __construct($string, $limit, $end = '...', $ext = '') {
        
        if(empty($string) || !is_string($string))
        {
            return;
        }
        // create dom element using the html string
        $this->tempDiv = new \DOMDocument();
        libxml_use_internal_errors(true);
        $this->tempDiv->loadHTML(mb_convert_encoding('<div>' . $string . '</div>', 'HTML-ENTITIES', 'UTF-8'));
        libxml_clear_errors();
        // keep the characters count till now
        $this->charCount = 0;
        $this->encoding = 'UTF-8';
        $this->end = $end;
        $this->ext_str = $ext;
        $this->string = $string;
        // character limit need to check
        $this->limit = $limit;
    }

    function cut() {
        
        if(empty($this->string) || !is_string($this->string))
        {
            return '';
        }
        
        // create empty document to store new html
        $this->newDiv = new \DOMDocument();
        // cut the string by parsing through each element
        $this->searchEnd($this->tempDiv->documentElement, $this->newDiv);
        $new_html = $this->newDiv->saveHTML();
        $arrayReplace = array('<body><div>' => '', '</div></body>' => '');
        
        $new_html = trim(htmlspecialchars_decode(strtr($new_html, $arrayReplace)));
        $end_string = ($this->is_cut ? $this->end : '');
        if($end_string == '')
        {
            $end_string = $this->ext_str;
        }
        
        if(substr(trim($new_html), -4, 4) == '</p>')
        {
            $new_html = substr_replace($new_html, $end_string . '</p>', -4, 4);
        }
        else if(substr(trim($new_html), -10, 10) == '</p></div>')
        {
            $new_html = substr_replace($new_html, $end_string . '</p></div>', -10, 10);
        }
        else
        {
            $new_html = $new_html . $end_string;
        }
        
        return $new_html;
    }

    function deleteChildren($node) {
        while (isset($node->firstChild)) {
            $this->deleteChildren($node->firstChild);
            $node->removeChild($node->firstChild);
        }
    }

    function searchEnd($parseDiv, $newParent) {
        
        foreach ($parseDiv->childNodes as $ele) {
            // not text node
            if ($ele->nodeType != 3) {
                $newEle = $this->newDiv->importNode($ele, true);
                if (count($ele->childNodes) === 0) {
                    $newParent->appendChild($newEle);
                    continue;
                }
                $this->deleteChildren($newEle);
                $newParent->appendChild($newEle);
                $res = $this->searchEnd($ele, $newEle);
                if ($res)
                {
                    return $res;
                }
                else {
                    continue;
                }
            }

            // the limit of the char count reached
            if (mb_strlen($ele->nodeValue, $this->encoding) + $this->charCount >= $this->limit) {
                $this->is_cut = true;
                $newEle = $this->newDiv->importNode($ele);
                $newEle->nodeValue = substr($newEle->nodeValue, 0, $this->limit - $this->charCount);
                $spacepos = strrpos($newEle->nodeValue, ' ');
                if (isset($spacepos)) {
                    // ...and cut the text in this position
                    $newEle->nodeValue = substr($newEle->nodeValue, 0, $spacepos);
                }
                
                $newParent->appendChild($newEle);
                return true;
            }
            $newEle = $this->newDiv->importNode($ele);
            $newParent->appendChild($newEle);
            $this->charCount += mb_strlen($newEle->nodeValue, $this->encoding);
        }
        return false;
    }
}

