<?php

/*
 * This file is part of the prestaSitemapPlugin package.
 * (c) David Epely <depely@prestaconcept.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace inSing\DataSourceBundle\Utilities\Sitemap;


/**
 * Xml requirements for sitemap protocol
 * @see http://www.sitemaps.org/protocol.html
 *
 * @author depely
 */
abstract class XmlConstraint
{
    protected $limitItems = 49999;
    protected $limitBytes = 10000000;

    protected $limitItemsReached = false;
    protected $limitBytesReached = false;
    protected $countBytes = 0;
    protected $countItems = 0;


    public function __construct($limitUrls = 0, $limitBytes = 0)
    {
        if($limitUrls)
        {
            $this->limitItems = $limitUrls;
        }
        if($limitBytes)
        {
            $this->limitBytes = $limitBytes;
        }
    }

    /**
     * @return bool
     */
    public function isFull()
    {
        return $this->limitItemsReached || $this->limitBytesReached;
    }

    public function getLimitItems() {
      return $this->limitItems;
    }

    public function getLimitBytes() {
      return $this->limitBytes;
    }

    /**
     * Render full and valid xml
     */
    abstract public function toXml();
}
