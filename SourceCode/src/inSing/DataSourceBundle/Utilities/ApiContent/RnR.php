<?php

namespace inSing\DataSourceBundle\Utilities\ApiContent;

use inSing\DataSourceBundle\Utilities\ApiContent\ApiContentAbstract;
use inSing\DataSourceBundle\Utilities\RnRApiUtils;
use inSing\DataSourceBundle\Utilities\Common;
use inSing\DataSourceBundle\Utilities\Constant;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

    class RnR extends ApiContentAbstract {

    /**
     * @var Array
     */
    protected $result = array(
        'result_code' => 0,
        'details' => array(
            'item_id' => '',
            'review_image' => '',
            'restaurant_name' => '',
            'restaurant_url' => '',
            'review_url' => '',
            'review_title' => '',
            'reviewer_display_name' => '',
            'reviewer_profile_url' => '',
            'reviewer_profile_pic_url' => '',
            'review_vote' => '',
            'restaurant_id' => '',
            'number_of_review' => '',
        )
    );

    /**
     * @var String
     */
    private $routeIBLDetail;

    /**
     * @var String
     */
    private $routeReviewDetail;

    public function __construct() {
        parent::__construct();
        $this->routeIBLDetail = 'in_sing_hgwr_ibl';
        $this->routeReviewDetail = 'in_sing_hgwr_ibl_reviews_detail_page';
    }

    /**
     * Return api result
     *
     * @param Array $param Include itemId, transCodeName
     * @return Array
     */
    public function getContent($param) {
        if (!isset($param['item_id']) || empty($param['item_id']) ||
                !isset($param['module_type']) && !empty($param['module_type']) ) {
            throw new \Exception('Invalid variable');
        }
        $itemId = $param['item_id'];
        $countryCode = $this->controller->getSelectedCountry();
        $transCodeName = $this->getTransCodeName($param['module_type']);

        $apiResult = $this->container->get('rnr.api')->getSubmissionDetails($itemId, true);
        $apiResult = $this->filterByChannel($countryCode, $apiResult);

        $frontEndUrl = $this->container->getParameter('frontend_url');
        $frontEndUrl = trim($frontEndUrl[$this->controller->getSelectedCountry()], '/');
        $result = $this->result['details'];
        if ($apiResult) {
            $this->result['result_code'] = 200;
            $result['item_id'] = $apiResult['submission_id'];
            if (is_array($apiResult['photos']) && $apiResult['photos']) {
                $photo = array_pop($apiResult['photos']);
                $result['review_image'] = $this->container->get('common.utils')
                            ->generatePhotoFromUrl($photo['url'], $transCodeName);
            }
            if ($apiResult['item'] && is_array($apiResult['item'])) {
                $result['restaurant_name'] = $apiResult['item']['name'];

                $result['restaurant_url'] = $frontEndUrl . $this->container->get('router')->generate($this->routeIBLDetail, array(
                    'state' => $apiResult['item']['state'] != '' ? $apiResult['item']['state'] : 'n-a',
                    'slug' => $apiResult['item']['slug'] != '' ? $apiResult['item']['slug'] : 'n-a',
                ));

                $result['review_url'] = $frontEndUrl . $this->controller->generateUrl($this->routeReviewDetail, array(
                        'state' => $apiResult['item']['state'] != '' ? $apiResult['item']['state'] : 'n-a',
                        'slug' => $apiResult['item']['slug'] != '' ? $apiResult['item']['slug'] : 'n-a',
                        'hash_id' => Common::idToUrlId($apiResult['submission_id'])
                    ));

                $result['restaurant_id'] = $apiResult['item']['restaurant_id'];
            }
            $result['review_title'] = isset($apiResult['review']['title']) ? $apiResult['review']['title'] : '';

            $result['reviewer_profile_pic_url'] = $apiResult['user']['avatar_url'];

            if ($apiResult['user']['is_blogger'] == true) {
                $result['reviewer_display_name'] = "Blogger";
            }
            else if ($apiResult['user'] && is_array($apiResult['user'])) {
                $result['reviewer_display_name'] = $apiResult['user']['user_name'];
                //$result['reviewer_profile_url'] = $this->container->getParameter('url_user_profile_insing') . Common::convertToSlug($apiResult['user']['user_name']) . '/id-' . $apiResult['user']['user_id'];
                $result['reviewer_profile_url'] = $frontEndUrl . $this->controller->generateUrl('in_sing_hgwr_profile_home',
                        array('display_name' => Common::convertToSlug($apiResult['user']['user_name']),
                            'hex_id' => Common::idToUrlId($apiResult['user']['user_id']))
                        );
                $result['number_of_review'] = intval($apiResult['user']['user_review']);
            }
            if ($apiResult['vote'] && is_array($apiResult['vote'])) {
                if ($apiResult['vote']['thumbs_up'] == 1) {
                    $result['review_vote'] = 'Up';
                } else if ($apiResult['vote']['thumbs_up'] == 0) {
                    $result['review_vote'] = 'Down';
                }

            }

            $this->result['details'] = $result;
        }

        return $this->result;
    }

        /**
         * Return api result
         *
         * @param Array $param Include itemId, transCodeName
         * @return Array
         */
        public function getContentSubmission($param, $container) {
            if (!isset($param['item_id']) || empty($param['item_id']) ||
                !isset($param['module_type']) && !empty($param['module_type']) ) {
                throw new \Exception('Invalid variable');
            }
            $itemId = $param['item_id'];
            $countryCode = $param['country_code'];
            $transCodeName = $this->getTransCodeName($param['module_type']);

            $apiResult = $container->get('rnr.api')->getSubmissionDetails($itemId, true);

            //-----------
            $listChannel = $container->getParameter('rnr_channel');
            
            if ($apiResult && isset($listChannel[$countryCode]) && $channel = $listChannel[$countryCode]) {                
                if (isset($apiResult['item_type']) && $apiResult['item_type'] == $channel) {                    
                    $apiResult = $apiResult;
                } else {
                    $apiResult = array();
                }
            } else {
                $apiResult = array();
            }
            
            //-----------

            $frontEndUrl = $container->getParameter('frontend_url');
            $frontEndUrl = trim($frontEndUrl[$countryCode], '/');
            $result = $this->result['details'];

            if ($apiResult) {
                $this->result['result_code'] = 200;
                $result['item_id'] = $apiResult['submission_id'];
                if (is_array($apiResult['photos']) && $apiResult['photos']) {
                    $photo = array_pop($apiResult['photos']);
                    $result['review_image'] = $container->get('common.utils')
                        ->generatePhotoFromUrl($photo['url'], $transCodeName);
                }
                if ($apiResult['item'] && is_array($apiResult['item'])) {
                    $result['restaurant_name'] = isset($apiResult['item']['name']) ? $apiResult['item']['name'] : '';

                    $result['restaurant_url'] = $frontEndUrl . $container->get('router')->generate($this->routeIBLDetail, array(
                            'state' => $apiResult['item']['state'] != '' ? $apiResult['item']['state'] : 'n-a',
                            'slug' => (isset($apiResult['item']['slug']) && $apiResult['item']['slug'] != '') ? $apiResult['item']['slug'] : 'n-a',
                        ));

                    $result['review_url'] = $frontEndUrl . $container->get('router')->generate($this->routeReviewDetail, array(
                            'state' => $apiResult['item']['state'] != '' ? $apiResult['item']['state'] : 'n-a',
                            'slug' => (isset($apiResult['item']['slug']) && $apiResult['item']['slug'] != '') ? $apiResult['item']['slug'] : 'n-a',
                            'hash_id' => Common::idToUrlId($apiResult['submission_id'])
                        ));


                    $result['restaurant_id'] = isset($apiResult['item']['restaurant_id']) ? $apiResult['item']['restaurant_id'] : '';
                }
                $result['review_title'] = isset($apiResult['review']['title']) ? $apiResult['review']['title'] : '';

                if ($apiResult['user']['is_blogger'] == true) {
                    if ($countryCode == Constant::COUNTRY_CODE_SINGAPORE) {
                        $result['reviewer_profile_pic_url'] = $apiResult['user']['user_avatar'];
                    } else if ($countryCode == Constant::COUNTRY_CODE_MALAYSIA) {
                        $result['reviewer_profile_pic_url'] = $apiResult['user']['avatar_url'];
                    }
                    $result['reviewer_display_name'] = "Blogger";
                }
                else if ($apiResult['user'] && is_array($apiResult['user'])) {
                    $result['reviewer_display_name'] = $apiResult['user']['user_name'];
                    //$result['reviewer_profile_url'] = $this->container->getParameter('url_user_profile_insing') . Common::convertToSlug($apiResult['user']['user_name']) . '/id-' . $apiResult['user']['user_id'];
                    $result['reviewer_profile_url'] = $frontEndUrl . $container->get('router')->generate('in_sing_hgwr_profile_home',
                            array('display_name' => Common::convertToSlug($apiResult['user']['user_name']),
                                'hex_id' => Common::idToUrlId($apiResult['user']['user_id']))
                        );
                    if ($countryCode == Constant::COUNTRY_CODE_SINGAPORE) {
                        $result['reviewer_profile_pic_url'] = $apiResult['user']['user_avatar'];
                    } else if ($countryCode == Constant::COUNTRY_CODE_MALAYSIA) {
                        $result['reviewer_profile_pic_url'] = $apiResult['user']['avatar_url'];
                    }

                    $result['number_of_review'] = intval($apiResult['user']['user_review']);
                }
                if ($apiResult['vote'] && is_array($apiResult['vote'])) {
                    if ($apiResult['vote']['thumbs_up'] == 1) {
                        $result['review_vote'] = 'Up';
                    } else if ($apiResult['vote']['thumbs_up'] == 0) {
                        $result['review_vote'] = 'Down';
                    }

                }

                $this->result['details'] = $result;
            }

            return $this->result;
        }

    /**
     * Filter RnR by channel
     *
     * @param String $countryCode
     * @param String $channel
     * @param Array $data
     * @return Array
     */
    public function filterByChannel($countryCode, $data) {
        $result = array();
        $listChannel = $this->container->getParameter('rnr_channel');
        if ($data && isset($listChannel[$countryCode]) && $channel = $listChannel[$countryCode]) {
            if (isset($data['item_type']) && $data['item_type'] == $channel) {
                return $data;
            }
        }
        return $result;
    }
}


