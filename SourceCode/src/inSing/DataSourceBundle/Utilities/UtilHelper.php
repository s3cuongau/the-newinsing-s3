<?php

namespace inSing\DataSourceBundle\Utilities;

use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Authentication\Token\AnonymousToken;

class UtilHelper
{
    /**
     * Create unique string
     * @author An Lam
     * @param string $prefix
     * @return string
     */
    public static function createUniqueString($prefix = '')
    {
        $prefix .= rand(0,999999);

        return md5(uniqid($prefix, true));
    }
    /**
     * Convert from int to hash
     *
     * @author Vu Tran
     * @param int $id
     * @return hash id
     */
    public static function idToUrlId($id)
    {
        $paddedId = sprintf("%08s", dechex($id));
        $strArr = str_split($paddedId, 2);
        return implode(array_reverse($strArr));
    }

    /**
     * Convert from hash to int
     *
     * @author Vu Tran
     * @param has $urlId
     * @return int
     */
    public static function urlIdToId($urlId)
    {
        $strArr = str_split($urlId, 2);
        $paddedId = implode(array_reverse($strArr));
        return hexdec($paddedId);
    }

    /**
     * Generating slug
     * @author Tin Nguyen
     * @param $str
     * @param string $defaultTxt
     * @return string
     */
    public static function generateSlug($str, $defaultTxt = 'n-a')
    {
        if($str) {
            if (function_exists('iconv')) $str = iconv('UTF-8', 'ASCII//TRANSLIT//IGNORE', $str);
            $str = preg_replace("/\W+/", ' ', $str);
            $str = preg_replace("/\s\s+/", ' ', $str);
            $str = trim($str);
        }

        if( !$str ) return $defaultTxt;

        $str = str_replace(' ', '-', $str);
        $str = strtolower($str);

        return $str;
    }

    /**
     * Create slug (This function will be removed, please use generateSlug function instead)
     * @author Tin Nguyen
     */
    public static function slugify($str, $returnBlank = true)
    {
        if($str) {
            if (function_exists('iconv')) $str = iconv('UTF-8', 'ASCII//TRANSLIT//IGNORE', $str);
            $str = preg_replace("/\W+/", ' ', $str);
            $str = preg_replace("/\s\s+/", ' ', $str);
            $str = trim($str);
        }

        if( !$str ) return $returnBlank ? '' : 'n-a';

        $str = str_replace(' ', '-', $str);
        $str = strtolower($str);

        return $str;
    }

    /**
     * Get positive integer
     * @author Tin Nguyen
     * @param $val
     * @param int $defaultVal
     * @return int
     */
    public static function getPositiveInteger($val, $defaultVal = 1, $allowedVals = array())
    {
        if ($val && preg_match('/^\d+$/', $val)) {
            $val = intval($val);
            if ($allowedVals) {
                if (in_array($val, $allowedVals)) {
                    return $val;
                }
            } else {
                return $val;
            }
        }

        return $defaultVal;
    }

    /**
     * Get unsigned integer
     * @author Tin Nguyen
     * @param $val
     * @param int $defaultVal
     * @return int
     */
    public static function getUnsignedInt($val, $defaultVal = 0)
    {
        if (preg_match('/^[0-9]$/', $val)) {
            return intval($val);
        }

        return $defaultVal;
    }

    /**
     * Get wanted string
     * @author Tin Nguyen
     * @param $str
     * @param string $defaultStr
     * @param array $allowedStrs
     * @return string
     */
    public static function getWantedString($str, $defaultStr = '', $allowedStrs = array())
    {
        if ($str && in_array($str, $allowedStrs)) {
            return $str;
        }

        return $defaultStr;
    }

    /**
     * Get url path for generating API Sig
     * @author Tin Nguyen
     * @param string $url
     * @return string
     */
    public static function getPathInfoFormUrl($url = '')
    {
        $parseUrl = parse_url($url);
        $path = (isset($parseUrl['path'])) ? $parseUrl['path'] : $url;

        return $path;
    }

    public static function sortParams(&$params, $removeNullData = false)
    {
        if (isset($params) && is_array($params)) {
            foreach ($params as $key => &$param) {
                if($removeNullData && ($param === '' || $param === null)) {
                    unset($params[$key]);
                }

                self::sortParams($param);
            }
            ksort($params);
        }
    }

    /**
     * Generate Signature
     * @author Tin Nguyen
     * @param $secret
     * @param $url
     * @param array $params
     * @return string
     */
    public static function genSignature($secret, $url, $params = array(), $removeArrayIndex = false, $removeNullData = false)
    {
        $uri = preg_replace('/\//', '', self::getPathInfoFormUrl($url));
        $result = $secret . $uri;

        if($params) {
            self::sortParams($params, $removeNullData);

            $strParams = http_build_query($params, '', '');

            $result .= str_replace('=', '', $strParams);
        }

        if($removeArrayIndex) {
            $result = preg_replace('/\%5B\d+\%5D/', '%5B%5D', $result);
        }

        return md5($result);
    }

    /**
     * Get url path for generating API Sig
     * @author Tin Nguyen
     * @param string $url
     * @return string
     */
    public static function getUrlPathForGenSig($url = '')
    {
        $parseUrl = parse_url($url);
        $path = (isset($parseUrl['path'])) ? str_replace('app_api.php', '', $parseUrl['path']) : '';
        $path = preg_replace('/\//', '', $path);

        return $path;
    }

    /**
     * Safe json decode
     * @author Tin Nguyen
     * @param $str
     * @return array|mixed
     */
    public static function safeJsonDecode($str, $assoc = false)
    {
        $result = array();

        if($str = trim($str)) {
            $result = json_decode($str, $assoc);

            if($result === null || !is_array($result)) {
                $result = explode(',', $str);
            }
        }

        return $result;
    }

    /**
     * @param $secret
     * @param $uri
     * @param $params
     * @author Tin Nguyen
     * @return string
     */
    public static function genSignatureJson($secret, $uri, $params)
    {
        $uri = self::getUrlPathForGenSig($uri);
        $uri = preg_replace('/\//', '', $uri);
        $params = json_encode($params);
        $params = str_replace("\n", '', $params);
        $params = str_replace("\r", '', $params);

        $uri = $secret . $uri;

        return md5( $uri . $params );
    }

    /**
     * Get micro time in float
     *
     * @author Trung Nguyen
     * @return float
     */
    public static function getMicroTimeInFloat()
    {
        list($usec, $sec) = explode(" ", microtime());
        return ((float) $usec + (float) $sec);
    }

    /**
     * @author Tin Nguyen
     * @param array $data
     * @param string $field
     * @return array
     */
    public static function convertFieldToIndex($data = array(), $field = 'id')
    {
        $result = array();

        if(count($data)) {
            foreach($data as $item) {
                $result[$item[$field]] = $item;
            }
        }

        return $result;
    }

    /**
     * Truncate text (not use for rich text)
     * @author Tin Nguyen
     * @param $str
     * @param int $length
     * @param string $suffix
     * @return string
     */
    public static function truncateText($str, $length = 100, $suffix = '...')
    {
        $truncated = false;

        if(trim($str)) {
            $encoding = mb_detect_encoding($str);
            $strLength = mb_strlen($str, $encoding);
            if($strLength > $length) {
                $str = mb_substr($str, 0, $length, $encoding);
                $lastSpace = mb_strrpos($str, ' ', $encoding);
                $str = ($lastSpace) ? mb_substr($str, 0, $lastSpace, $encoding) : mb_substr($str, 0, (mb_strlen($str, $encoding) - mb_strlen(html_entity_decode($suffix), $encoding)), $encoding);
                $truncated = true;
            }
        }

        return ($truncated) ? $str . $suffix : $str;
    }

    /**
     * Generate media url
     * @uathor Tin Nguyen
     * @return string
     */
    public static function generateMediaUrl($container, $resourceId, $transcode = 'pc_143x107', $fileType = 'jpg')
    {
        $cdnPath = str_replace('{i}', rand(1, 5), $container->getParameter('insing_cdn_path'));
        $hex = self::idToUrlId($resourceId);
        $splitHex = str_split($hex, 2);

        return $cdnPath . '/' . implode('/', $splitHex) . '/' . $transcode . '.' . $fileType;
    }


    /**
     * Get module, return module by module name
     * @author Hung Dang
     * @param Array $modules
     * @param String $name
     * @return Array
     * */
    static public function getModule($modules, $name)
    {
        foreach ($modules as $module) {
            if($module['module']==$name){
                return $module;
            }
        }

        return array();
    }

    /*
     * author Minh Tong
     * generate List of Reviews template
     * updated by Dung Le
     */
    public static function getIBLReviewDetailList($container, $bussinessId , $loadmore = false , $perPage = null)
    {
        try{
            $trading_name = null ;
            $slug = null ;
            $page = 1 ;
            $truncate = 300 ;

            if (empty($perPage)) {
                $perPage = $container->getParameter("ibl_review_per_page_size") ;
            }

            if ($loadmore) {
                $truncate = 600 ;
            }
            $reviewsInfo = self::_getReviewDetails($container, $bussinessId, $page, $perPage, $truncate) ;

            $restaurant = $container->get('bizsearch.api')->getBusinessDetails(array($bussinessId));

            if (empty($restaurant)) {
                return array();
            }

            if (!empty($restaurant['slug'])) {
                $slug = $restaurant['slug'] ;
            }
            if (!empty($restaurant['trading_name'])) {
                $trading_name = $restaurant['trading_name'] ;
            }

            $data = array (
                'bussinessId' => $bussinessId,
                'reviews'     => $reviewsInfo['data'] ,
                'totalPage'   => ceil($reviewsInfo['total'] / $perPage),
                'restaurantName' => $trading_name,
                'slug'        => $slug,
                'loadmore' => $loadmore
            );
            return $data;
        } catch (\Exception $exc) {
            $logger = new HgwLogger($container, 'ibl_place_holder_reviews');
            $logger->exp_err($exc);
        }

        return null;
    }

    private static function _getReviewDetails($container, $bussinessId, $page, $perPage , $truncate) {
        $reviews = array('total'=> 0 , 'data' => null ) ;

        $submissionDatas = $container->get('rnr.api')->getSubmissionList($perPage ,$page , $bussinessId, 'review' , '' , true );

        if (!empty($submissionDatas) && !empty($submissionDatas['submission']) && count($submissionDatas['submission']) > 0) {
            foreach($submissionDatas['submission'] as $key => $submissionData) {

                $submissionData['photo_url'] = null ;
                if (!empty($submissionData['photos'])) {
                    $submissionData['photos'] = array_reverse($submissionData['photos']);
                    $submissionData['photo_url'] = $container->get('common.utils')->generatePhotoFromUrl($submissionData['photos'][0]['url'], '210x159');
                    $submissionData['photo_caption'] = $submissionData['photos'][0]['caption'] ;
                    $submissionData['photo_id'] = $submissionData['photos'][0]['photo_id'] ;
                }

                if (!empty($submissionData['review']['body'])) {
                    $body = $container->get('common.utils')->filterStyleAttributeFromHTML(strip_tags($submissionData['review']['body'], Constant::LIST_STRIPTAGS_HTML_REVIEW), Constant::LIST_STRIPTAGS_HTML_REVIEW);
                } else {
                    $body = $submissionData['review']['body'];
                }
                $submissionData['description'] = $body;
                $submissionData['boolTruncated'] = strlen($body) > strlen($submissionData['description']) ? 1 : 2;

                $avatar = $container->getParameter('default_user_avatar_url');
                if (isset($submissionData['user']['avatar_url']) && !empty($submissionData['user']['avatar_url'])) {
                    $avatar = $container->get('common.utils')->generatePhotoFromUrl($submissionData['user']['avatar_url'], '90x90') ;
                } elseif (isset($submissionData['user']['avatar_id'])) {
                    $avatar = $submissionData['user']['avatar_id'] ? $container->get('common.utils')->generateMediaUrl($submissionData['user']['avatar_id'], 'pc_90x90') : $avatar;
                }
                $submissionData['user_avata'] =  $avatar ;

                $reviews['data'][] = $submissionData ;
            }
            $reviews['total'] = $submissionDatas['total'] ;
        }

        return $reviews ;
    }
}
