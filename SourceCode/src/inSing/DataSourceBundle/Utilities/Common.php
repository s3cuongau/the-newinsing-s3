<?php
/**
 * Description of Common
 *
 */

namespace inSing\DataSourceBundle\Utilities;

use inSing\DataSourceBundle\Repository\TemplateRepository;
use inSing\DataSourceBundle\Utilities\Socket\CommandServers;

class Common {

    /**
     * List of never allowed strings
     *
     * @var array
     */
    protected static $_never_allowed_str =    array(
            'document.cookie'    => '[removed]',
            'document.write'    => '[removed]',
            '.parentNode'        => '[removed]',
            '.innerHTML'        => '[removed]',
            'window.location'    => '[removed]',
            '-moz-binding'        => '[removed]',
            '<!--'                => '&lt;!--',
            '-->'                => '--&gt;',
            '<![CDATA['            => '&lt;![CDATA[',
            '<comment>'            => '&lt;comment&gt;'
    );

    /**
     * List of never allowed regex replacement
     *
     * @var array
     */
    protected static $_never_allowed_regex = array(
            'javascript\s*:',
            'expression\s*(\(|&\#40;)', // CSS and IE
            'vbscript\s*:', // IE, surprise!
            'Redirect\s+302',
            "([\"'])?data\s*:[^\\1]*?base64[^\\1]*?,[^\\1]*?\\1?"
    );

    protected static $_container;

    public function __construct(\Symfony\Component\DependencyInjection\ContainerInterface $container) {
        self::$_container = $container;
    }

    public function getHGWRLocales() {
        try {
            return self::$_container->getParameter('hgwr_locales');
        } catch (\Exception $exc) {
        }
        return array();
    }

    public function getCountry() {
        try {
            return self::$_container->getParameter('country_code');
        } catch (\Exception $exc) {
        }
        return 'en_SG';
    }

    /**
     * If FO user (SG, MY) logged in, return UserProvider object, else return false
     * @return \inSing\HgwrBundle\User\UserProvider
     */
    public function isLogin() {
        $profile = null;
        $profile = self::$_container->get('hgwr.user')->isLogin();
        return $profile;
    }

    /**
     * Get FE profile owner
     * @return \inSing\HgwrBundle\User\UserProvider
     */
    public function getProfileOwner() {
        $profile = null;
        $profile = self::$_container->get('hgwr.user')->isLogin();
        return $profile;
    }

    /**
     * get user profile by profile id
     * @param int $profileId
     * @return \inSing\HgwrBundle\User\UserProvider
     */
    public function getUserProfileById($profileId, $countryCode = '') {
        $profile = self::$_container->get('hgwr.user')->getUserProfileById($profileId);
        return $profile;
    }

    /**
     * http://cdn.test.insing.com/hgwr/users/profile_images/000/000/006/original/1679091c5a880faf6fb5e6087eb1b2dc.jpg
     * @param int $id
     * @param string $fileName
     * @return string
     */
    static public function getUrlOnCDN($id, $fileName) {
        $key = self::getKeyOnAWS($id);
        $domains = self::$_container->getParameter('cdn_links_accept');
        $index = array_rand($domains);
        $url = "http://{$domains[$index]}/hgwr/{$key}/{$fileName}";
        return $url;
    }

    public static function getPriceRange($price) {
        $listRange = array('$' => range(1, 20), '$$' => range(21, 40), '$$$' => range(41, 59), '$$$$' => range(60, 5000));
        $keyLabel = '$$$$';
        foreach ($listRange as $key => $value) {
            if(in_array($price, $value))
            {
                $keyLabel = $key;
                break;
            }
        }
        return $keyLabel;
    }

    /**
     * Detect a link in text and add a tag around the link
     *
     * @param type $content
     * @return string with link
     * @author HoangMinh TonThat
     */
    public static function detectLinks($content) {
        $pattern = '/\s((http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.\/]+\.[a-zA-Z\/]{2,3}(\/\S*)?)/';
        $tagA = ' <a href="${1}" title="${1}">${1}</a>';

        return preg_replace($pattern, $tagA, $content);
    }

    public static function filterStyleAttributeFromHTML($str) {
        $sa = new StripAttributes();
        $sa->allow = array( 'id', 'class' );
        $sa->ignore = array('span');
        $sa->exceptions = array(
                'img' => array( 'src', 'alt' ),
                'a' => array( 'href', 'title', 'target' )
        );
        return $sa->strip( $str );
    }

    public static function truncateHtml($string, $limit, $end = '...') {
        $obj = new HtmlCutStringUseDom($string, $limit, $end);

        return $obj->cut();
    }

    static public function removeUnsafeChars($str)
    {
        $unsafeChar = '? : @ / % & \\ " \' = ! + # * ~ ; ^ ( ) < > [ ] { } | . ,';
        $arrayUnsafeChar = array_fill_keys(explode(' ', $unsafeChar), ' ');

        return strtr($str, $arrayUnsafeChar);
    }

    static public function generateImageFromImagePath($imagePath, $bucketName = 'business') {
        $imagePath = '/' . trim($imagePath, '/');
        $domains = self::$_container->getParameter('cdn_links_accept');
        $index = array_rand($domains);
        $url = "http://{$domains[$index]}/{$bucketName}{$imagePath}";
        return $url;
    }

    static public function getKeyOnAWS($id) {
        $length = strlen($id);
        if ($length < 9) {
            for ($i = 0; $i < (9 - $length); $i++) {
                $id = '0' . $id;
            }
        }
        $id_arr = str_split($id, 3);
        $format = self::$_container->getParameter('aws_s3_file_key_name_format');
        $format = str_replace('[slot0]', $id_arr[0], $format);
        $format = str_replace('[slot1]', $id_arr[1], $format);
        $format = str_replace('[slot2]', $id_arr[2], $format);
        //$result = 'users/profile_images/'.$id_arr[0].'/'.$id_arr[1].'/'.$id_arr[2].'/original';
        return $format;
    }

    static public function getKeyTempOnAWS($filename) {
        $format = self::$_container->getParameter('aws_s3_file_key_name_temp_format');
        $format = str_replace('[filename]', $filename, $format);
        //$result = 'users/profile_images/{$filename};
        return $format;
    }

    static public function getUrlTempOnCDN($fileName) {
        $key = self::getKeyTempOnAWS($fileName);
        $domains = self::$_container->getParameter('cdn_links_accept');
        $index = array_rand($domains);
        $url = "http://{$domains[$index]}/hgwr/{$key}";
        return $url;
    }


    static public function convertToSlug($text, $lower = true)
    {
        $text = htmlspecialchars_decode($text, ENT_QUOTES);
        $text = trim($text, ' ');
        $text = preg_replace('/[^A-Za-z0-9\s]/', '-', $text);
        $text = preg_replace('/\s+/', '-', $text);
        $text = preg_replace('/-+/', '-', $text);
        $text = trim($text, '-');

        if (empty($text)) {
            return 'n-a';
        }

        // lowercase
        $text = $lower ? strtolower($text) : $text;

        return $text;
    }

    /**
     * Convert from int to hash
     * @param int $id
     * @return hash id
     */
    public static function idToUrlId($id) {

        $hex_id = dechex($id);
        $hex_number = strlen($hex_id) > 8 ? "016" : "08";
        $paddedId = sprintf("%{$hex_number}s", dechex($id));
        $strArr = str_split($paddedId, 2);
        return implode(array_reverse($strArr));
    }

    /**
     * Convert from hash to int
     * @param has $urlId
     * @return int
     */
    public static function urlIdToId($urlId) {
        $strArr = str_split($urlId, 2);
        $paddedId = implode(array_reverse($strArr));
        return hexdec($paddedId);
    }

    /**
     * @author Dat.Dao
     * @param $photo_url
     * @param $photo_width_height
     * @param $factorZoom
     * @return Array
     */
    static public function generatePhotoFromUrlByZoom($photo_url, $photo_width_height, $factorZoom = 1.25) {
        $image_size = explode('x', $photo_width_height);
        $cropping_width_height = '';
        if(count($image_size) == 2) {
            $cropping_width_height = $photo_width_height;
            //apply resize and crop image default: 125%
            $photo_width_height = round($image_size[0] * $factorZoom) . 'x' . round($image_size[1] * $factorZoom);
        }

        return self::generatePhotoFromUrlOptions($photo_url, $cropping_width_height, $photo_width_height);
    }

    static public function generatePhotoFromUrl($photo_url, $photo_width_height, $cropping_width_height = '') {
        if($cropping_width_height == '') {
            $image_size = explode('x', $photo_width_height);
            if(count($image_size) == 2) {
                $cropping_width_height = $photo_width_height;
                $photo_width_height = ($image_size[0] + intval($image_size[0] / 4)) . 'x' . ($image_size[1] + intval($image_size[1] / 4));
            }
        }
        if($photo_width_height == $cropping_width_height)
        {
            $cropping_width_height = '';
        }
        return self::generatePhotoFromUrlOptions($photo_url, $cropping_width_height, $photo_width_height);
    }
    static public function generatePhotoFromPath($photo_path, $photo_width_height, $cropping_width_height = '') {
        if($cropping_width_height == '') {
            $image_size = explode('x', $photo_width_height);
            if(count($image_size) == 2) {
                $cropping_width_height = $photo_width_height;
                $photo_width_height = ($image_size[0] + intval($image_size[0] / 4)) . 'x' . ($image_size[1] + intval($image_size[1] / 4));
            }
        }
        if($photo_width_height == $cropping_width_height)
        {
            $cropping_width_height = '';
        }
        $imageUrl = self::generateImageFromImagePath($photo_path);
        return self::generatePhotoFromUrlOptions($imageUrl, $cropping_width_height, $photo_width_height);
    }

    /**
     * Generate Photo url with Size & Sig
     *
     * @param String $photo_url
     * @param String $photo_width
     * @param String $photo_height
     * @param boolean $fillbg is fill background or not
     * @param string $cropping_width_height cropping size
     * @return Array $photo
     *
     * sample return array:
     *     array (
     *         'base_photo_url'        => 'http://local.cdn/rnr/4653/abcd.jpg'
     *         'photo_url'        => 'http://local.cdn/rnr/4653/abcd_300x300_fillbg_crop_200x300_b18222f6d4.jpg'
     *         'photo_name_full'    => 'abcd_300x300_fillbg_crop_200x200_b18222f6d4.jpg'
     *         'photo_name'        => 'abcd'
     *         'photo_width'        => '300'
     *         'photo_height'        => '300'
     *         'fill background'        => 'filbg'
     *         'croping width'          => '200'
     *         'croping height          => '300'
     *         'photo_ext'        => 'jpg'
     *         'photo_sig'        => 'b18222f6d4'
     *     )
     *
     * sample :
     *     $container->get('common.utils')->generatePhotoFromUrlOptions(
     *         "http://local.cdn/rnr/4653/abcd.jpg",
     *         "300x300",
     *         "true",
     *         "200x300"
     *     );
     */
    static public function generatePhotoFromUrlOptions($photo_url, $cropping_width_height, $photo_width_height = '', $fillbg = true, $no_stretch = false) {

        $cdn_links = self::$_container->getParameter('cdn_links_accept');
        $url_info  = parse_url($photo_url);

        if (empty($photo_url) || strpos($photo_url, 'staticc0') !== false || empty($url_info['host']) || !in_array($url_info['host'], $cdn_links)) {
            return $photo_url;
        }

        if(!empty($cropping_width_height))
        {
            $fillbg = false;
        }

        //Get Photos Size
        preg_match('/^(?P<width>[0-9]+)x(?P<height>[0-9]+)$/', $photo_width_height, $matches);

        $photo_width = ( isset($matches['width']) ? trim($matches['width']) : '' );
        $photo_height = ( isset($matches['height']) ? trim($matches['height']) : '' );

        //Get Cropping Photos Size
        preg_match('/^(?P<width>[0-9]+)x(?P<height>[0-9]+)$/', $cropping_width_height, $matches);

        $crop_width = ( isset($matches['width']) ? trim($matches['width']) : '' );
        $crop_height = ( isset($matches['height']) ? trim($matches['height']) : '' );

        //Get Photos Information from URL
        $url_info_path = isset($url_info['path']) ? $url_info['path'] : '' ;
        $photo_info = pathinfo( $url_info_path );

        $secret = self::$_container->getParameter('cdn_secret');

        //Create Photo Information
        $photo = array(
            'base_photo_url'    => $photo_url,
            'photo_url'         => '',
            'photo_name_full'   => '',
            'photo_name'        => (isset($photo_info) && isset($photo_info['filename'])) ? $photo_info['filename'] : '',
            'photo_width'       => $photo_width,
            'photo_height'      => $photo_height,
            'fillbg'            => ($fillbg == true)? 'fillbg' : '',
            'no_stretch'        => ($no_stretch == true)? 'no_stretch' : '',
            'crop_width'        => $crop_width,
            'crop_height'       => $crop_height,
            'photo_ext'         => (isset($photo_info) && isset($photo_info['extension'])) ? $photo_info['extension'] : '',
            'photo_secret'      => $secret
        );

        //Generate Photo Sig
        $photo['photo_sig'] = self::generatePhotoSig( $photo );

        //Create File Name
        $photo['photo_name_full'] = strtr('[name][widthxheight][fillbg][no_stretch][cwidthxcheight]_[sig].[ext]', array(
            '[name]' => $photo['photo_name'],
            '[widthxheight]' => $photo['photo_width'] ? '_' . $photo['photo_width'] . 'x' . $photo['photo_height'] : '',
            '[fillbg]' => $photo['fillbg'] ? '_fillbg' : '',
            '[no_stretch]' => $photo['no_stretch'] ? '_no_stretch' : '',
            '[cwidthxcheight]' => $photo['crop_width'] ? '_crop_' . $photo['crop_width'] . 'x' . $photo['crop_height'] : '',
            '[sig]' => $photo['photo_sig'],
            '[ext]' => $photo['photo_ext']
        ));

        //Generate Photo Url
        $photo['photo_url'] = strtr('[scheme]://[host][dirname]/[photo_name_full]', array(
                '[scheme]'          => isset($url_info['scheme']) ? $url_info['scheme'] : '',
                '[host]'            => isset($url_info['host']) ? $url_info['host'] : '',
                '[dirname]'         => (isset($photo_info) && isset($photo_info['dirname'])) ? $photo_info['dirname'] : '',
                '[photo_name_full]' => $photo['photo_name_full']
        ));

        return $photo['photo_url'];
    }

    /**
     * Generate media URL from the resource ID
     *
     * @param    Int    $resourceId, $transcodeName, $fileType
     * @return    String
     */
    static public function generatePhotoFromResourceId($resourceId, $transcodeName = 'pc_143x107', $fileType = 'jpg') {
        $str = Common::idToUrlId($resourceId);
        $hex_array = str_split($str, 2);
        $path = implode('/', $hex_array);

        $arrInsingCdnUrls = self::$_container->getParameter('insing_cdn_url');
        $insingCdnUrl = $arrInsingCdnUrls[rand(0, count($arrInsingCdnUrls) - 1)];

        return $insingCdnUrl . $path . '/' . $transcodeName . '.' . $fileType;
    }


    /**
     * Generate sigature
     *
     * @param    Array    $photo
     * @return    String
     */
    private static function generatePhotoSig( $photo )
    {
        $file_name = strtr('[name]_[width]x[height].[ext][fillbg][no_stretch][crop_width][crop_height][secret]', array(
            '[name]'        => $photo['photo_name'],
            '[width]'       => $photo['photo_width'],
            '[height]'      => $photo['photo_height'],
            '[ext]'         => $photo['photo_ext'],
            '[fillbg]'      => $photo['fillbg'],
            '[no_stretch]'  => $photo['no_stretch'],
            '[crop_width]'  => $photo['crop_width'],
            '[crop_height]' => $photo['crop_height'],
            '[secret]'      => $photo['photo_secret']
        ));
        return substr(md5($file_name), 0, 10);
    }

    /**
     * Recogise the device: phone, tablet or computer
     * @author Khuong Phan
     * @return string
     */
    public static function checkDevice()
    {
        $session = self::$_container->get('session');
        
        if (!$session->get('hgwr_device')) {
            $detect = new DevicesDetect();
            if ( $detect->isTablet() ) {
                $device = Constant::DEVICE_TABLET;
            } elseif ( $detect->isMobile() ) {
                $device = Constant::DEVICE_PHONE;
            } else {
                $device = Constant::DEVICE_COMPUTER;
            }
        } else {
            $device = $session->get('hgwr_device');
        }
        return $device;
    }

    public static function isComputer() {
        return self::checkDevice() == Constant::DEVICE_COMPUTER;
    }

    public static function isTablet() {
        return self::checkDevice() == Constant::DEVICE_TABLET;
    }

    public static function isMobile() {
        return self::checkDevice() == Constant::DEVICE_PHONE;
    }

    public static function isMobileIos() {
        $detect = new DevicesDetect();
        if ($detect->isiOS()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * parse format: [[iframe:w=600:h=600]https://vine.co/v/bQePm27I3E3/embed/simple]
     * @param string $iframeCode
     * @author: BienTran
     * @return: string
     */
    public static function iframeCodeParser($iframeCode) {

      /* Matching codes */
      $urlmatch = "([a-zA-Z]+[:\/\/]+[A-Za-z0-9\-_]+\\.+[A-Za-z0-9\.\/%&=\?\-_]+)";

      $match["iframe"] = "/\[\[iframe:w=([0-9]+(%|px)?):h=([0-9]+(%|px)?)\]\[".$urlmatch."\]\]/is";

      /**
       * array (size=6)
          0 => string '[iframe:w=800:h=600][https://vine.co/v/bQePm27I3E3/embed/simple]'
          1 => string '800'
          2 => string ''
          3 => string '600'
          4 => string ''
          5 => string 'https://vine.co/v/bQePm27I3E3/embed/simple' (length=42)
       */
      $replace["iframe"] = "<iframe src=\"$5\" width=\"$1\" height=\"$3\" frameborder=\"0\"></iframe>";

      /* Parse */
      $iframe = preg_replace($match, $replace, $iframeCode);

      return $iframe;
    }

    /**
     *
     * Parse image size from Url ...
     * @param string $url url
     */
    public static function parseImageSizeFromUrl($url) {
        $size = array('width' => 0, 'height' => 0);
        preg_match('/([0-9]+)x([0-9]+)/', $url, $match);
        if ($match) {
            $arrSize = explode('x', $match[0]);
            $size['width'] = $arrSize[0];
            $size['height'] = $arrSize[1];
        }
        return $size;
    }

    /**
     *
     * Get domain name by full domain
     * @param string $fullDomain domain
     */
    public static function getDomainNameByDomain($fullDomain) {
        if (self::$_container instanceof ContainerInterface ) {
            $session = self::$_container->get('session');
            if (!$session->get('hgw_domain_name')) {
                if (preg_match('/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i', $fullDomain, $regs)) {
                    $domainName = $regs['domain'];
                } else {
                    $domainName = null;
                }
                $session->set('hgw_domain_name', $domainName);
            } else {
                $domainName = $session->get('hgw_domain_name');
            }
        } else {
            if (preg_match('/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i', $fullDomain, $regs)) {
                $domainName = $regs['domain'];
            } else {
                $domainName = null;
            }
        }
        return $domainName;
    }

    public static function getDomainName() {
        $domainName = null;
        $sessionDomainName = 'hgw_regional_domain_name';
        $fullDomain = self::$_container->get('request')->getHost();
        $session = self::$_container->get('session');

        if (!$session->get($sessionDomainName)) {
            if (preg_match('/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i', $fullDomain, $regs)) {
                $domainName = $regs['domain'];
            }
            $session->set($sessionDomainName, $domainName);
        } else {
            $domainName = $session->get($sessionDomainName);
        }
        return $domainName;
    }

    static public function parseKeywordForGPT($obj, $is_array = false)
    {
      $filterProperties = '';
        if($is_array)
        {
          //remove unsafe chars on each item
          $filterProperties = array_map(
                  function($item) {
                    //remove unsafe chars on each item
                    $temp = is_array($item) ? Common::removeUnsafeChars(implode(' ', $item)) : Common::removeUnsafeChars($item);
                    $temp = array_filter(array_map('trim', explode(' ', $temp)), 'strlen');
                    // parse array to string follow format 'item1', 'item2', 'item3', ...
                    $temp = implode(' ', $temp);
                    return $temp;
                  },
                  $obj);

          $filterProperties = implode(',', $filterProperties);
        }
        else
        {
          // Remove unsafe chars
          // remove '? : @ / % & \\ " \' = ! + # * ~ ; ^ ( ) < > [ ] { } | . ,'
          $filterProperties = self::removeUnsafeChars($obj);
          // remove space on begin and end of string, convert string to array and remove values null from array
          $filterProperties = array_filter(array_map('trim', explode(' ', $filterProperties)), 'strlen');
          // parse array to string follow format 'item1', 'item2', 'item3', ...
          $filterProperties = implode(',', $filterProperties);
        }

        return $filterProperties;
    }

    public static function generateCmsContentSecretSignature($id, $previewTime, $secretKey) {
        return md5($id . $previewTime . $secretKey);
    }


    public static function removeRegionNumber($fullNumber) {
        return preg_replace("/\+[0-9]{2,}\s/",'', $fullNumber);
    }

    /**
     * Generate image url for Restaurant
     * @param string $resourceId
     * @param string $transcodeName
     * @param string $fileType
     * @return string
     */
    static public function generateMediaUrl($resourceId, $transcodeName = 'pc_143x107', $fileType = 'jpg') {
        $str = Common::idToUrlId($resourceId);
        $hex_array = str_split($str, 2);
        $path = implode('/', $hex_array);
        $arrInsingCdnUrls = self::$_container->getParameter('insing_cdn_url');
        $insingCdnUrl = $arrInsingCdnUrls[rand(0, count($arrInsingCdnUrls) - 1)];

        return $insingCdnUrl . $path . '/' . $transcodeName . '.' . $fileType;
    }

    static public function checkUserRole($userRole){

        $role_flag = false ;
        $role_list = array("ROLE_SUPER_ADMIN", "ROLE_SG_ADMIN", "ROLE_MY_ADMIN") ;

        foreach ($userRole as $key) {
            if(in_array($key->getRole(), $role_list)){
                $role_flag = true ;
            }
        }

        return $role_flag ;
    }

    /**
     * Show time slot of booking pages
     * @param string $timeId
     * @return string
     * @author minh.ton
     */
    public static function convertTimeShow($timeId)
    {
        $strTime = (count(strVal($timeId))<4)?str_pad(strVal($timeId), 4, '0', STR_PAD_LEFT): strVal($timeId);

        //$subFix = intVal(substr($strTime, 0, 2) >= 12) ? ' PM' : ' AM';
        //$ret = substr($strTime, 0, 2).':'.substr($strTime, 2, 2).$subFix;

        $ret = \DateTime::createFromFormat('d-m-Y Hi', "01-01-1990 $strTime");

        return ($ret) ? $ret->format('h:i A') : '';
    }

    /**
     * Generate Url base on Absolute Path
     *
     * @author Lap To
     * @param String $absolutePath
     * @param Array $param
     */
    public static function generateUrl($absolutePath, $param) {
        if (is_array($param) && !empty($param)) {
            $absolutePath .= '?';
            foreach ($param as $k=>$v) {
                $absolutePath .= $k . '=' . $v . '&';
            }
            $absolutePath = trim($absolutePath, '&');
        }
        return $absolutePath;
    }

    /**
     * Convert stdObj to a Declared object
     * @param object $instance
     * @param string $className
     * @return object
     */
    function objectToObject($instance, $className) {
        if (!is_object($instance)) {
            return null;
        }
        return unserialize(sprintf(
            'O:%d:"%s"%s',
            strlen($className),
            $className,
            strstr(strstr(serialize($instance), '"'), ':')
        ));
    }


    /**
     * Get environment dev
     *
     * @author  bientran
     * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
     * @return boolean
     */
    public static function getEnvironmentDev($container) {
        if ($container instanceof ContainerInterface) {
            return $container->get('kernel')->getEnvironment() == Constant::ENV_DEV ? true : false;
        } else {
            return false;
        }
    }

    /**
     * Clear all data cache of front-end
     *
     * @author Lap To
     * @param ContainerAwareInterface $controller
     * @param String $country_code
     * @param String $pageType
     * @param String $page_identify It slug or item_id
     * @param boolean $is_command if call this function in a Command, set this one is true
     * @return Void
     */
    public static function clearAllCachedPage($country_code, $pageType, $page_identify = null, $is_command = false) {
        try {
            $env = self::$_container->get('kernel')->getEnvironment();
            
            if($is_command)
            {
                self::ClearCachePageContentForCommand($pageType, $page_identify);
            }
            else
            {
                $country_locale = array(
                    Constant::COUNTRY_CODE_SINGAPORE => 'sg',
                    Constant::COUNTRY_CODE_MALAYSIA => 'my',
                    Constant::COUNTRY_CODE_AUSTRALIA => 'au',
                );
                $locale = $country_locale[Constant::COUNTRY_CODE_SINGAPORE];
                if(isset($country_locale[$country_code]))
                {
                    $locale = $country_locale[$country_code];
                }
                $cmd = "cd .. && php app/console inSing:cache:delete_page_content_keys $pageType --locale=$locale -e $env > /dev/null &";
                if(!empty($page_identify))
                {
                    $cmd = "cd .. && php app/console inSing:cache:delete_page_content_keys $pageType $page_identify --locale=$locale -e $env > /dev/null &";
                }
                exec($cmd);
            }
            
        } catch (\Exception $exc) {
            return false;
        }
        return true;
    }
    
    public static function ClearCachePageContentForCommand($page_type, $page_identify = null) {
        $logger = new HgwLogger(self::$_container, 'delete_page_content_cache_keys');
        $result_cache_keys = null;
        try {
            
            $date = date('Y-m-d H:i:s');
            $logger->info("----------- Start for {$page_type} at {$date}");
            
            $params = array(Constant::CACHE_KEY_PAGE_DETAIL,
                self::$_container->getParameter('country_code'), $page_type);
            if(!empty($page_identify))
            {
                $params[] = $page_identify;
            }
            $search_key = join ('_',  $params);
            $result_cache_keys = self::ClearCacheFromKeyWord($search_key);
            //$logger->info("----------- Cache keys: " . print_r($result_cache_keys, true));
            $logger->info("----------- Total Cache keys: " . count($result_cache_keys));
            
            
            $date = date('Y-m-d H:i:s');
            $logger->info("----------- End for {$page_type} at {$date}");
        } catch (\Exception $exc) {
            $logger->exp_err($exc);
        }
        return $result_cache_keys;
    }
    
    public static function ClearCacheFromKeyWord($key_word, $delete = true) {
        $logger = new HgwLogger(self::$_container, 'delete_page_content_cache_keys');
        //$cache_prefix = Constant::CACHE_PREFIX . self::$_container->getParameter('assets_version');
        try {            
            $servers_config = self::$_container->getParameter('s3_cache_memcached_servers');
            
            $servers = array();
            $result_cache_keys = array();
            
            $key_word = str_replace(array(' ', '-', '.'), '_', $key_word);

            foreach ($servers_config as $server) {
                list($host, $port) = explode(':', $server[0]);
                if(empty($host) || empty($port))
                {
                    continue;
                }
                $servers[] = array($host, $port);
            }
            
            $comand_server = new CommandServers();
            //$cache = self::$_container->get('hgw.cache');
            foreach ($servers as $server) {
                $result = $comand_server->search($server[0], $server[1], $key_word);
                if(!$result)
                {
                    continue;
                }
                if($delete)
                {
                    if(!empty($result))
                    {
                        foreach ($result as $cache_key) {
                            // using Socket to delete
                            $comand_server->delete($server[0], $server[1], $cache_key);
                            
                            // using php memcached to delete
                            //$cache->delete(str_replace($cache_prefix, '', $cache_key));
                        }
                    }
                }
                $result_cache_keys = array_merge($result_cache_keys, $result);
            }
            return $result_cache_keys;
        } catch (\Exception $exc) {
            $logger->exp_err($exc);
        }
        return null;
    }

    /**
     * Cut short a text to a certain length and round up to word-boundary
     *
     * @author  thai.pham@s3corp.com.vn
     * @param String $text: text to be trimmed
     * $param Int $desired_width: desired length
     * @return trimmed text
     */
    public static function textTruncate($text, $desired_width) {
        $parts = preg_split('/([\s\n\r]+)/', $text, null, PREG_SPLIT_DELIM_CAPTURE);
        $parts_count = count($parts);

        $length = 0;
        $last_part = 0;
        for (; $last_part < $parts_count; ++$last_part) {
            $length += strlen($parts[$last_part]);
            if ($length > $desired_width) { break; }
        }

        $new_text = implode(array_slice($parts, 0, $last_part));
        return $new_text;
    }

    /**
     *
     * @param int $iTimestamp
     * @param string $lDFormat ($lDFormat = 'l' => Saturday throught Sunday, $lDFormat = 'D' => Mon throught Sun)
     * @return string Date
     */
    public static function getTimeAgo($iTimestamp, $lDFormat = 'l') {
        if(!is_numeric($iTimestamp))
        {
            return '';
        }

        if(!$lDFormat)
        {
            $lDFormat = 'l';
        }

        // Get Current Timestamp
        $iCurrentTimestamp = time();
        if ($iTimestamp >= $iCurrentTimestamp) {
            return 0;
        }
        // Get Difference between Current Timestamp
        // and Timestamp of item

        $iDifference = $iCurrentTimestamp - $iTimestamp;

        // Save the periods in an array

        $periods = array("sec", "min", "hour", "day",
            "week", "month", "year", "decade");

        // Save the lengths of each period in another array

        $lengths = array("60", "60", "24", "7", "4.35", "12", "10");

        // The loop below will go through the lengths array
        // starting at key 0 and incrementing each time the
        // loop is run
        // Each time, the $iDifference variable is divided by the
        // length being accessed
        // The loop will stop running once $iDifference is smaller
        // than the value of the length being accessed at the time
        // The aim of this loop is find out how old the item is

        for ($j = 0; $iDifference >= $lengths[$j]; $j++)
            $iDifference /= $lengths[$j];

        // If 'j' is equal to 6, this means the item was added
        // over a month ago. In this case, we simply want to
        // display the month and the year.
        if ($j < 3) {
            // On the other hand, if the item is more recent, we use
            // the code below to display the age of the item such as:
            // '3 days ago' or '7 hours ago'

            $iDifference = round($iDifference);
            if ($iDifference != 1)
                $periods[$j] .= "s";
            $sReturn = $iDifference . " " . $periods[$j] . " ago";
        } elseif ($j == 3 && date('W', $iTimestamp) == date('W', time())) {
            $sReturn = date($lDFormat . ' \a\t g:i A', $iTimestamp);
        } else {
            $sReturn = date('d M Y', $iTimestamp);
        }

        // If the value is '1 day ago', we can change this
        // to 'yesterday'.

        //if (trim($sReturn) == '1 day ago')
        //    $sReturn = "Yesterday";

        return $sReturn;
    }

    /**
     * Parse the array to number of columns
     *
     * @param array $arrTag
     * @param integer $columnNumber
     * @param integer $columnNumber
     * @param bool $groupAlphabe group alphabet on reservation find-restaurant module
     * @param string $field: field in array to want to group alphabet
     * @author Minh Ton
     */
    static public function parsetArrayToColumn($arrTag, $columnNumber = 4, $groupAlphabe = false, $field = '')
    {
        $ret = array();
        $alphas = range('A', 'Z');

        $totalItem = count($arrTag);

        if ($totalItem > 0) {
            //Min items on each column
            $numberInColumn = ceil($totalItem / $columnNumber);

            // index of item in array
            $indexItem = 0;
            $indexColumn = 0;
            $titleAlp = '';
            $arrAlp = array();
            if($indexItem < $totalItem)
            {
                if ($groupAlphabe) {
                    foreach ($arrTag as $value) {
                        // Get the first char to arrange alphabet
                        if ($field) {
                            $firstChar = strtoupper(substr($value[$field], 0, 1));
                        }
                        else {
                            $firstChar = strtoupper(substr($value, 0, 1));
                        }
                        if ($titleAlp != $firstChar) {
                            $titleAlp = $firstChar;
                            if ($indexItem >= (($indexColumn + 1) * $numberInColumn )){
                                $ret[$indexColumn] = $arrAlp;
                                $indexColumn++;
                                $arrAlp = array();
                            }
                        }

                        // If the first char is number or not alphabet, group to #
                        if (!in_array($firstChar, $alphas)) {
                            $arrAlp['#'][] = $value;
                        }
                        // If the fist char in alphabet, group by this char
                        else{
                            $arrAlp[$titleAlp][] = $value;
                        }

                        $indexItem++;
                        if ($indexItem == $totalItem) {
                            $ret[$indexColumn] = $arrAlp;
                        }
                    }
                } else {
                    for ($i = 0; $i < $columnNumber; $i++) {
                        $ret[] = array_slice($arrTag, $indexItem, $numberInColumn);
                        $indexItem += ($numberInColumn);
                    }
                }
            }
        }
        return $ret;
    }

    /*
     * dung.le
     */
    public function convertDateTimeSearchPage($dateTime, $type)
    {
        $rs = "";
        if ($type == "time")
        {
            $dateTime = substr_replace($dateTime, ':', 2, 0);
            $filter_time_arr = explode(':', $dateTime);
            $hour = intval($filter_time_arr[0]);
            $hour_label = ( $hour > 12) ? ($hour - 12) : $hour;
            $hour_label = intval($hour_label) == 0 ? 12 : $hour_label;
            $time = sprintf("%s%s:%s%s", ($hour_label < 10 ? '0' : ''), $hour_label, $filter_time_arr[1], ($hour < 12 ? 'am' : 'pm'));
            $rs = sprintf("%s%s:%s %s", ($hour_label < 10 ? '0' : ''), $hour_label, $filter_time_arr[1], ($hour < 12 ? 'AM' : 'PM'));
        }
        else if ($type == "date")
        {
            $date = new \DateTime(date('Y') . '-' . $dateTime);
            $rs = ($date->format('m-d') == date('m-d')) ? $date->format('d M') . ' (Today)' : $date->format('d M');
        }
        return  $rs;
    }

    public static function getMimeType($filename, $filestream) {
        $type = false;
        // Fileinfo documentation says fileinfo_open() will use the
        // MAGIC env var for the magic file
        if (extension_loaded('fileinfo') && isset($_ENV['MAGIC']) &&
                ($finfo = finfo_open(FILEINFO_MIME, $_ENV['MAGIC'])) !== false) {
            if (($type = finfo_buffer($finfo, $filestream)) !== false) {
                // Remove the charset and grab the last content-type
                $type = explode(' ', str_replace('; charset=', ';charset=', $type));
                $type = array_pop($type);
                $type = explode(';', $type);
                $type = trim(array_shift($type));
            }
            finfo_close($finfo);

            // If anyone is still using mime_content_type()
        } elseif (function_exists('mime_content_type')) {
            // mime_content_type supports streams for input, even if
            // the fact is undocumented! That's kind of crazy.
            $type = trim(mime_content_type($filestream));
        }

        if ($type !== false && strlen($type) > 0)
            return $type;

        // Otherwise do it the old fashioned way
        static $exts = array(
            'jpg' => 'image/jpeg', 'png' => 'image/png'
        );
        $ext = strtolower(pathInfo($filename, PATHINFO_EXTENSION));
        return isset($exts[$ext]) ? $exts[$ext] : null;
    }

    /**
     * Get facebook info
     * @author Lap To
     * @param string $property
     * @return Array
     */
    public static function getFacebookAcount($property = '') {
        $countryCode = self::$_container->getParameter('country_code');
        $facebookInfo = self::$_container->getParameter('facebook_api_account');
        $facebookInfo = $facebookInfo[$countryCode];
        if (!empty($property) && isset($facebookInfo[$property])) {
            return $facebookInfo[$property];
        }
        return '';
    }

    /**
     * Generate Url for IBL
     *
     * @param $countryCode
     * @param $slug
     * @return mixed|string
     */
    public static function generateIblUrl($state, $countryCode, $slug)
    {
        $frontEndUrl = self::$_container->getParameter('frontend_url');
        $frontEndUrl = trim($frontEndUrl[$countryCode], '/');
        $url = $frontEndUrl . self::$_container->get('router')->generate('in_sing_hgwr_ibl', array(
            'state' => $state,
            'slug' => $slug
        ));
        $countryRoutePath = self::$_container->getParameter('country_route_path');
        $countryRoutePathLocale = self::$_container->getParameter('country_route_path_locale');
        $url = preg_replace("/\/{$countryRoutePath}\//", '/' . $countryRoutePathLocale[$countryCode] . '/', $url, 1);

        return $url;
    }

    /**
     * Debug: collect API calls
     * ONLY FOR DEV ENVIRONMENT
     * @param string $channel
     * @param string $url
     */
    public function collectApiCall($channel, $url) {
        //Debug: collect API calls
        if ('dev' === self::$_container->get('kernel')->getEnvironment()) {
            self::$_container->get('data_collector.api')->setDataUrls(array($channel => $url));
        }
    }

    /**
     * Author Minh Tong
     * Check device
     * @param string $mobile_flatphom iOS , Android , WindowPhone , BlackBerry
     * @return boolean
     */
    public function mobileOperaSystemDetect($mobile_flatphom){
        $devicesDetect = new DevicesDetect();
        switch ($mobile_flatphom) {
            case 'iOS':
                return $devicesDetect->isiOS();
                break;
            case 'Android':
                return $devicesDetect->isAndroidOS();
                break;
            case 'WindowPhone':
                return $devicesDetect->isWindowsPhoneOS();
                break;
            case 'BlackBerry':
                $bbOsVersion = $devicesDetect->version('BlackBerry');
                $bbOsVersionRequire = 10;
                $isMatchBbOsVersion = false;
                if (!empty($bbOsVersion) && substr($bbOsVersion, 0, 2) == $bbOsVersionRequire ) {
                    $isMatchBbOsVersion = true;
                }
                return ( $devicesDetect->isBlackBerryOS() && $isMatchBbOsVersion == true );
                break;
        }
    }

    /**
     * Author Minh Tong
     * Check right URL for IBL Page
     * @param string $mobile_flatphom iOS , Android , WindowPhone , BlackBerry
     * @return boolean
     */
    public function redirectToRightStateIBL($restaurant, $request, $state, $slug, $pageType = 'IBL', $hash_id = null){
        if ($restaurant) {
            if (isset($restaurant['state']) && !empty($restaurant['state'])) {
                $state_name = $this->convertToSlug($restaurant['state']);
                if(trim($state_name) != trim($state)){
                    $routeName = $request->get('_route');
                    $url = null;
                    switch ($pageType) {
                        case 'IBL' :
                            $url = self::$_container->get('router')->generate($routeName, array('state' => $state_name , 'slug' => $slug,'hash_id' => $hash_id),true);
                            break;
                        case 'HGB' :
                            $url = self::$_container->get('router')->generate($routeName, array('state' => $state_name , 'restaurant_slug' => $slug),true);
                            break;
                        case 'PHOTO_DETAIL':
                            $param = array('state' => $state_name , 'slug' => $slug);
                            if (!empty($hash_id)) {
                                $param = array_merge($param, array('photo_hash_id' => $hash_id));
                            }
                            $url = self::$_container->get('router')->generate($routeName, $param,true);
                            break;
                        case 'REVIEW':
                            if ($hash_id) {
                                $url = self::$_container->get('router')->generate($routeName, 
                                    array('state' => $state_name , 'slug' => $slug,'review_hash' => $hash_id),true);
                            } else {
                                $url = self::$_container->get('router')->generate($routeName, 
                                    array('state' => $state_name , 'slug' => $slug),true);
                            }
                            break;
                        default :
                            $url = self::$_container->get('router')->generate($routeName, array('page_slug' => $state_name , 'search_term' => $slug),true);
                    }

                    return $url;
                }
            }
        }
        return null;
    }

    /**
     * Convert second to slot second
     * @param int $second
     * @return mixed
     */
    static public function convertSecondToSlotSecond($second = 0){
        $array = array(0,15,30,45);
        $pos = 0;

        foreach($array as $value){
            if($value > $second){
                break;
            }
            $pos++;
        }
        $return = ( $pos>count($array) )?$array [0]: $array[$pos-1];

        return $return;
    }

    public static function convertTimeSlotToTimestamp($now, $timeSlot = 100){
        $h = sprintf("%02u",$timeSlot/100);
        $s = sprintf("%02u",$timeSlot%100);
        $t = strtotime($now->format('Y-m-d').' '.$h.':'.$s);
        return $t;
    }

    /**
     * https://jira.insing.com/jira/browse/HGW-461
     *
     * @param $datetime
     * @return \DateTime|string
     */
    public static function getAvailabilityDatetime($datetime, $isSERP = false)
    {
        $dateDiffTemp1 = clone $datetime; // Midnight
        $dateDiffTemp1->setTime(0,0);
        $dateDiffTemp2 = new \DateTime($datetime->format('Y-m-d') . ' 11:00:59'); // 11am
        $dateDiffTemp3 = new \DateTime($datetime->format('Y-m-d') . ' 11:01:00'); // 11:01am
        $dateDiffTemp4 = new \DateTime($datetime->format('Y-m-d') . ' 13:00:59'); // 13am
        $dateDiffTemp5 = new \DateTime($datetime->format('Y-m-d') . ' 13:01:00'); // 13:01am
        $dateDiffTemp6 = new \DateTime($datetime->format('Y-m-d') . ' 18:30:59'); // 6:30pm
        $dateDiffTemp7 = new \DateTime($datetime->format('Y-m-d') . ' 18:31:00'); // 6:31pm
        $dateDiffTemp8 = new \DateTime($datetime->format('Y-m-d') . ' 21:00:59'); // 9pm
        $dateDiffTemp9 = new \DateTime($datetime->format('Y-m-d') . ' 21:01:00'); // 9pm
        $dateDiffTemp10 = new \DateTime($datetime->format('Y-m-d') . ' 23:59:59'); // Midnight
        if($datetime->getTimestamp() >= $dateDiffTemp1->getTimestamp()
            && $datetime->getTimestamp() <= $dateDiffTemp2->getTimestamp()) {
            $datetime->modify($isSERP ? '7pm' : '6:30pm');
            $ret = array(
                'availability_datetime' =>  $datetime,
                'availability_force_slots' => 21
            );
        } elseif($datetime->getTimestamp() >= $dateDiffTemp3->getTimestamp()
            && $datetime->getTimestamp() <= $dateDiffTemp4->getTimestamp()) {
            $datetime->modify($isSERP ? '+45 minutes' : '+15 minutes');
            $forceSlots = 24 - (int) $datetime->format('G');
            $forceSlots *= 4;
            $forceSlots += 4;
            $ret = array(
                'availability_datetime' =>  $datetime,
                'availability_force_slots' => $forceSlots
            );
        } elseif($datetime->getTimestamp() >= $dateDiffTemp5->getTimestamp()
            && $datetime->getTimestamp() <= $dateDiffTemp6->getTimestamp()) {
            $datetime->modify($isSERP ? '7pm' : '6:30pm');
            $ret = array(
                'availability_datetime' =>  $datetime,
                'availability_force_slots' => 21
            );
        } elseif($datetime->getTimestamp() >= $dateDiffTemp7->getTimestamp()
            && $datetime->getTimestamp() <= $dateDiffTemp8->getTimestamp()) {
            $datetime->modify($isSERP ? '+45 minutes' : '+15 minutes');
            $forceSlots = 24 - (int) $datetime->format('G');
            $forceSlots *= 4;
            $forceSlots += 4;
            $ret = array(
                'availability_datetime' =>  $datetime,
                'availability_force_slots' => $forceSlots
            );
        } elseif($datetime->getTimestamp() >= $dateDiffTemp9->getTimestamp()
            && $datetime->getTimestamp() <= $dateDiffTemp10->getTimestamp()) {
            $datetime->modify('+1 day ' . ($isSERP ? '7pm' : '6:30pm'));
            $ret = array(
                'availability_datetime' =>  $datetime,
                'availability_force_slots' => 21
            );
        } else {
            $ret = array(
                'availability_datetime' => '',
                'availability_force_slots' => ''
            );
        }
        
        return $ret;
    }

    /**
     * Counting value of elements in array recursively.
     *
     * @author khoa.nguyen
     *
     * @param mixed $mixedValue
     * @return number
     */
    public static function avgArrayRecursive($mixedValue, $keyOfRating = 'overall') {

        $avgOfRatings = $totalEntities = $totalRatings = 0;
        $overAll = -1;

        $callback = function(&$val, $key) use(&$totalRatings, &$totalEntities, &$overAll, $keyOfRating) {
            // Forcing return overall if have.
            if ($key == $keyOfRating) {
                $overAll = $val;
            }
            if ($val > 0) {
                $totalRatings += $val;
                $totalEntities++;
            }
        };

        /* Should check to prevent potential errors */
        if (!empty($mixedValue)) {
            $mixedValue = is_array($mixedValue) ? $mixedValue : array($mixedValue);
            array_walk_recursive($mixedValue, $callback);
            if ($totalEntities > 0) {
                $avgOfRatings = round($totalRatings / $totalEntities, 1);
            }
        }

        return $overAll >= 0 ? $overAll : $avgOfRatings;
    }

    /**
     * XSS Cleaner
     *
     * @param string $str
     * @return string
     */
    public static function xssClean($str) {
        $str = self::doNeverAllowed($str);
        $str = str_replace(array('<?', '?'.'>'),  array('&lt;?', '?&gt;'), $str);

        if (preg_match('/script|xss/i', $str))
        {
            $str = preg_replace('#</*(?:script|xss).*?>#si', '[removed]', $str);
        }

        $search = array( '@<script[^>]*?>.*?</script>@si', '@<[\/\!]*?[^<>]*?>@si', '@<style[^>]*?>.*? </style>@siU', '@<![\s\S]*?--[ \t\n\r]*>@');
        $str = preg_replace($search, '[removed]', $str);

        return $str;
    }

    /**
     * Do Never Allowed
     *
     * A utility function for xss_clean()
     *
     * @param     string
     * @return     string
     */
    private static function doNeverAllowed($str)
    {
        $str = str_replace(array_keys(self::$_never_allowed_str), self::$_never_allowed_str, $str);

        foreach (self::$_never_allowed_regex as $regex)
        {
            $str = preg_replace('#'.$regex.'#is', '[removed]', $str);
        }

        return $str;
    }

    /**
     * Shorten Url
     * Original URL: http://sub.domain.com/this-is/a-very-long-url/because-i-like-posting-long-urls-lol.html
     * Truncated URL: http://sub.domain.com/...osting-long-urls-lol.html
     * @param string $url
     * @param int $length
     * @param string $including
     * @return string
     */
    public function shortenUrl($url = '', $length = 25, $including= '&hellip;')
    {
        $var = parse_url($url);
        if(isset($var['scheme']) || isset($var['host'])) {
            $url = isset($var['scheme']) ? $var['scheme'] . '://' : '';
            $url .= isset($var['host']) ? $var['host'] . '/' : '';
            $url .= isset($var['port']) ? ':' . $var['port'] : '';
            $url .= isset($var['user']) ? $var['user'] : '';
            $url .= isset($var['pass']) ? '@' . $var['pass'] : '';
            $relativeUrl = isset($var['path']) ? substr($var['path'], 1, strlen($var['path'])) : '';
            $relativeUrl .= isset($var['query']) ? $var['query'] : '';
            $relativeUrl .= isset($var['fragment']) ? $var['fragment'] : '';
            if(strlen($relativeUrl) > $length) {
                $last = substr($relativeUrl, - $length);
                $url .= $including . $last;
            } else {
                $url .= $relativeUrl;
            }
        }

        return $url;
    }

    /**
     * mapping state AU by city
     * @param unknown $city
     * @author tri.van
     */
    public function mappingStateAUByCity($city) {
        $listMapping = array(
                'sydney' => 'new-south-wales',
                'melbourne' => 'victoria',
                'brisbane' => 'queensland',
                'gold-coast' => 'queensland',
                'sunshine-coast' => 'queensland',
                'cairns' => 'queensland',
                'townsville' => 'queensland',
                'mount-isa' => 'queensland',
                'mackay' => 'queensland',
                'rockhampton' => 'queensland',
                'bundaberg' => 'queensland',
                'hervey-bay' => 'queensland',
                'toowoomba' => 'queensland',
                'maryborough' => 'queensland',
                'gladstone' => 'queensland',
                'warwick' => 'queensland',
                'charters-towers' => 'queensland',
                'port-douglas' => 'queensland',
                'darwin' => 'northern-territory',
                'canberra' => 'australian-capital-territory',
                'perth' => 'western-australia',
                'adelaide' => 'south-australia',
                'hobart' => 'tasmania',
                'lismore' => 'new-south-wales',
                'byron' => 'new-south-wales',
                'ballina' => 'new-south-wales',
                'tamworth' => 'new-south-wales',
                'armidale' => 'new-south-wales',
                'dubbo' => 'new-south-wales',
                'greater-taree' => 'new-south-wales',
                'grafton' => 'new-south-wales',
                'port-macquarie' => 'new-south-wales',
                'kempsey' => 'new-south-wales',
                'port-stephens' => 'new-south-wales',
                'maitland' => 'new-south-wales',
                'newcastle' => 'new-south-wales',
                'orange' => 'new-south-wales',
                'bathurst' => 'new-south-wales',
                'wollongong' => 'new-south-wales',
                'nowra' => 'new-south-wales',
                'griffith' => 'new-south-wales',
                'albury' => 'new-south-wales',
                'queanbeyan' => 'new-south-wales',
                'alice-springs' => 'northern-territory',
                'katherine' => 'northern-territory',
                'port-lincoln' => 'south-australia',
                'whyalla' => 'south-australia',
                'murray-bridge' => 'south-australia',
                'port-pirie' => 'south-australia',
                'mount-gambier' => 'south-australia',
                'victor-harbor' => 'south-australia',
                'launceston' => 'tasmania',
                'devonport' => 'tasmania',
                'burnie' => 'tasmania',
                'mildura' => 'victoria',
                'bendigo' => 'victoria',
                'ballarat' => 'victoria',
                'shepparton' => 'victoria',
                'wangaratta' => 'victoria',
                'wodonga' => 'victoria',
                'geelong' => 'victoria',
                'warrnambool' => 'victoria',
                'geraldton' => 'western-australia',
                'albany' => 'western-australia',
                'bunbury' => 'western-australia',
                'kalgoorlie-boulder' => 'western-australia',
                'central-coast' => 'new-south-wales',
        );
        if(isset($listMapping[$city]) && $listMapping[$city]) {
            return $listMapping[$city];
        }
        else {
            return 'australia';
        }
    }
    
    /**
     * Convert datetime
     */
    public static function convertDateTime($filterTime, $filterDate)
    {
        $filter_time_arr = explode(':', substr_replace($filterTime, ':', 2, 0));
        $hour = intval($filter_time_arr[0]);
        $hour_label = ($hour > 12) ? ($hour - 12) : $hour;
        $hour_label = intval($hour_label) == 0 ? 12 : $hour_label;
        $time = sprintf("%s%s:%s%s", ($hour_label < 10 ? '0' : ''), $hour_label, $filter_time_arr[1], ($hour < 12 ? 'am' : 'pm'));
        $time_label = sprintf("%s%s:%s%s", ($hour_label < 10 ? '0' : ''), $hour_label, $filter_time_arr[1], ($hour < 12 ? 'AM' : 'PM'));
        $date = new \DateTime(date('Y') . '-' . $filterDate);
        $timeText = $time;
        $dateText = $date->format('d F Y');
        return array($timeText, $dateText);
    }
    
    static public function getKeyOnAWSForEatability($id) {
        $length = strlen($id);
        if ($length < 9) {
            for ($i = 0; $i < (9 - $length); $i++) {
                $id = '0' . $id;
            }
        }
        $id_arr = str_split($id, 3);
        $format = self::$_container->getParameter('aws_s3_file_key_name_format_for_eatability');
        $format = str_replace('[slot0]', $id_arr[0], $format);
        $format = str_replace('[slot1]', $id_arr[1], $format);
        $format = str_replace('[slot2]', $id_arr[2], $format);
        //$result = 'users/profile_images/'.$id_arr[0].'/'.$id_arr[1].'/'.$id_arr[2].'/original';
        return $format;
    }
    static public function getUrlTempOnCDNEatability($fileName) {
        $key = self::getKeyTempOnAWS($fileName);
        $domains = self::$_container->getParameter('cdn_links_accept');
        $index = array_rand($domains);
        $url = "http://{$domains[$index]}/profile/{$key}";
        return $url;
    }

    /**
     * Get timeslot from tabledb
     *
     * @param $date
     * @param $bookingId
     * @param int $numTimeSlot
     * @return array
     */
    static public function parseTimeSlotsTableDB($date, $bookingId, $numTimeSlot = 4)
    {
        $timeSlots = array();
        $timeSlotParam = array(
            'date' => $date->format('Y-m-d'),
            'restaurant_id' => $bookingId
        );
        $tableDbApi = self::$_container->get('table.db.api');
        $listTimeSlot = $tableDbApi->getListTimeSlot($timeSlotParam);
        $listTimeSlot = $tableDbApi->parseTimeSlotForSearch($listTimeSlot, $timeSlotParam['date']);
        $idx = 0;
        $timeslotValue = $date->format('Hi');
        foreach($listTimeSlot as $value) {
            $timeSlot = self::$_container->get('bizsearch.api')->setTimeSlotInfo($value);
            if($timeSlot['date'] && strtotime($timeSlot['date']) == strtotime($timeSlotParam['date'])
                && intval($timeSlot['time_value']) >= intval($timeslotValue)) {
                if(!$timeSlot['notfully']) {
                    continue;
                } else {
                    $timeSlots[] = $timeSlot;
                    $idx++;
                    if($idx == $numTimeSlot) {
                        break;
                    }
                }
            }
        }
        
        return $timeSlots;
    }

    /**
     * Add no follow for external links
     *
     * @author minh.nguyen@s3corp.com.vn
     * @param $link
     * @return string
     */
    public static function addNoFollowForExternalLink($link)
    {
        $ret = '';
        if (!empty($link)) {
            if (false === strpos($link, '://')) {
                $link = 'http://' . $link;
            }
            $onlyHostName = implode('.', array_slice(explode('.', parse_url($link, PHP_URL_HOST)), -2));
            $internalLinks = self::$_container->getParameter('internal_links');
            if (!empty($onlyHostName) && !in_array($onlyHostName, $internalLinks)) {
                $ret = ' rel="nofollow" ';
            }
        }

        return $ret;
    }

    /**
    * Get special tags of term, HGW-793
    */
    public static function getSpecialTag($term) {
        $patternSlugTerm = array('pork-free', 'halal');
        $slug = strtolower(self::convertToSlug($term));
        if (in_array($slug, $patternSlugTerm)) {
            return $term;
        }
        return '';
    }

    /**
     * Get special tags for SERP, HGW-794
     * @param $term
     * @return array
     */
    public static function getSpecialTagForSERP($term)
    {
        $regex = '/(?<=^|\s)(pork-free|pork free|no pork|no-pork)(?=\s|$)/im';
        $cuisine = array();
        preg_match_all($regex, $term, $matches, PREG_OFFSET_CAPTURE);
        if (isset($matches[0]) && $matches[0]) {
            foreach ($matches[0] as $row) {
                $term = str_replace($row[0], '', $term);
            }
            $cuisine[] = 'Pork Free';
        }
        $regex = '/(?<=^|\s)(halal)(?=\s|$)/im';
        preg_match_all($regex, $term, $matches, PREG_OFFSET_CAPTURE);
        if (isset($matches[0]) && $matches[0]) {
            foreach ($matches[0] as $row) {
                $term = str_replace($row[0], '', $term);
            }
            $cuisine[] = 'Halal';
        }
        $term = preg_replace('/^\s+|\s+$|\s+(?=\s)/', '', $term);

        return array($term, $cuisine);
    }
    
    public function saveCache($cache_key, $data, $cache_time = 0) {
        $cache = self::$_container->get('hgw.cache');
        return $cache->save($cache_key, $data, $cache_time);
    }
    public function fetchCache($cache_key, $force = false) {
        $cache = self::$_container->get('hgw.cache');
        // Check if refresh is enable, return null
        if ($force) {
            $cache->delete($cache_key);
            return null;
        }
        return $cache->fetch($cache_key);
    }
    public function deleteCache($cache_key) {
        $cache = self::$_container->get('hgw.cache');
        return $cache->delete($cache_key);
    }

    public function importCSV2Array($filename = '', $delimiter = ',')
    {
        $row = 0;
        $col = 0;
        $fields = $ret = array();
        $data = array_map("str_getcsv", preg_split('/[\r\n]+/', file_get_contents($filename)));
        if($data) {
            foreach ($data as $key => $row) {
                if (empty($fields)) 
                {
                    $fields = $row;
                    continue;
                }
                foreach ($row as $k=>$value) 
                {
                    $ret[$col][$fields[$k]] = $value;
                }
                $col++;
                unset($row);
            }
        }

        return $ret;
    }

    /**
     * Get display name of a user
     * @param  array $user
     * @return string
     */
    public function getUserDislayName($user)
    {
        $displayName = '';
        if(isset($user['nickname']) && !empty($user['nickname'])) {
            $displayName = $user['nickname'];
        }
        if(empty($displayName)) {
            if(isset($user['first_name']) && isset($user['last_name'])) {
                $displayName = $user['first_name'] . ' ' . $user['last_name'];
            }
        }
        if(empty($displayName) || trim($displayName) == "") {
            if(isset($user['facebookName']) && !empty($user['facebookName'])) {
                $displayName = $user['facebookName'];
            }
        }

        return $displayName;
    }
    
    /**
     * Parse Canonical Follow config FrontEnd Url
     * @author VuTran
     * @param type $original_channel
     * @param string $canonical
     * @return string
     */
    public function parseCanonical ($original_channel, $canonical) {
        $listCmsChannel = self::$_container->getParameter('cms_channel');
        $countryCode = Constant::COUNTRY_CODE_SINGAPORE;
        foreach ($listCmsChannel as $key => $channel) {
            if (strtolower(trim($original_channel)) == strtolower(trim($channel))) {
                $countryCode = $key;
                break;
            }
        }
        
        $frontendUrl = self::$_container->getParameter('frontend_url');
        $canonical = trim($frontendUrl[$countryCode], '/').$canonical;
        
        return $canonical;
    }
    
    /**
     * extract Domain by Host
     * @author VuTranQ
     * @param type $domain
     * @return string
     */
    public static function extractDomain($domain)
    {
        if (preg_match("/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i", $domain, $matches)) {
            return $matches['domain'];
        } else {
            return $domain;
        }
    }

    /**
     * extract Subdomains by host
     * @author VuTranQ
     * @param type $domain
     * @return string
     */
    public static function extractSubdomains($domain)
    {
        $subdomains = $domain;
        $domain = self::extractDomain($subdomains);

        $subdomains = rtrim(strstr($subdomains, $domain, true), '.');

        return $subdomains;
    }

    /**
     * check a url is black list or not
     * @param unknown $url
     * @param unknown $arrBl
     * @author tri.van
     */
    public static function checkUrlIsInBlackList($container, $url) {
        try {
            // get all black list
            $cache = $container->get('hgw.cache');
            $cacheKey = Constant::CACHE_KEY_PROFILE_VERIFY_BLOGGER .'_ALLBLACKLIST';
            $arrBls = $cache->fetch($cacheKey);
            if(!$arrBls) {
                $conn = $container->get ( 'doctrine' )->getConnection ();
                $query = 'SELECT url FROM blog_urls_black_list;';
                $db = $conn->executeQuery ($query);
                $arrBls = $db->fetchAll();
                $cache->save($cacheKey, $arrBls);
            }

            $arrUrl = parse_url($url);
            $host = str_replace("www.", "", $arrUrl['host']);
            $mainDomain = self::extractDomain($host);
            foreach ($arrBls as $arrBl) {
                $arrBL = parse_url($arrBl['url']);
                $hostBL = str_replace("www.", "", $arrBL['host']);
                if($hostBL == $host) {
                    return true;
                }
                if("*.".$mainDomain == $hostBL) {
                    return true;
                }
            }
        }
        catch ( \Exception $ex ) { echo $ex->getMessage(); }
        return false;
    }
}
