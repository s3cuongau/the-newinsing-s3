<?php

namespace inSing\DataSourceBundle\Utilities;

/**
 * Bitly Exception class
 *
 * @author	Tijs Verkoyen <php-bitly@verkoyen.eu>
 */
class BitlyException extends \Exception
{
}

?>