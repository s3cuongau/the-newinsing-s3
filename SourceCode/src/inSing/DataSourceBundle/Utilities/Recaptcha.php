<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace inSing\DataSourceBundle\Utilities;

require_once __DIR__ . '/recaptcha/recaptchalib.php';

/**
 * Description of Recaptcha
 *
 * @author Dat.dao
 */
class Recaptcha {

    protected $publicKey = null;
    protected $privateKey = null;
    protected $error = null;

    public function __construct($publickey, $privatekey) {
        $this->publicKey = $publickey;
        $this->privateKey = $privatekey;
    }

    public function generate() {
        return recaptcha_get_html($this->publicKey, $this->error);
    }

    public function verified() {
        if (isset($_POST["recaptcha_response_field"]) && $_POST["recaptcha_response_field"]) {
            $resp = recaptcha_check_answer($this->privateKey, $_SERVER["REMOTE_ADDR"]
                    , $_POST["recaptcha_challenge_field"], $_POST["recaptcha_response_field"]);

            if ($resp->is_valid) {
                //echo "You got it!";
                return true;
            } else {
                # set the error code so that we can display it
                $this->error = $resp->error;
                return false;
            }
        }
    }
    
    /**
     * 
     * @param string $response
     * @param string $challenge
     */
    public function isValid($response, $challenge) {
    	if (isset($response) && !empty($response)) {
    		$resp = recaptcha_check_answer($this->privateKey, $_SERVER["REMOTE_ADDR"]
                    , $challenge, $response);
    		if ($resp->is_valid) {
    			return true;
    		}
    	}
    	return false;
    }
    
    public function getError() {
        return $this->error;
    }

}
