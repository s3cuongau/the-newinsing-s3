<?php

namespace inSing\DataSourceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Content
 *
 * @ORM\Table(name="content")
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="inSing\DataSourceBundle\Repository\ContentRepository")
 */
class Content
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"comment":"auto increment"})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="module", type="string", length=255, nullable=false, options={"comment":"carousel, hgw"})
     */
    private $module;

    /**
     * @var integer
     *
     * @ORM\Column(name="content_type", columnDefinition="TINYINT(4) DEFAULT 1 NOT NULL", options={"comment":"1 : movies, 2 : events, 3 : restaurant, 4 : restaurant deals, 5 : hungry deals, 6 : article, 7 : gallery"})
     */
    private $contentType;

    /**
     * @var integer
     *
     * @ORM\Column(name="item_id", type="integer", nullable=false)
     */
    private $itemId;

    /**
     * @var string
     *
     * @ORM\Column(name="channel_name", type="string", length=255, nullable=true)
     */
    private $channelName;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true, options={"comment":"if module is carousel it is for taglines and every tagline is separated by | sign"})
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="content_url", type="string", length=255, nullable=true)
     */
    private $contentUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="image_url", type="string", length=255, nullable=true)
     */
    private $imageUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="editor", type="string", length=255, nullable=true, options={"comment":"Only for hgw module"})
     */
    private $editor;

    /**
     * @var integer
     *
     * @ORM\Column(name="slot", type="integer", nullable=false)
     */
    private $slot;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="publish_on", type="date", nullable=false)
     */
    private $publishOn;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="expire_on", type="date", nullable=true)
     */
    private $expireOn;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", columnDefinition="TINYINT(1) DEFAULT 1 NOT NULL", options={"comment":"0 : deleted, 1 : active"})
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="extra_field", type="text", nullable=true, options={"comment":"For store extra information. Json format"})
     */
    private $extraField;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=false)
     */
    private $updatedAt;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set module
     *
     * @param string $module
     * @return Content
     */
    public function setModule($module)
    {
        $this->module = $module;

        return $this;
    }

    /**
     * Get module
     *
     * @return string 
     */
    public function getModule()
    {
        return $this->module;
    }

    /**
     * Set contentType
     *
     * @param string $contentType
     * @return Content
     */
    public function setContentType($contentType)
    {
        $this->contentType = $contentType;

        return $this;
    }

    /**
     * Get contentType
     *
     * @return string 
     */
    public function getContentType()
    {
        return $this->contentType;
    }

    /**
     * Set itemId
     *
     * @param integer $itemId
     * @return Content
     */
    public function setItemId($itemId)
    {
        $this->itemId = $itemId;

        return $this;
    }

    /**
     * Get itemId
     *
     * @return integer 
     */
    public function getItemId()
    {
        return $this->itemId;
    }

    /**
     * Set channelName
     *
     * @param string $channelName
     * @return Content
     */
    public function setChannelName($channelName)
    {
        $this->channelName = $channelName;

        return $this;
    }

    /**
     * Get channelName
     *
     * @return string 
     */
    public function getChannelName()
    {
        return $this->channelName;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Content
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Content
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set contentUrl
     *
     * @param string $contentUrl
     * @return Content
     */
    public function setContentUrl($contentUrl)
    {
        $this->contentUrl = $contentUrl;

        return $this;
    }

    /**
     * Get contentUrl
     *
     * @return string 
     */
    public function getContentUrl()
    {
        return $this->contentUrl;
    }

    /**
     * Set imageUrl
     *
     * @param string $imageUrl
     * @return Content
     */
    public function setImageUrl($imageUrl)
    {
        $this->imageUrl = $imageUrl;

        return $this;
    }

    /**
     * Get imageUrl
     *
     * @return string 
     */
    public function getImageUrl()
    {
        return $this->imageUrl;
    }

    /**
     * Set editor
     *
     * @param string $editor
     * @return Content
     */
    public function setEditor($editor)
    {
        $this->editor = $editor;

        return $this;
    }

    /**
     * Get editor
     *
     * @return string 
     */
    public function getEditor()
    {
        return $this->editor;
    }

    /**
     * Set slot
     *
     * @param integer $slot
     * @return Content
     */
    public function setSlot($slot)
    {
        $this->slot = $slot;

        return $this;
    }

    /**
     * Get slot
     *
     * @return integer 
     */
    public function getSlot()
    {
        return $this->slot;
    }

    /**
     * Set publishOn
     *
     * @param \DateTime $publishOn
     * @return Content
     */
    public function setPublishOn($publishOn)
    {
        $this->publishOn = $publishOn;

        return $this;
    }

    /**
     * Get publishOn
     *
     * @return \DateTime 
     */
    public function getPublishOn()
    {
        return $this->publishOn;
    }

    /**
     * Set expireOn
     *
     * @param \DateTime $expireOn
     * @return Content
     */
    public function setExpireOn($expireOn)
    {
        $this->expireOn = $expireOn;

        return $this;
    }

    /**
     * Get expireOn
     *
     * @return \DateTime 
     */
    public function getExpireOn()
    {
        return $this->expireOn;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return Content
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set extraField
     *
     * @param string $extraField
     * @return Content
     */
    public function setExtraField($extraField)
    {
        $this->extraField = $extraField;

        return $this;
    }

    /**
     * Get extraField
     *
     * @return string 
     */
    public function getExtraField()
    {
        return $this->extraField;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Content
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Content
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}
