<?php

/**
 * RnRApiUtils class to help in calling RnR Api
 *
 */
namespace inSing\DataSourceBundle\Lib;

class RnRApiUtils
{
    private static $sessionTokenCache = "RnrSessionTokenCache";
    private static $sessionToken;

    /**
     * 4 SUBMISSION CREATE API
     *
     * Production - https://api.rnr.insing.com/submissions
     * Test - https://api.rnr.test.insing.com/submissions
     *
     * @author Vu Tran
     * @param $container
     * @param Review Submit Data $data
     *
     * @return Submission ID
     */
    static public function createSubmission( $container, $data ) {
        try {
            $session_token = self::getSessionToken($container);

            if (is_null($session_token))
                return array();

            $data['session_token'] = $session_token;

            //Get Recommendation Item List from API
            $result = self::doPostWrapper($container,
                            $container->getParameter('urlRnR') . "submissions", $container->getParameter('urlRnRAuthSecret'), $data
            );

            //Return if not SUCCESS
            if (!is_array($result)) {
                return array('status' => 0, 'resultMessage' => 'Request failed!');
            }

            return $result;
        } catch (\Exception $exc) {
            if (Common::getEnvironmentDev($container)) {
                echo $exc->getMessage();
                echo $exc->getTraceAsString();
            }
        }
    }


    /**
     * 6 SUBMISSION GET API - Get Review Information
     *
     * https://api.rnr.insing.com/submission/{submission_id}
     *
     * @author Vu Tran
     * @param $container
     * @param string $submission_id ( review_id )
     */
    static public function getSubmissionApi( $container, $submission_id, $more_info = false ) {
    	try {
    		$session_token = self::getSessionToken($container);

    		if (is_null($session_token))
    			return array();

    		//Get Recommendation Item List from API
    		$result = self::doGetWrapper($container,
    				$container->getParameter('urlRnR') . "submission/$submission_id",
    				$container->getParameter('urlRnRAuthSecret'),
    				array(
    						'session_token' => $session_token,
                            'more_info' => $more_info ? 'true' : 'false'
    				));

    		//Return if not SUCCESS
    		if (RestfulAPIHelper::HTTP_OK != $result['status'] || $result['submission'] == null)
    			return array();

            $userId = $result['submission']['user_id'];
            $user = isset($result['submission']['user']) ? $result['submission']['user'] : array();
            $userStatistics = array();
            // Get User Statistic
            if (!empty($userId)) {
                $userStatistics = self::getUserStatisticsApi($container, $userId);
            }
            $avatar = $container->getParameter('default_user_avatar_url');
            if (isset($user['avatar_id'])) {
                $avatar = $user['avatar_id'] ? $container->get('common.utils')->generateMediaUrl($user['avatar_id'], 'pc_50x50') : $avatar;
            }
            $nickName = isset($user['name']) ? $user['name'] : '';
            if(!$nickName && isset($user['facebook_name']) && $user['facebook_name']) $nickName = $user['facebook_name'];
            $fullName = $nickName;

            if (isset($result['submission']['review']['response']) && count($result['submission']['review']['response'])) {
                $charBreakDown   = array("\\n\\r", "\\r\\n","\\r","\\n", "\n\r", "\r\n","\r","\n");
                foreach ($result['submission']['review']['response'] as $k=>$response) {
                    $result['submission']['review']['response'][$k]['body'] = str_replace($charBreakDown, '<br />', "$response[body]");
                }
            }
            $result['submission']['user'] = array(
                'user_id' => !empty($userId) ? $userId : 0,
                'user_avatar' => $avatar,
                'user_name' => $nickName,
                'user_fullname' => $fullName,
                'user_review' => count($userStatistics) && isset($userStatistics[0]['review_count']) ? $userStatistics[0]['review_count'] : 0,
                'user_follow' => isset($user['followers_count']) ? $user['followers_count'] : 0,
            );

    		return $result['submission'];
    	} catch (\Exception $exc) {
            if (Common::getEnvironmentDev($container)) {
                echo $exc->getMessage();
                echo $exc->getTraceAsString();
            }
        }
    }

    /*
     * getSubMissionList()
     * $param $container, $more_params
     * return array submission
     */
    public static function getSubmissionList($container, $more_params = array())
    {
        $result_authenticate = self::authenticate($container);

        //Return if not SUCCESS
        if ( !$result_authenticate['session_token'] ) return array();

        $sessionToken =  $result_authenticate['session_token'];
        $params = array_merge(
            $more_params,
            array(
                'session_token' => $sessionToken,
                'secret'  => $container->getParameter('urlRnRAuthSecret'),
                'item_type' => Constant::FOOD,
                'more_info' => 'true'
            )
        );

        $submissionList = self::doGetWrapper($container,
            $container->getParameter('urlRnR').'submissions',
            $container->getParameter('urlRnRAuthSecret'),
            $params
        );

        //Return if not SUCCESS
        if ( RestfulAPIHelper::HTTP_OK != $submissionList['status'] ) return array();

        return $submissionList;
    }

    /**
     * 7 SUBMISSION UPDATE API
     *
     * Production - https://api.rnr.insing.com/submissions/{submission_id}
     * Test - https://api.rnr.test.insing.com/submissions/{submission_id}
     *
     * @author Vu Tran
     * @param $container
     * @param Review Submit Data $data
     *
     * @return Submission ID
     */
    static public function updateSubmission( $container, $submission_id, $data ) {
    	try {
    		$session_token = self::getSessionToken($container);

    		if (is_null($session_token))
    			return array();

    		$data['session_token'] = $session_token;

    		//Get Recommendation Item List from API
    		$result = self::doPostWrapper($container,
    				$container->getParameter('urlRnR') . "submission/$submission_id",
    				$container->getParameter('urlRnRAuthSecret'),
    				$data,
    				RestfulAPIHelper::RESTFUL_API_PUT
    		);

    		//Return if not SUCCESS
    		if (RestfulAPIHelper::HTTP_OK != $result['status'])
    			return array();

    		return $result;
    	} catch (\Exception $exc) {
            if (Common::getEnvironmentDev($container)) {
                echo $exc->getMessage();
                echo $exc->getTraceAsString();
            }
        }
    }

    /**
     * 18 ITEM LIST GET API
     *
     * https://api.rnr.insing.com/items
     *
     * @author thai.pham@s3corp.com.vn
     * @param $container
     * @param string $item_type ( Hgw : food )
     * @param Array $item_id ( Hgw : restaurant_id )
     */
    static public function getItemListApi( $container, $item_type, array $item_id_list, $category='', $perpage = 10, $more_info = false ) {
        try {
            $session_token = self::getSessionToken($container);

            if (is_null($session_token)) {
                return array();
            }

            $params = array(
                'session_token' => $session_token,
                'item_type' => Constant::FOOD,
                'page' => 1,
                'per_page' => $perpage,
                'item_id' => implode(',', $item_id_list),
                'more_info' => $more_info ? 'true' : 'false'
            );
            if (!empty($category)) {
                $params['category'] = $category;
            }

            //Get Recommendation Item List from API
            $result = self::doGetWrapper($container,
                $container->getParameter('urlRnR') . "items",
                $container->getParameter('urlRnRAuthSecret'),
                $params
            );

            //Return if not SUCCESS
            if (RestfulAPIHelper::HTTP_OK != $result['status'])
                return array();

            return $result['items'];
        } catch (\Exception $exc) {
            if (Common::getEnvironmentDev($container)) {
                echo $exc->getMessage();
                echo $exc->getTraceAsString();
            }
        }

    }

    /**
     * 19 ITEM GET API -> in Hgw that is restaurant information
     *
     * https://api.rnr.insing.com/item/{item_type}/{item_id}
     *
     * @author Vu Tran
     * @param $container
     * @param string $item_type ( Hgw : food )
     * @param string $item_id ( Hgw : restaurant_id )
     */
    static public function getItemApi( $container, $item_type, $item_id ) {
    	try {
    		$session_token = self::getSessionToken($container);

    		if (is_null($session_token))
    			return array();

                $item_type = Constant::FOOD;
    		//Get Recommendation Item List from API
    		$result = self::doGetWrapper($container,
    				$container->getParameter('urlRnR') . "item/$item_type/$item_id",
    				$container->getParameter('urlRnRAuthSecret'),
    				array(
    						'session_token' => $session_token
    				));

    		//Return if not SUCCESS
    		if (RestfulAPIHelper::HTTP_OK != $result['status'])
    			return array();

    		return $result['item'];
    	} catch (\Exception $exc) {
            if (Common::getEnvironmentDev($container)) {
                echo $exc->getMessage();
                echo $exc->getTraceAsString();
            }
        }

        return array();
    }

    /**
     * 20 ITEM RECOMMENDATION LIST API
     *
     * https://api.rnr.insing.com/item/{item_type}/{item_id}/recommendations
     *
     * @author Vu Tran
     * @param $container
     * @param string $item_type ( Hgw : food )
     * @param string $item_id ( Hgw : restaurant_id )
     */
    static public function getItemRecommendationListApi( $container, $item_type, $item_id, $options = null ) {
        //Add some defaults search Values if Options is NULL
        if (is_null($options)) {
            $options = array(
                'per_page' => 10,
                'keyword' => ''
            );
        }

        try {
            $session_token = self::getSessionToken($container);

            if (is_null($session_token))
                return array();

            $item_type = Constant::FOOD;

            //Get Recommendation Item List from API
            $result = self::doGetWrapper($container,
                $container->getParameter('urlRnR') . "item/$item_type/$item_id/recommendations",
            	$container->getParameter('urlRnRAuthSecret'),
            	array(
                    'session_token' => $session_token,
                    'per_page' => $options['per_page'],
                    'keyword' => $options['keyword']
			));

            //Return if not SUCCESS
            if (RestfulAPIHelper::HTTP_OK != $result['status'])
                return array();

            return $result['recommendations'];
        } catch (\Exception $exc) {
            if (Common::getEnvironmentDev($container)) {
                echo $exc->getMessage();
                echo $exc->getTraceAsString();
            }
        }
        return array();
    }

     /**
     * @author Vu Luu
     * @param $container
     * @param string $item_type ( Hgw : food )
     * @param string $item_id ( Hgw : restaurant_id )
     */
    static public function getItemRecommendationTotalApi( $container, $item_type, $item_id, $options = null ) {
        //Add some defaults search Values if Options is NULL
        try {
            $session_token = self::getSessionToken($container);

            if (is_null($session_token))
                return array();
            $item_type = Constant::FOOD;
            //Get Recommendation Item List from API
            $result = self::doGetWrapper($container,
                $container->getParameter('urlRnR') . "item/$item_type/$item_id/recommendations",
            	$container->getParameter('urlRnRAuthSecret'),
            	array(
                    'session_token' => $session_token,
                    'keyword' => $options['keyword']
			));

            //Return if not SUCCESS
            if (RestfulAPIHelper::HTTP_OK != $result['status'])
                return array();

            return $result['total'];
        } catch (\Exception $exc) {
            if (Common::getEnvironmentDev($container)) {
                echo $exc->getMessage();
                echo $exc->getTraceAsString();
            }
        }
        return array();
    }

    /**
     * 24 SUITABLE FOR LIST API
     *
     * https://api.rnr.insing.com/suitable_for/{item_type}
     *
     * @param string $item_type
     */
    static public function getSuitableForListApi( $container, $item_type )
    {
        try {
            $session_token = self::getSessionToken($container);

	        if (is_null($session_token) ) return array();

            $item_type = Constant::FOOD;
            //Get Suitable For List from API
            $suitable_for_list = self::doGetWrapper($container,
			    $container->getParameter('urlRnR'). "suitable_for/$item_type",
			    $container->getParameter('urlRnRAuthSecret'),
			    array(
                            'session_token' => $session_token
                            )
            );
            //Return if not SUCCESS
            if (RestfulAPIHelper::HTTP_OK != $suitable_for_list['status'])
                return array();

            return $suitable_for_list['suitable_fors'];
        } catch (\Exception $exc) {
            if (Common::getEnvironmentDev($container)) {
                echo $exc->getMessage();
                echo $exc->getTraceAsString();
            }
        }

        return null;
    }


    static public function getSuitableForListByItemIdApi( $container, $item_id, $page = 1, $per_page = 10, $item_type = Constant::FOOD )
    {
        try {
            $session_token = self::getSessionToken($container);

	        if (is_null($session_token) ) return array();

            $item_type = Constant::FOOD;
            //Get Suitable For List from API
            $suitable_for_list = self::doGetWrapper($container,
			    $container->getParameter('urlRnR'). "item/$item_type/$item_id/suitable_fors",
			    $container->getParameter('urlRnRAuthSecret'),
			    array(
                            'session_token' => $session_token,
                            'page' => $page,
                            'per_page' => $per_page
                            )
            );
            //Return if not SUCCESS
            if (RestfulAPIHelper::HTTP_OK != $suitable_for_list['status'])
                return array();

            return $suitable_for_list['suitable_fors'];
        } catch (\Exception $exc) {
            if (Common::getEnvironmentDev($container)) {
                echo $exc->getMessage();
                echo $exc->getTraceAsString();
            }
        }

        return array();
    }


    static public function authenticate($container)
    {
        try {
            $result_authenticate = self::doPostWrapper($container,
	    		$container->getParameter('urlRnRAuth'),
                        $container->getParameter('urlRnRAuthSecret'),
	    		array(
                        'app_id' => $container->getParameter('urlRnRAuthAppId'),
                        'secret' => $container->getParameter('urlRnRAuthSecret')
	    		),
	    		RestfulAPIHelper::RESTFUL_API_POST_AUTHENTICATE
            );

            if (RestfulAPIHelper::HTTP_OK != $result_authenticate['status'])
                return null;
            return array("session_token" => $result_authenticate['session_token'],
                "expire_at" => $result_authenticate['expire_at']
            );
        } catch (\Exception $exc) {
            if (Common::getEnvironmentDev($container)) {
                echo $exc->getMessage();
                echo $exc->getTraceAsString();
            }
        }
    }

    /**
     * Get session token
     * @author Date Tran
     * @return string sessionToken
     */
    static public function getSessionToken($container)
    {
        try {
            $cache = new HgwCache($container);
            $rnrSessionTokenKey = Constant::HGW_SESSTION_TOKEN_DATA_10;
            $sessionToken = $cache->fetch($rnrSessionTokenKey);

            if (empty($sessionToken)) {
                $result = self::authenticate($container);

                if(!empty($result) && count($result))
                {
                  $sessionToken = $result['session_token'];

                  $cacheTime = 300;
                  //cache data on $cacheTime
                  $cache->save($rnrSessionTokenKey, $sessionToken, $cacheTime);
                }
            }

            return $sessionToken;
        } catch (\Exception $exc) {
            if (Common::getEnvironmentDev($container)) {
                echo $exc->getMessage();
                echo $exc->getTraceAsString();
            }
        }

        return null;
    }

    /**
     * @static
     * @param $container
     * @param $suitableForName
     * @param string $category
     * @return array|Returns
     * author tuan.bui
     */
    public static function getRestaurantIdBySuitableFor($container, $suitableForName, $pageSize = 20, $category = '') {
        $session_token = self::getSessionToken($container);

        //Get Suitable For List from API
        $params = array(
            'session_token' => $session_token,
            'item_type' => 'food',
            'suitable_for'  => $suitableForName,
            'per_page' => $pageSize
        );
        if ($category) {
            $params['item_category'] = $category;
        }
        $suitable_for_list = self::doGetWrapper($container,
            $container->getParameter('urlRnR'). "items", $container->getParameter('urlRnRAuthSecret'), $params
        );

        //Return if not SUCCESS
        if (RestfulAPIHelper::HTTP_OK != $suitable_for_list['status'])
            return array();
        return $suitable_for_list;
    }

    public static function getTop5RestaurantsBySuitableFor($container, $suitableForName){
        $session_token = self::getSessionToken($container);
        if (is_null($session_token) ) return array();
        $params = array(
            'session_token' => $session_token,
            'suitable_for'  => $suitableForName,
        );
        $suitableForName = urlencode($suitableForName);

        $dataTop5Restaurants = self::doGetWrapper($container, $container->getParameter('urlRnR'). "items/top5/food", $container->getParameter('urlRnRAuthSecret'), $params
        );
        //Return if not SUCCESS
        if (RestfulAPIHelper::HTTP_OK != $dataTop5Restaurants['status'])
            return array();

        return $dataTop5Restaurants;
    }

    /**
     * author sang.nguyen
     * @param $reviewId
     * @return array|Returns
     */
    public static function getReviewDetailApi($container, $reviewId, $more_info = false) {
        $token = RnRApiUtils::getSessionToken($container);
        try {
            $fields = array('session_token' => $token, 'more_info' => $more_info ? 'true' : 'false');
            $data = self::doGetWrapper($container, $container->getParameter('urlRnR') . 'review/' . $reviewId, $container->getParameter('urlRnRAuthSecret'), $fields);

        } catch (\Exception $exc) {
            if (Common::getEnvironmentDev($container)) {
                echo $exc->getMessage();
                echo $exc->getTraceAsString();
            }
            return array();
        }

        return $data;
    }

    public static function getLatestReviewApi($container, $category = '', $per_page = 10, $more_info = false)
    {
        $token = self::getSessionToken($container);
        $results = array();
        try {

            $fields = array('session_token' => $token, 'item_category' => $category, 'page' => 1, 'per_page' => $per_page, 'item_type' => Constant::FOOD, 'more_info' => $more_info ? 'true' : 'false');
            $results = self::doGetWrapper($container, $container->getParameter('urlRnR') . 'reviews', $container->getParameter('urlRnRAuthSecret'), $fields);
        } catch (\Exception $exc) {
            if (Common::getEnvironmentDev($container)) {
                echo $exc->getMessage();
                echo $exc->getTraceAsString();
            }
        }
        return $results;
    }

    /**
     *
     * @param type $container
     * @param type $category
     * @param int $per_page
     * @param type $page
     * @param type $itemId
     * @param type $submissionFilter
     * @param INT $userIdFilter: ID of a user
     * @return array submissions
     */
    public static function getLatestSubmissions($container, $category = '', $per_page = 3, $page = 1, $itemId = 0, $submissionFilter = '', $userIdFilter = '')
    {
        $token = self::getSessionToken($container);
        $listItem = array();
        $all_data = array('submissions' => array(), 'total' => 0);
        try {
            $fields = array('session_token' => $token, 'page' => $page, 'item_type' => Constant::FOOD, 'per_page' => $per_page, 'more_info' => 'true');
            if ($category) {
                $fields['item_category'] = $category;
            }
            if ($userIdFilter) {
                $fields['user_id'] = $userIdFilter;
                $fields['user_type'] = 'insing';
            }

            switch ($submissionFilter) {
              case ReviewUtils::SUBMISSION_TYPE_FAVOURITE:
                $fields['filter'] = 'favourite';
                $per_page = 1;
                break;
              case ReviewUtils::SUBMISSION_TYPE_RECOMENDATION:
                $fields['filter'] = 'recommendation';
                break;

              case ReviewUtils::SUBMISSION_TYPE_PHOTOS:
                $fields['filter'] = 'photo';
                break;
              case ReviewUtils::SUBMISSION_TYPE_ALL:
                break;
              default:
                $fields['filter'] = 'review';
                break;
            }

            if($itemId)
            {
              $fields['item_id'] = $itemId;
            }

            $data = self::doGetWrapper($container, $container->getParameter('urlRnR') . 'submissions', $container->getParameter('urlRnRAuthSecret'), $fields);

            if ($data) {
                $listItem = array();
                if ($data['status'] == 200) {
                    $listItem = $data['submission'];
                }

                // IF get data success
                if (is_array($listItem)) {

                  foreach ($listItem as $key => $review) {
                    $listItem[$key]['time_ago'] = $review['created_dt'];
                  }
                }
            }
            $all_data = array('submissions' => $listItem, 'total' => isset($data['total']) ? $data['total'] : 0);
        } catch (\Exception $exc) {
            if (Common::getEnvironmentDev($container)) {
                echo $exc->getMessage();
                echo $exc->getTraceAsString();
            }
            return $all_data;
        }

        return $all_data;
    }

    // Action content like getLatestSubmissions
    /*
     * @author BienTran
     * @description Get lastest photos according to the different submissions.Pasing $onlyphoto argument aim to get restaurants info belonging to submission id.
     */
    public static function getLatestPhotosApi($container, $page = 1, $per_page = 20, $itemId = 0, $category = '', $userId = 0)
    {
        $listItem = self::getLatestSubmissions($container, $category, $per_page, $page, $itemId, ReviewUtils::SUBMISSION_TYPE_PHOTOS, $userId);
        return $listItem['submissions'];
    }

    public static function getUserStatisticsApi($container, $userId, $userType = 'insing')
    {
        try {
            $token = self::getSessionToken($container);
            $fields = array('session_token' => $token, 'user_id' => $userId, 'user_type' => $userType);
            $data = self::doGetWrapper($container, $container->getParameter('urlRnR') . 'users/statistics', $container->getParameter('urlRnRAuthSecret'), $fields);
            $result = array();
            if ($data['status'] == 200) {
                $result = $data['users'];
            }
        } catch (\Exception $exc) {
            if (Common::getEnvironmentDev($container)) {
                echo $exc->getMessage();
                echo $exc->getTraceAsString();
            }
            return array();
        }

        return $result;
    }
    /**
     * get reviews from user id
     * @param Container $container
     * @param int $userId
     * @return array[]
     */
    public static function getReviewsFromUserId($container, $userId, $userType = 'insing', $category = 'Editorial_Restaurants')
    {
        $token = self::getSessionToken($container);
        $fields = array('session_token' => $token, 'user_id' => $userId, 'user_type' => $userType, 'item_type' => Constant::FOOD, 'item_category' => $category);
        $data = self::doGetWrapper($container, $container->getParameter('urlRnR') . 'reviews', $container->getParameter('urlRnRAuthSecret'), $fields);


        return $data;
    }
    /**
     * Get submission from specific user id
     * @param Container $container
     * @param int $userId
     * @param String $userType
     * @return array('item_id', 'isVote', 'title', 'description', 'recommendations', 'create_at') | null if nothing
     * @author Dat Tran
     */
    public static function getSubmissionsFromUserId($container, $userId, $userType = 'insing', $per_page = 1)
    {
        $results = null;
        try {

            $token = self::getSessionToken($container);
            $fields = array('session_token' => $token, 'filter' => 'review', 'user_id' =>  $userId ,
                            'user_type' => $userType, 'item_type' => Constant::FOOD, 'page' => 1, 'per_page' => $per_page);
            $data = self::doGetWrapper($container, $container->getParameter('urlRnR') . 'submissions', $container->getParameter('urlRnRAuthSecret'), $fields);

            if (isset($data['submission']) && (count($data['submission'] > 0)) )
            {
                //var_dump($data['submission'][0]);die();
                $results = array(
                                'item_id' => $data['submission'][0]['item_id'],
                                'isVote' => isset($data['submission'][0]['vote']) ? $data['submission'][0]['vote']['thumbs_up'] : null,
                                'title' => isset($data['submission'][0]['review']) ? $data['submission'][0]['review']['title'] : '',
                                'description' => isset($data['submission'][0]['review']) ? $data['submission'][0]['review']['body'] : '',
                                'recommendations' => isset($data['submission'][0]['recommendations']) ? $data['submission'][0]['recommendations'][0]['name'] : '',
                                'create_at' => date("Y-m-d H:i:s", $data['submission'][0]['created_dt'])
                                );


            }

        } catch (\Exception $exc) {
            if (Common::getEnvironmentDev($container)) {
                echo $exc->getMessage();
                echo $exc->getTraceAsString();
            }
            return array();
        }
        return $results;
    }

    public static function getBusinessStatistic($container, $itemId) {

        $token = self::getSessionToken($container);

        $fields = array('session_token' => $token, 'item_id' => is_array($itemId) ? implode(',', $itemId) : $itemId, 'item_type' => Constant::FOOD , 'page' => 1, 'per_page' => 10);

        try {
            $data = self::doGetWrapper($container, $container->getParameter('urlRnR') . 'items', $container->getParameter('urlRnRAuthSecret'), $fields);

            if ($data['status'] == 200 && $data['total'] > 0) {
                return $data['items'];
            }
        } catch (Exception $exc) {
            if (Common::getEnvironmentDev($container)) {
                echo $exc->getMessage();
                echo $exc->getTraceAsString();
            }
        }

        return array();
    }

    public static function postManagementResponse($container, $reviewId, \InSing\HgwBundle\Entity\ManagementResponse $response) {
        //review/{review_id}/response

        $token = self::getSessionToken($container);

        $fields = array('session_token' => $token, 'name' => $response->getName(), 'email' => $response->getEmail(), 'contact' => $response->getPhone(), 'body' => $response->getContent());

        try {
            $data = self::doPostWrapper($container, $container->getParameter('urlRnR') . 'review/' . $reviewId . '/response', $container->getParameter('urlRnRAuthSecret'), $fields);
            if ($data['status'] == 200 && isset($data['response_id']) && $data['response_id']) {
                return true;
            }
        } catch (Exception $exc) {
            if (Common::getEnvironmentDev($container)) {
                echo $exc->getTraceAsString();
            }
        }

        return false;
    }

    public static function getListViolationTerms($container, $termType = 'review') {
        //review/{review_id}/response

        $token = self::getSessionToken($container);

        $fields = array('session_token' => $token);

        try {
            $data = self::doGetWrapper($container, $container->getParameter('urlRnR') . 'violation_terms/' . $termType, $container->getParameter('urlRnRAuthSecret'), $fields);
            if ($data['status'] == 200 && isset($data['violation_terms']) && count($data['violation_terms'])) {
                return $data['violation_terms'];
            }
        } catch (Exception $exc) {
            if (Common::getEnvironmentDev($container)) {
                echo $exc->getTraceAsString();
            }
        }

        return array();
    }

    public static function postAbusesReport($container, $reviewId, $userId, $violationTerm, $body, $userType = 'insing') {
        //review/{review_id}/response

        $token = self::getSessionToken($container);

        $fields = array('session_token' => $token, 'user_id' => $userId, 'user_type' => $userType, 'review_id' => $reviewId, 'violation_term' => $violationTerm, 'body' => $body);

        try {
            $data = self::doPostWrapper($container, $container->getParameter('urlRnR') . 'abuses', $container->getParameter('urlRnRAuthSecret'), $fields);
            if ($data['status'] == 200 && isset($data['abuse_id']) && $data['abuse_id']) {
                return true;
            }
        } catch (Exception $exc) {
            if (Common::getEnvironmentDev($container)) {
                echo $exc->getTraceAsString();
            }
        }

        return false;
    }

    public static function postHelpfulReview($container, $reviewId, $userId, $userType = 'insing') {
        //review/{review_id}/response

        $token = self::getSessionToken($container);

        $fields = array('session_token' => $token, 'user_id' => $userId, 'user_type' => $userType);
        $data = null;
        try {
            $data = self::doPostWrapper($container, $container->getParameter('urlRnR') . 'review/' . $reviewId . '/helpful', $container->getParameter('urlRnRAuthSecret'), $fields);

        } catch (Exception $exc) {
            if (Common::getEnvironmentDev($container)) {
                echo $exc->getTraceAsString();
            }
        }

        return $data;
    }


    public static function postLikePhoto($container, $photoId, $userId, $userType = 'insing') {
        $token = self::getSessionToken($container);

        $fields = array('session_token' => $token, 'user_id' => $userId, 'user_type' => $userType);

        try {
            $data = self::doPostWrapper($container, $container->getParameter('urlRnR') . 'photo/' . $photoId . '/like', $container->getParameter('urlRnRAuthSecret'), $fields);
            if ($data['status'] == 200 && isset($data['photo_id']) && $data['photo_id']) {
                return true;
            }
        } catch (Exception $exc) {
            if (Common::getEnvironmentDev($container)) {
                echo $exc->getTraceAsString();
            }
        }

        return false;
    }

    public static function getRnrPhoto($container, $photoId, $more_info = false) {
        $token = self::getSessionToken($container);

        $fields = array('session_token' => $token, 'more_info' => $more_info ? 'true' : 'false');

        try {
            $data = self::doGetWrapper($container, $container->getParameter('urlRnR') . 'photo/' . $photoId , $container->getParameter('urlRnRAuthSecret'),$fields);
            if ($data['status'] == 200) {
                return $data;
            }
        } catch (Exception $exc) {
            if (Common::getEnvironmentDev($container)) {
                echo $exc->getTraceAsString();
            }
        }

        return false;
    }


    /*
     * @author hoc.nguyen
     * @description This function just only returns an array of different submissions.
     *
     */

    public static function getSubmissionListPhotos($container, $per_page = 20, $page = 1)
    {
        $result = array();
        try {
            $userIds = array();
            $userMobileIds = array();
            $restaurantIds = array();
            $token = self::getSessionToken($container);
            $fields = array('session_token' => $token, 'filter' => 'photo', 'page' => $page, 'per_page' => $per_page, 'item_type' => Constant::FOOD, 'more_info' => 'true');
            $data = self::doGetWrapper($container, $container->getParameter('urlRnR') . 'submissions', $container->getParameter('urlRnRAuthSecret'), $fields);
            if ($data) {
                if ($data['status'] == 200 && is_array($data['submission'])) {
                    foreach ($data['submission'] as $review) {
                        if (!count($review)) {
                            continue;
                        }
                        $restaurantIds[] = is_numeric($review['item_id']) ? $review['item_id'] : Common::urlIdToId($review['item_id']);

                        if ($review['user_type'] == 'mobile') {
                            $temp_id = is_numeric($review['user_id']) ? $review['user_id'] : Common::urlIdToId($review['user_id']);
                            // This check ensures no duplicate user_id will be queried
                            if (!in_array($temp_id, $userMobileIds)) {
                                $userMobileIds[] = $temp_id;
                            }
                        } else {
                            $temp_id = is_numeric($review['user_id']) ? $review['user_id'] : Common::urlIdToId($review['user_id']);
                            if (!in_array($temp_id, $userIds)) {
                                $userIds[] = $temp_id;
                            }
                        }
                        $review['time_ago'] = $review['created_dt'];
                        $result[] = $review;
                    }
                }
            }
        } catch (\Exception $exc) {
            if (Common::getEnvironmentDev($container)) {
                echo $exc->getMessage();
                echo $exc->getTraceAsString();
            }
            return array();
        }
        return $result;
    }

    /**
     * get User Photos By restaurant Id
     * @param $container
     * @param $itemId
     * @param $per_page
     * @param $page
     */
    public static function getUserPhotosByRestaurantId($container, $itemId = '', $per_page = 10, $page = 1) {
        $result = array();
        try {
            $token = self::getSessionToken($container);
            $imageCache = new HgwCache($container);
            $cacheConstant = Constant::IBL_REVIEW_PHOTOS .$itemId . '_perpage' . $per_page . '_page' . $page;
            if (!$imageCache->contains($cacheConstant)) {
                $fields = array(
                	'session_token' => $token,
                	'item_type' => Constant::FOOD,
                    'item_id' => $itemId,
                    'per_page' => $per_page,
                    'page' => $page,
                );

                $results = self::doGetWrapper($container, $container->getParameter('urlRnR') . 'photos' , $container->getParameter('urlRnRAuthSecret'),$fields);

                $imageCache->save($cacheConstant, $results,$container->getParameter('ibl_review_image_cache_time'));
            } else {
                $results = $imageCache->fetch($cacheConstant);
            }
            return $results;
        } catch (\Exception $exc) {
            if (Common::getEnvironmentDev($container)) {
                echo $exc->getMessage();
            }
            return null;
        }
    }

    /**
     *
     * getRnrPhotoList
     * @param $container
     * @param $itemId
     * @param $userId
     * @param $moreInfo boolean (true => return user Info)
     * @param $page
     * @param $perPage
     */
    public static function getRnrPhotoList($container, $itemId = 0, $userId = 0, $moreInfo = true, $page = 1, $perPage = 10) {
        try {
            $cache = new HgwCache($container);
            $cacheKey = Constant::IBL_REVIEWER_PHOTOS .$itemId . '_perpage' . $perPage . '_page' . $page;

            if (!$cache->contains($cacheKey)) {
                $token = self::getSessionToken($container);

                $fields = array(
                	'session_token' => $token,
                    'page' => $page,
                    'per_page' => $perPage,
                    'item_type' => Constant::FOOD,
                    'more_info' => $moreInfo ? 'true' : 'false');

                if($itemId)
                {
                  $fields['item_id'] = $itemId;
                }
                if($userId)
                {
                  $fields['user_id'] = $userId;
                }

                $data = self::doGetWrapper($container, $container->getParameter('urlRnR') . 'photos' , $container->getParameter('urlRnRAuthSecret'),$fields);
                if ($data['status'] == 200) {
                    if ($moreInfo == true) {
                        for ($i=0, $n = count($data['photos']); $i < $n; $i++ ) {
                            if (empty($data['photos'][$i]['user']['name'])) {
                                if (!empty($data['photos'][$i]['user']['facebook_name'])) {
                                    $data['photos'][$i]['user']['name'] = $data['photos'][$i]['user']['facebook_name'];
                                } else {
                                    $data['photos'][$i]['user']['name'] = '';
                                }
                            }
                        }
                    }
                    $cache->save($cacheKey, $data, $container->getParameter('ibl_review_image_cache_time'));
                }
            } else {
                $data = $cache->fetch($cacheKey);
            }
            return $data;
        } catch (Exception $exc) {
            if (Common::getEnvironmentDev($container)) {
                echo $exc->getTraceAsString();
            }
        }

        return array();
    }

    protected static function doGetWrapper($container, $url, $secret, $fields) {
        $res = RestfulAPIHelper::doGet($url, $secret, $fields, $container);
        if ($res['status'] == RestfulAPIHelper::SESSION_TOKEN_TIMEOUT || $res['status'] == RestfulAPIHelper::HTTP_UNAUTHORIZED) {
            $sessionTokenCache = new HgwCache($container);
            $sessionTokenCache->delete($container->getParameter("RnrSessionTokenCache"));
            $sessionToken = self::getSessionToken($container);
            $fields['session_token'] = $sessionToken;
            $res = RestfulAPIHelper::doGet($url, $secret, $fields);
        }
        return $res;
    }

    protected static function doPostWrapper($container, $url, $secret, $fields, $post_method = RestfulAPIHelper::RESTFUL_API_POST_JSON) {
        $res = RestfulAPIHelper::doPost($url, $secret, $fields, $post_method, $container);
        if ($res['status'] == RestfulAPIHelper::SESSION_TOKEN_TIMEOUT || $res['status'] == RestfulAPIHelper::HTTP_UNAUTHORIZED) {
            $sessionTokenCache = new HgwCache($container);
            $sessionTokenCache->delete($container->getParameter("RnrSessionTokenCache"));
            $sessionToken = self::getSessionToken($container);
            $fields['session_token'] = $sessionToken;
            $res = RestfulAPIHelper::doPost($url, $secret, $fields, $post_method);
        }
        return $res;
    }
}
