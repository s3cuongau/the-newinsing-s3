<?php
namespace inSing\DataSourceBundle\Lib;

class Constant
{
    //For STATUS active / inactive
    const STATUS_ACTIVE        = 1;
    const STATUS_INACTIVE      = 0;
    const STATUS_ACTIVE_TEXT   = 'Active';
    const STATUS_INACTIVE_TEXT = 'Inactive';

    const BLOCK_NUM                = 5;
    const API_CONTENT_TYPE_ARTICLE = 0;
    const API_CONTENT_TYPE_GALLERY = 1;
    const API_CONTENT_TYPE_EVENT   = 2;
    const API_CONTENT_TYPE_MOVIE   = 3;

    const CUSTOM_PAGE_STATUS_DELETED     = 0;
    const CUSTOM_PAGE_STATUS_DRAFT       = 1;
    const CUSTOM_PAGE_STATUS_PUBLISHED   = 2;
    const CUSTOM_PAGE_STATUS_SUSPENDED   = 3;
    const CUSTOM_PAGE_STATUS_ARCHIVED    = 4;
    const CUSTOM_PAGE_IS_NOT_ADVERTORIAL = 0;
    const CUSTOM_PAGE_IS_ADVERTORIAL     = 1;

    const CUSTOM_PAGE_TOP_SPACE    = 'Top';
    const CUSTOM_PAGE_LEFT_SPACE   = 'Left';
    const CUSTOM_PAGE_RIGHT_SPACE  = 'Right';
    const CUSTOM_PAGE_BOTTOM_SPACE = 'Bottom';

    const TEMPLATE_HERO          = 'HERO';
    const TEMPLATE_STORY_TILES   = 'STORY_TILES';
    const TEMPLATE_STORY_LIST    = 'STORY_LIST';
    const TEMPLATE_MPU1_ADS      = 'MPU_AD';
    const TEMPLATE_MPU2_ADS      = 'MPU2_AD';
    const TEMPLATE_MARKETING_ADS = 'MARKETING_AD';
    
    const FEATURED_CHANNEL = 'insing.com';
    const FEATURED_MOVIES = 'movies';
    const FEATURED_EVENTS = 'events';
    const FEATURED_BUSINESS = 'business';

    const DEVICE_TYPE_MOBILE        = 'mobile';
    const DEVICE_TYPE_DESKTOP       = 'desktop';
    const DEVICE_TYPE_TABLET        = 'tablet';
    const DEVICE_TYPE_MOBILE_IOS    = 'ios';

    const BROWSER_SAFARI_DESKTOP    = 'safari_desktop';

    const SERIES_ADVERTORIAL = 'Advertorial';

    // GA
    const GA_HEADER     = 'Header';
    const GA_HOMEPAGE   = 'Homepage';
    const GA_FOOTER     = 'Footer';

    //FOR render GPT Ads
    //View Type
    const VIEW_TYPE_MPU1				= 'mpu1';
    const VIEW_TYPE_MPU2				= 'mpu2';
    const VIEW_TYPE_MKTG				= 'mktg';
    const VIEW_TYPE_SPONSOR_DETAILS		= 'sponsor_details';
    const VIEW_TYPE_MOBILE_AD			= 'mobile_ad_unit';
    const VIEW_TYPE_MOBILE_AD2			= 'mobile_ad_unit2';
    const VIEW_TYPE_MPU1MOBILE			= 'mpu1moblie';

    //Orientation
    const ORIENTATION_PORTRAIT = 'portrait';
    const ORIENTATION_LANDSCAPE = 'landscape';
    const ORIENTATION_MOBILE = 'mobile';

    /**
     * @author Cuong.Bui
     */
    public static function getCustomPageStatus()
    {
        return array(
            self::CUSTOM_PAGE_STATUS_DELETED   => 'Deleted',
            self::CUSTOM_PAGE_STATUS_DRAFT     => 'Draft',
            self::CUSTOM_PAGE_STATUS_PUBLISHED => 'Published',
            self::CUSTOM_PAGE_STATUS_SUSPENDED => 'Suspended',
            self::CUSTOM_PAGE_STATUS_ARCHIVED  => 'Archived'
        );
    }
   
    /**
     * @author Cuong.Bui
     */
    public static function getChannels(array $channel_name)
    {
        $ret = array();
        $ret[''] = '- Please select -';

        foreach ($channel_name as $key => $value) {
            $ret[$key] = $value;
        }

        return $ret;
    }

    /**
     * @author Vu.Luu
     */
    public static function getContentTypes()
    {
        return array(
            1 => 'Movie',
            2 => 'Event',
            3 => 'Restaurant',
//            4 => 'Rest Deal',
            5 => 'Rest HungryDeal',
            6 => 'Article',
            7 => 'Gallery'
        );
    }

    /**
     * @author Vu.Luu
     */
    public static function getHGWContentTypes()
    {
        return array(
            3 => 'Restaurant',
//            4 => 'Rest Deal',
            5 => 'Rest HungryDeal',
            6 => 'Article',
            7 => 'Gallery'
        );
    }

    /**
     * @author	Vu Luu
     */
    public static function getAllCarouselSlot()
    {
        return array(
            1  => 'Slot 1',
            2  => 'Slot 2',
            3  => 'Slot 3',
            4  => 'Slot 4',
            5  => 'Slot 5'
        );
    }

    /**
     * @author	Vu Luu
     */
    public static function getAllHGWSlot()
    {
        return array(
            1  => 'Slot 1',
            2  => 'Slot 2',
            3  => 'Slot 3',
            4  => 'Slot 4',
            5  => 'Slot 5',
            6  => 'Slot 6',
            7  => 'Slot 7',
            8  => 'Slot 8'
        );
    }

    /**
     * @author	Vu Tran
     * @since	Jun 26, 2013 5:29:35 PM 2013
     */
    public static function getAllSubStorySlot()
    {
    	return array(
            1  => 'Slot 1',
            2  => 'Slot 2',
            3  => 'Slot 3',
            4  => 'Slot 4',
            5  => 'Slot 5'
    	);
    }

    /**
     * Ex :
     * @author	phuc.nguyen
     * @method	getApiContentType
     * @return
     * @since	Jun 27, 2013 4:40:01 PM 2013
     */
    public static function getApiContentTypes()
    {
        return array(
            self::API_CONTENT_TYPE_ARTICLE => 'Article ID',
            self::API_CONTENT_TYPE_GALLERY => 'Gallery ID'
        );
    }
}