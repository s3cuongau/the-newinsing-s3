<?php

namespace inSing\DataSourceBundle\Lib;

class AwsApi extends RnRApiUtils
{

    /**
	 * Get File Extension from MimeType
	 *
	 * @author  Vu Tran
	 * @param string $mime_type
	 */
	public static function getFileExtensionFromMimeType( $mime_type ) {
		//Create Extension Array
		$known_extension = array(
			"image/png" 	=> "png",
			"image/jpg"	=> "jpg",
			"image/jpeg"	=> "jpg",
			"image/pjpeg"	=> "jpg",
            "image/gif"	=> "gif"
		);

		return isset($known_extension[$mime_type]) ? $known_extension[$mime_type] : null;
	}

    /**
     * Create photo array object from Uploaded Photo
     *
     * @param $container
     * @param UploadedFile $photo
     * @param $upload_token
     */
    public static function createPhotoObject($container, $photo)
    {
//        try {

            // Get Resource file uploaded

            $resource = fopen($photo->getPathname(), 'r+');


            $mime_type = self::getMimeType($photo->getClientOriginalName(), $resource);
            $photo_extension = self::getFileExtensionFromMimeType($mime_type);

            if (!$photo_extension) {
                return null;
            }

            $aws_id = $container->getParameter('aws_s3_id');
            $aws_secret = $container->getParameter('aws_s3_secret');
            $dir_source = $container->getParameter('aws_s3_dir_name');
            $bucket = $container->getParameter('aws_s3_bucket_name');

            $client = new InSingAwsS3($aws_id, $aws_secret);



            $md5Raw = md5(stream_get_contents($resource), true);
            $md5Hex = bin2hex($md5Raw); // Used as part of file storage name, if this one is file name, we can check duplicate file what is uploaded by user
            $md5Base64 = base64_encode($md5Raw); // Amazon requires this format, using for MD5 checksum

            $file_name = $md5Hex . '_' . time(); // Accept file duplicate
            // Key file sample: hgwweb_temp/food/item_id/user_id/file_name.jpg, hgwweb_temp is temp folder
            //$key_file = $dir_source . join('/', array($save_folder, $file_name . '.' . $photo_extension));
            $key_file = $dir_source . '/' . date('Ymd') . '/' .  $file_name . '.' . $photo_extension;

            // Upload to server and wait to upload successfully
            $guzzleObject = $client->putResource($bucket, $key_file, $resource, true, true);

            // Check upload successfully
            if (!$guzzleObject) {
                return null;
            }

            $photo_size = getimagesize($photo->getPathname());

            //Create Object for this Photo & store it into SESSION
            $photo_arr = array();
            $photo_arr['path'] = $key_file;
//        } catch (\Exception $exc) {
//            return null;
//        }

        return $photo_arr;
    }

    public static function getMimeType($filename, $filestream)
    {
        $type = false;
        // Fileinfo documentation says fileinfo_open() will use the
        // MAGIC env var for the magic file
        if (extension_loaded('fileinfo') && isset($_ENV['MAGIC']) &&
        ($finfo = finfo_open(FILEINFO_MIME, $_ENV['MAGIC'])) !== false)
        {
            if (($type = finfo_buffer($finfo, $filestream)) !== false)
            {
                // Remove the charset and grab the last content-type
                $type = explode(' ', str_replace('; charset=', ';charset=', $type));
                $type = array_pop($type);
                $type = explode(';', $type);
                $type = trim(array_shift($type));
            }
            finfo_close($finfo);

        // If anyone is still using mime_content_type()
        } elseif (function_exists('mime_content_type')) {
            // mime_content_type supports streams for input, even if
            // the fact is undocumented! That's kind of crazy.
            $type = trim(mime_content_type($filestream));
        }

        if ($type !== false && strlen($type) > 0) return $type;

        // Otherwise do it the old fashioned way
        static $exts = array(
            'jpg' => 'image/jpeg', 'png' => 'image/png'
        );
        $ext = strtolower(pathInfo($filename, PATHINFO_EXTENSION));
        return isset($exts[$ext]) ? $exts[$ext] : null;
    }

}
