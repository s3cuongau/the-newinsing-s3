<?php 

namespace inSing\DataSourceBundle\Lib\Monitor;

/**
 * An observer class for monitoring SoapClient
 *
 * @author Trung Nguyen
 */
class SoapClientMonitor implements \SplObserver 
{
    const MAX_EXECUTE_TIME = 5;
    
    const SOAP_LOG_FILE = 'soap.log';
    
    const SLOWORDIE_LOG_FILE = 'slow_or_die.log';
    
    /**
     * @var Logger
     */
    protected $logger;
    
    public function __construct($logger)
    {
        $this->logger = $logger;
    }
    
    /**
     * Calculate the duration of execution and save to log file
     *
     * @param  SplSubject $subject
     * @return void
     */
    public function update(\SplSubject $subject)
    {
        $lastError = $subject->getLastError();
        
        // in case of having no error
        if (is_null($lastError)) {
            
            $context = array(
                $subject->url,
                $subject->currentMethod,
                $subject->stopTime - $subject->startTime
            );
            
            // log to file
            $this->logger->addInfoToFile($subject->pid, $context, self::SOAP_LOG_FILE);
            
            // log to slow_or_die.log in case the duration of execution reach to limit
            if ($subject->stopTime - $subject->startTime >= self::MAX_EXECUTE_TIME) {
                $this->logger->addInfoToFile($subject->pid, $context, self::SLOWORDIE_LOG_FILE);
            }
            
        } else {
            $context = array(
                $subject->url,
                $subject->currentMethod,
                $lastError['error_code'],
                $lastError['error_str']
            );
            
            // log to file
            $this->logger->addInfoToFile($subject->pid, $context, self::SLOWORDIE_LOG_FILE);
        }
    }
}