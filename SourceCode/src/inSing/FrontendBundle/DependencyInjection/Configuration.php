<?php

namespace inSing\FrontendBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('in_sing_frontend');

        // Here you should define the parameters that are allowed to
        // configure your bundle. See the documentation linked above for
        // more information on that topic.
        $rootNode->children()
            ->scalarNode('insing_url')
            ->isRequired()
            ->end()
            ->scalarNode('insing_login_url')
            ->isRequired()
            ->end()
            ->scalarNode('insing_register_url')
            ->isRequired()
            ->end()
            ->scalarNode('insing_search_url')
            ->isRequired()
            ->end()
            ->scalarNode('insing_profile_url')
            ->isRequired()
            ->end()
            ->scalarNode('ga_tracking_id')
            ->isRequired()
            ->end()
            ->scalarNode('facebook_app_id')
            ->isRequired()
            ->end()

            ->scalarNode('cookie_secret')
            ->isRequired()
            ->end()
            ->scalarNode('user_api_client_id')
            ->isRequired()
            ->end()
            ->scalarNode('user_api_password')
            ->isRequired()
            ->end()
            ->scalarNode('user_api_auth_url')
            ->isRequired()
            ->end()
            ->scalarNode('user_api_service_url')
            ->isRequired()
            ->end()
            ->scalarNode('gpt_ad_zone_homepage')
            ->isRequired()
            ->end()
            ->scalarNode('cache_type')
            ->isRequired()
            ->end()
            ->scalarNode('cache_timeout')
            ->isRequired()
            ->end()
                ;

        return $treeBuilder;
    }
}
