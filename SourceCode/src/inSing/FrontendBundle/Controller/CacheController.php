<?php

namespace inSing\FrontendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class CacheController extends Controller
{
    public function clearAllCacheAction()
    {
        $cache = $this->get('new.insing.cache');
        $result = $cache->clearAllCache();

        return new Response(var_dump($result));
    }

    public function clearCacheAction($key)
    {
        $cache = $this->get('new.insing.cache');
        $result = $cache->clearCache($key);

        return new Response(var_dump($result));
    }
}