<?php

namespace inSing\FrontendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class NewInSingController extends Controller
{
    public function kenttestAction()
    {
        $new_insing_api = $this->get('new_insing_api');
        $result = $new_insing_api->kenttest();
        return new Response(var_dump($result));
    }
}
