/**
 * Created by root on 8/21/15.
 */
$().ready(function() {
    var filesize = $('.upload-preview').data('filesize');//bytes

    // Detecting IE
    var oldIE;
    if ($('html').is('.ie8')) {
        oldIE = true;
    }

    if(oldIE){
        $('#submit_event_images').removeAttr('accept');
    }
    if($.browser.msie)
    {
        $('#submit_event_images').css('visibility', 'visible').css('position', 'static');
        $('.file-button').hide();
        //$('#select-photos-btn').next('br').hide();
        $('.ie-no-support').show();
        $('.ie-no-support-multiple').show();
        //$('.to-hide').hide();
    }
    else
    {
        $('#submit_event_images').attr('accept', 'image/*');
        document.getElementById("submit_event_images").addEventListener("change", handleFileSelect, false);
    }



    //trigger file upload
    $('.file-button').click(function(){
        $('.file-input').trigger('click');
    });
    $('input:radio[name="pricingType"]').change(function(event) {

        var value = $('input:radio[name="pricingType"]:checked').val();
        //reset
        $('.singlePrice-input,.multiplePriceMin-input,.multiplePriceMax-input').prop('disabled', true);
        switch (value) {
            case '1':
                //deafult
                $('#submit_event_ticketUrl').attr('disabled','disabled');
                $('#submit_event_ticketVendorName').attr('disabled','disabled');
                break;
            case '2':
                $('.singlePrice-input').prop('disabled', false);
                $('.singlePrice-input').focus();

                $('#submit_event_ticketUrl').removeAttr('disabled');
                $('#submit_event_ticketVendorName').removeAttr('disabled');
                break;
            case '3':
                $('.multiplePriceMin-input,.multiplePriceMax-input').prop('disabled', false);
                $('.multiplePriceMin-input').focus();

                $('#submit_event_ticketUrl').removeAttr('disabled');
                $('#submit_event_ticketVendorName').removeAttr('disabled');
                break;
        }
        $('#errorPricing').html('').hide();
    });


    $.validator.addMethod("cusTermOfUseRequired", function(value) {
        if($('#submit_event_termOfUse').attr('checked')) {
            return true;
        }
        return false;
    }, 'Please enter "myTermOfUse"!');


    // validate signup form on keyup and submit
    $("#eventSubmitForm").validate({
        //debug: true,
        errorElement: "label",
        errorContainer: $(".error"),
        //errorPlacement: function(error, element) {
            //error.appendTo(element.parent("td").next("td"));
        //},
        rules: {
            'submit_event[fullName]': "required",
            'submit_event[emailAddress]': {
                required: true,
                email: true
            },
            'submit_event[primaryContactNo]': {
                required: true,
                number: true

            },
            'submit_event[title]': "required",
            'submit_event[startDate]': {
                required: true,
                date: true
            },
            'submit_event[endDate]': {
                required: true,
                date: true
            },
            'submit_event[venueName]': "required",
            'submit_event[unitNo]': {
                required: true,
                number: true
            },
            'submit_event[buildingName]': "required",
            'submit_event[blockNo]': {
                required: true,
                number: true
            },
            'submit_event[streetName]': "required",
            'submit_event[postalCode]': "required",
            'submit_event[eventSummary]': "required",
            'submit_event[subcategories]': "required",
            'submit_event[termOfUse]': "required",
            //topic: {
            //    required: "#newsletter:checked",
            //    minlength: 2
            //},
            pricingType: "required",
            singlePrice: {
                number: true,
                required: {
                    depends: function(element) {
                        return $('#pricingTypeSingle').is(':checked');
                    }
                }
            },
            multiplePriceMin: {
                number: true,
                required: {
                    depends: function(element) {
                        return $('#pricingTypeMulti').is(':checked');
                    }
                },
                max: function() {
                    var smax = $('#multiplePriceMax').val();
                    var reg = /^\d+$/;
                    if ( reg.test(smax) ) {
                        return parseFloat(smax);
                    } else {
                        //default pass
                        return 999999999;
                    }

                }
            },
            multiplePriceMax: {
                number: true,
                required: {
                    depends: function(element) {
                        return $('#pricingTypeMulti').is(':checked');
                    }
                }
            },
            "submit_event[ticketUrl]": {
                url: true,
                required: {
                    depends: function() {
                        return $('#pricingTypeSingle').is(':checked') || $('#pricingTypeMulti').is(':checked');
                    }
                }
            },
            "submit_event[ticketVendorName]": {
                required: {
                    depends: function() {
                        return $('#pricingTypeSingle').is(':checked') || $('#pricingTypeMulti').is(':checked');
                    }
                }
            },
            "submit_event[images][]": "required"

        },
        messages: {
            'submit_event[fullName]': "Full name is required.",
            'submit_event[emailAddress]': {
                required: "Email address is required.",
                email: "Email address is invalid."
            },
            'submit_event[primaryContactNo]': {
                required: "ContactNo is required.",
                number: "ContactNo is invalid."

            },
            'submit_event[title]': "Title is required.",
            'submit_event[startDate]': {
                required: "Start date is required.",
                date: "Start date is invalid."
            },
            'submit_event[endDate]': {
                required: "End date is required.",
                date: "Start date is invalid."
            },
            'submit_event[venueName]': "Venue name is required.",
            'submit_event[unitNo]': {
                required: "UnitNo is required.",
                number: "UnitNo is invalid."
            },
            'submit_event[buildingName]': "Building name is required.",
            'submit_event[blockNo]': {
                required: "BlockNo is required.",
                number: "BlockNo is invalid."
            },
            'submit_event[streetName]': "Street name is required.",
            'submit_event[postalCode]': "Postal code is required.",
            'submit_event[eventSummary]': "Event summary is required.",
            'submit_event[subcategories]': "Category is required.",
            'submit_event[termOfUse]': "You must first read and understand the Terms of Use of insing.com.",
            'pricingType': 'Pricing is required.',
            'singlePrice': {
                required: "Single price is required.",
                number: "Single price is invalid."
            },
            'multiplePriceMin': {
                required: "Min price is required.",
                number: "Min price is invalid.",
                max: "Please enter a value less than or equal to {max}."
            },
            'multiplePriceMax': {
                required: "Max price is required.",
                number: "Max price is invalid."
            },
            'submit_event[ticketUrl]': {
                url: "Ticket url is invalid.",
                required: "Ticket url is required."
            },
            "submit_event[ticketVendorName]": "Ticket vendor name is required.",
            "submit_event[images][]": "Images are required."

        },
        submitHandler: function(form) {
            //before submit form
            var isValid = true;

            if(isValid == true) {
                $(form).submit();
            }

        }
    });

    //$('#submit_event_startDate').datepicker('option', 'onSelect', function(date) {
    //    console.log(date);
    //    var fromDate = new Date(date);
    //    //set min for endDate
    //    $('#submit_event_endDate').datepicker('option', 'minDate', fromDate);
    //});
    //
    //$('#submit_event_endDate').datepicker('option', 'onSelect', function(date) {
    //    var fromDate = new Date(date);
    //    console.log(fromDate);
    //    //set max for startDate
    //    $('#submit_event_startDate').datepicker('option', 'maxDate', fromDate);
    //});


    $("#submit_event_startDate").datepicker({
        dateFormat: "dd-M-yy",
        onSelect: function(dateText, inst) {
            var fromDate = new Date(dateText);
            $('#submit_event_endDate').datepicker('option', 'minDate', fromDate);
        }
    });

    $("#submit_event_endDate").datepicker({
        dateFormat: "dd-M-yy",
        onSelect: function(dateText, inst) {
            var fromDate = new Date(dateText);
            $('#submit_event_startDate').datepicker('option', 'maxDate', fromDate);
        }
    });





});

function handleFileSelect(evt) {
    $(".upload-preview").html(""); // clear list
    var files = evt.target.files; // FileList object
    // Loop through the FileList and render image files as thumbnails.

    var hasInvalidType = false;
    for (var i = 0, f; f = files[i]; i++) {
        // Allows only up to 5 images
        if (i >= 5) {
            alert("Allows only up to 5 photos.");
            break;
        }
        // Only process image files.
        if (!f.type.match("image.*")) {
            hasInvalidType = true;
            continue;
        }

        var reader = new FileReader();
        // Closure to capture the file information.
        reader.onload = (function(theFile) {
            return function(e) {
                // Render thumbnail.
                //filesize
                var filesize = $(".upload-preview").data("filesize");
                if( theFile.size > filesize) {
                    var s = Math.round((theFile.size / 1024) * 100) / 100;
                    var m = (filesize / 1024) * 100 / 100;
                    alert(theFile.name + ' has size ' + s + ' KB which is invalid(maxsize='+ m +' KB).');
                    //set value of input type=file
                    var $el = $('#submit_event_images');
                    $el.wrap('<form>').closest('form').get(0).reset();
                    $el.unwrap();
                    //$el[0].setCustomValidity('Images are required.');
                    return;
                }
                //var error = (theFile.size > $_max_file_size) ? " <span class='label label-error'>error</span>" : "";


                //var li = document.createElement("li");
                //li.innerHTML = ['<img class="thumb" src="', e.target.result,
                //    '" title="', escape(theFile.name), '" /><span class="filename">',
                //    theFile.name, '</span><span class="size">', Math.round((theFile.size / 1024) * 100) / 100,' KB</span>', error].join('');
                //if ($('#photos-list-1-3 li').length < 3)
                //    document.getElementById("photos-list-1-3").insertBefore(li, null);
                //else
                //    document.getElementById("photos-list-4-5").insertBefore(li, null);

                $(".upload-preview").append('<img src="'+ e.target.result +'">');
            };
        })(f);
        // Read in the image file as a data URL.
        reader.readAsDataURL(f);
    }
    if (hasInvalidType) {
        alert('Invalid types will not be uploaded.');
    }
}