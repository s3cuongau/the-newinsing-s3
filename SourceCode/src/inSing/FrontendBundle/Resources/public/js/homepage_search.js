/**
 * Created by cuong.au on 9/14/2015.
 */

$.fn.gen_url = function($activity) {
    var $activity = $activity.toLowerCase();
    $event_url = $("#event_finder_url_page").val();
    $movie_url = $("#movies_finder_url_page").val();
    $hgw_deal_url = $("#hgw_finder_url_page").val();
    $this = this;
    switch($activity){
        case 'events':
            $url ="";
            $event_key = "";
            if($this.find('.input-search-key').length){
                $event_key = $this.find('.input-search-key').val();
            }
            $event_cat = $this.find('.event_category').val();
//                        $event_date = $('#event_date').val();
            $event_date_from = $this.find('.event_date_fromdate').val();
            $event_date_to = $this.find('.event_date_todate').val();
            //http://www.insing.com/events/d/20-june-2015-25-june-2015/?category=slug1
            $event_date = "";
            if($event_date_from){
                $event_date =   $event_date_from;
                if($event_date_to){
                    $event_date =  $event_date + "-" +  $event_date_to;
                }
            }
            else{
                if($event_date_to){
                    $event_date = $event_date_to;
                }
            }

            //$url ='{{ url('event_finder') }}';
            $url =$event_url;

            if($event_key!="" ){

                $event_key = convertToSlug($event_key);
                $url = $url + 's/' + $event_key;
                if($event_date_from!="" || $event_date_to!="" ){
                    //http://www.insing.com/events/s/art-museum?start=20-june-2015&end=25-june-2015
                    if($event_date_from){
                        $url = $url + '?start='+$event_date_from;
                        if($event_date_to){
                            $url = $url + '&end='+ $event_date_to;
                        }
                    }
                    else{
                        if($event_date_to){
                            $event_date = $event_date_to;
                        }
                    }
                    if($event_cat!=""){
                        // www.insing.com/events/s/art-museum?date=20-june-2015&category=art-performing-arts,category-slug2
                        $url = $url + '&category='+$event_cat;
                    }
                }
                else
                {
                    if($event_cat!=""){
                        // www.insing.com/events/s/art-museum?date=20-june-2015&category=art-performing-arts,category-slug2
                        $url = $url + '?&category='+$event_cat;
                    }
                }
            }
            else {
                if($event_date_from!="" || $event_date_to!="" ){
                    $url = $url + 'd/'+$event_date;
                    if($event_cat!=""){
                        // www.insing.com/events/s/art-museum?date=20-june-2015&category=art-performing-arts,category-slug2
                        $url = $url + '?category='+$event_cat;
                    }
                }
                else{
                    if($event_cat!=""){
                        // www.insing.com/events/s/art-museum?date=20-june-2015&category=art-performing-arts,category-slug2
                        var $res = $event_cat.split(",");
                        if($res.length){
                            $url = $url + $res['0'];
                            if($res.length>1){
                                $url = $url + '?category='
                                $.each($res, function( index, value ) {
                                    if(index){
                                        if( 1 < index && index < $res.length ){
                                            $url = $url + ",";
                                        }
                                        $url = $url + value ;
                                    }

                                });
                            }
                        }

                    }
                }
            }
           // console.log($url);
                    window.location.href = $url;
            break;
        case 'movies':

            break;
        case 'hungrygowhere':
            $url ="";
            //http://hungrydeals.sg/hotdeals?cuisine=521ac36e67b4d89a04e3df22&neighbourhood=23
            $cuisine = $this.find('.hgw_cuisine').val();
            $neighborhood = $this.find('.hgw_neighborhood').val();
            //$url = '{{ hungry_deal_site ~ 'hotdeals'}}';
            $url = $hgw_deal_url;
            if($cuisine){
                $url = $url+'?cuisine='+$cuisine;
                if($neighborhood){
                    $url = $url +'&neighbourhood='+$neighborhood;
                }
            }
            else{
                if($neighborhood){
                    $url = $url +'?neighbourhood='+$neighborhood;
                }
            }
           // console.log($url);
                    window.location.href = $url;
            break;
    }
}

function convertToSlug(Text)
{
    return Text
        .toLowerCase()
        .replace(/ /g,'-')
        .replace(/[^\w-]+/g,'')
        ;
}



$(document).ready(function(){
    /**
     * check select activity
     */
    $('#search-header #activity-search li ').on('click', function(){
        if($(this).text())
        {
            var optionsValue =$(this).attr('data-value');

            if( optionsValue=='events' ){
                $('#search-header .input-search-key').removeAttr('disabled');
                $('#search-header .input-search-key').attr('placeholder','Type and hit enter')
            }
            else
            {
                $new_insing_hgw_input_text = $('#new_insing_hgw_input_text').val();
                $('#search-header .input-search-key').prop('disabled', true);
                $('#search-header .input-search-key').val("");
                if( optionsValue=='movies' ){
                    $('#search-header .input-search-key').attr('placeholder',"Select Movies and click Go")
                }else
                {
                    $('#search-header .input-search-key').attr('placeholder',$new_insing_hgw_input_text)
                }
            }
            $wrapper = optionsValue+"-search-wrapper";
            $('#search-header .search-wrapper').hide();
            $('#search-header #'+$wrapper).show();

        }
    });
    /**
     * genaration the url of search
     */
    $('#search-header .link-go').on('click', function(){
        $activity =  $('#activity').val() ;
        $('#search-header').gen_url($activity);
    });
    $('#search-header .input-search-key').keydown(function(e){
        //do stuff here
        if(e.keyCode == 13)
        {
            $activity =  $('#activity').val() ;
            $('#search-header').gen_url($activity);
        }
    });


    /*
    * Event find box
    * */

    /**
     * genaration the url of search
     */

    $('#event-page-input-search-key').keydown(function(e){
        //do stuff here
        if(e.keyCode == 13)
        {
            $('#events_search_box').gen_url("events");
        }
    });
    $('.link-go-event-page').on('click', function(){
        $('#events_search_box').gen_url("events");
    });

});
$(function() {
    var cache_event=[];
    var cache_movie=[];
    $( "#search-header .input-search-key" ).autocomplete({
        minLength: 3,
        deplay:500,
        open: function( event, ui ) {
            $("#search-header .autoComplete-search").show();
            return false;
        },
        select: function( event, ui ) {
            $( ".input-search-key" ).val( ui.item.label );
            $("#search-header .autoComplete-search").hide();
            return false;
        },
        close: function( event, ui ) {
            $("#search-header .autoComplete-search").hide();
        },
        appendTo: "#search-header .autoComplete-search",
        source: function( request, response ) {
            var term = request.term;
            if($('#activity').val()=='events'){
                if ( term in cache_event ) {
                    response( cache_event[term] );
                    return;
                }
                $.getJSON( $("#event_search_autocomplete").val(), request, function( data, status, xhr ) {
                    cache_event[ term ] = data;
                    response( data );
                });
            }
            if($('#activity').val()=='movies'){
                if ( term in cache_movie ) {
                    response( cache_movie[term] );
                    return;
                }
                $.getJSON( $("#movie_search_autocomplete").val(), request, function( data, status, xhr ) {
                    cache_movie[ term ] = data;
                    response( data );
                });
            }

        }
    }).data( "ui-autocomplete" )._renderItem =
        function( ul, item ) {
            ul.addClass('list');
            ul.addClass('list-unstyled');
            var label = "";
            // Replace the matched text with a custom span. This
            // span uses the class found in the "highlightClass" option.
            var re = new RegExp( "(" + this.term + ")", "gi" );
            template = "<b>$1</b>";
            if(item.label)
            {
                label = item.label.replace( re, template );
            }
            $li = $( "<li/>").appendTo(ul);

            if(""!=label)
            {
                $li.html(label);
            }
            return $li;
        };


    /*
    * Event find box autocomplete
    * */
    /**
     * autocomplete
     */
    var cache_event=[];
    if($("#events_search_box .input-search-key").length){
        $( "#events_search_box .input-search-key" ).autocomplete({
            minLength: 3,
            deplay:500,
            open: function( event, ui ) {
                $("#events_search_box .autoComplete-search").show();
                return false;
            },
            select: function( event, ui ) {
                $("#events_search_box .input-search-key" ).val( ui.item.label );
                $("#events_search_box .autoComplete-search").hide();
                return false;
            },
            close: function( event, ui ) {
                $("#events_search_box .autoComplete-search").hide();
            },
            appendTo: "#events_search_box .autoComplete-search",
            source: function( request, response ) {
                var term = request.term;
                $.getJSON( $("#event_search_autocomplete").val() , request, function( data, status, xhr ) {
                    cache_event[ term ] = data;
                    response( data );
                });

            }
        }).data( "ui-autocomplete" )._renderItem =
            function( ul, item ) {
                ul.addClass('list');
                ul.addClass('list-unstyled');
                var label = "";
                // Replace the matched text with a custom span. This
                // span uses the class found in the "highlightClass" option.
                var re = new RegExp( "(" + this.term + ")", "gi" );
                template = "<b>$1</b>";
                if(item.label)
                {
                    label = item.label.replace( re, template );
                }
                $li = $( "<li/>").appendTo(ul);

                if(""!=label)
                {
                    $li.html(label);
                }
                return $li;
            };
    }


    $('.link-go-sidebar').on('click', function(){
        var $chanel = $('#find-an-activity-channel').val();
        $('#module-find-activity').gen_url($chanel);
    });
});

