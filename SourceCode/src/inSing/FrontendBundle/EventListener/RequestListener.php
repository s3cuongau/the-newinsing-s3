<?php
namespace inSing\FrontendBundle\EventListener;

use inSing\DataSourceBundle\Lib\DataTracker;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use inSing\FrontendBundle\Security\User\inSingUser;
use inSing\DataSourceBundle\Lib\MobileDetect;
use inSing\DataSourceBundle\Lib\UtilHelper;
use inSing\DataSourceBundle\Lib\Constant;

/**
 * Implement common tasks on Request event
 *
 * @author Trung Nguyen <trung.nguyen@s3corp.com.vn>
 */
class RequestListener
{
    /**
     * @var DataTracker
     */
    protected $dataTracker;

    /**
     * @var SecurityContext
     */
    protected $securityContext;

    /**
     * @var array
     */
    protected $config;

    /**
     * Constructor
     *
     * @param DataTracker $data_tracker
     * @param DataSourceManager $ds_manager
     * @param Array $config
     * @author Trung Nguyen <trung.nguyen@s3corp.com.vn>
     */
    public function __construct(DataTracker $data_tracker, SecurityContext $security_context, array $config)
    {
        $this->securityContext	= $security_context;
        $this->dataTracker		= $data_tracker;
        $this->config			= $config;
    }

    /**
     * The following implementations:
     *
     * + Check device type
     * + Initialize DataTracker
     * + Check if request is in preview mode
     * + Check if user is logged in
     *
     * @param GetResponseEvent $event
     * @author Trung Nguyen <trung.nguyen@s3corp.com.vn>
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        // get Request
        $request = $event->getRequest();

        // detect device type
        $detect = new MobileDetect();

        $deviceType = $detect->isMobile() ? ($detect->isTablet() ? Constant::DEVICE_TYPE_TABLET : Constant::DEVICE_TYPE_MOBILE) : Constant::DEVICE_TYPE_DESKTOP;
        $request->attributes->set('device_type', $deviceType);

        // detect ios device
        $iOsDevice = $detect->isiOS() ? Constant::DEVICE_TYPE_MOBILE_IOS : '';
        $request->attributes->set('device_ios', $iOsDevice);

        // detect safari browser
		$browserSafari = $detect->version('Safari') && !$detect->version('Chrome') && !$detect->isMobile() && !$detect->isTablet() ? Constant::BROWSER_SAFARI_DESKTOP : '';
		$request->attributes->set('browser_safari', $browserSafari);

        // detect if user is logged in
        $this->authenticate($request);

        // initialize DataTracker
        $this->dataTracker->add($request->request->get('data'));

        // ititialize current time
        $currentTime = $request->request->get('time') ? $request->request->get('time') : ( $request->query->get('time') ? $request->query->get('time') : strtotime('now') );
        $request->attributes->set('current_time', $currentTime);
        /*
        // check if this request is in preview mode
        $previewMode = $request->request->get('mode') ? true : ( $request->query->get('mode') ? true : false );
        $request->attributes->set('preview_mode', $previewMode);
         */

        // get params for GPT to showing ADs with keyword
        $dcTestAdvert = $request->request->get('dcTestAdvert') ? $request->request->get('dcTestAdvert') : ( $request->query->get('dcTestAdvert') ? $request->query->get('dcTestAdvert') : '' );
        $dcTestAdvert = preg_replace('/\s/', '+', $dcTestAdvert);
        $request->attributes->set('gpt_keyword', $dcTestAdvert);

        // detect if user is logged in

        $this->authenticate($request);
    }

    /**
     *
     * @param unknown $request
     */
    protected function authenticate($request)
    {
        $secret = $this->config['cookie_secret'];

        $n_cookie = preg_replace('/"/', '', $request->cookies->get('n_cookie'));
        $i_cookie = $request->cookies->get('i_cookie');
        $l_cookie = preg_replace('/"/', '', $request->cookies->get('l_cookie'));

        $cookieSig = md5($n_cookie . $i_cookie . $l_cookie . $secret);
        $isLogged = ($request->cookies->get('s_cookie') == $cookieSig);

        if (!$request->getSession()->get('_security_secured_area') && $isLogged) {
            $user = new inSingUser($n_cookie, null, null, array('ROLE_USER'));

            $user->setId(UtilHelper::urlIdToId($i_cookie));
            $user->setDisplayName($n_cookie);

            $token = new UsernamePasswordToken($user, null, 'secured_area', array('ROLE_USER'));
            $this->securityContext->setToken($token);
            $request->getSession()->set('_security_secured_area', serialize($token));

        } elseif ($request->getSession()->get('_security_secured_area') && !$isLogged) {
            //$token = new A
            //nonymousToken(uniqid(), 'anon.');

            $this->securityContext->setToken(null);
            $request->getSession()->remove('_security_secured_area');
        }

        if (!$isLogged) {

            //$this->securityContext->setToken(null);
            //$request->getSession()->remove('_security_secured_area');
        }
    }
}