<?php
namespace inSing\FrontendBundle\Security\User;

use Symfony\Component\Security\Core\User\UserInterface;
use inSing\DataSourceBundle\Lib\UtilHelper;

/**
 * @author Dong.Truong (re-edited by Trung Nguyen)
 */
class inSingUser implements UserInterface
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $username;

    /**
     * @var string
     */
    private $password;

    /**
     * @var string
     */
    private $salt;

    /**
     * @var array
     */
    private $roles;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $display_name;

    /**
     * @var string
     */
    private $first_name;

    /**
     * @var string
     */
    private $last_name;

    /**
     * @var string
     */
    private $avatar;

    /**
     * @var string
     */
    private $mobile;

    /**
     * @var string
     */
    private $country_code;

    /**
     * @var string
     */
    private $salutation;

    /**
     * @var string
     */
    private $special_note;

    /**
     * @var bool
     */
    private $is_fetched_data;

    /**
     * Constructor
     *
     * @param string $username
     * @param string $password
     * @param string $salt
     * @param array $roles
     */
    public function __construct($username, $password, $salt, array $roles)
    {
        $this->username = $username;
        $this->password = $password;
        $this->salt = $salt;
        $this->roles = $roles;
        $this->is_fetched_data = false;
    }

    /**
     * (non-PHPdoc)
     * @see \Symfony\Component\Security\Core\User\UserInterface::getRoles()
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * (non-PHPdoc)
     * @see \Symfony\Component\Security\Core\User\UserInterface::getSalt()
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * (non-PHPdoc)
     * @see \Symfony\Component\Security\Core\User\UserInterface::getPassword()
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Getter of email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Setter of email
     *
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * Getter of ID
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Setter of ID
     *
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * (non-PHPdoc)
     * @see \Symfony\Component\Security\Core\User\UserInterface::getUsername()
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * (non-PHPdoc)
     * @see \Symfony\Component\Security\Core\User\UserInterface::eraseCredentials()
     */
    public function eraseCredentials()
    {

    }

    /**
     * Getter of display name
     *
     * @return string
     */
    public function getDisplayName()
    {
        return $this->display_name;
    }

    /**
     * Setter of display name
     *
     * @param string $display_name
     */
    public function setDisplayName($display_name)
    {
        $this->display_name = $display_name;
    }

    /**
     * Getter of last name
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->last_name;
    }

    /**
     * Setter of last name
     *
     * @param string $last_name
     */
    public function setLastName($last_name)
    {
        $this->last_name = $last_name;
    }

    /**
     * Getter of first name
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->first_name;
    }

    /**
     * Setter of first name
     *
     * @param string $first_name
     */
    public function setFirstName($first_name)
    {
        $this->first_name = $first_name;
    }

    /**
     * Getter of avatar
     *
     * @return string
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * Setter of avatar
     *
     * @param string $avatar
     */
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;
    }

    /**
     * Getter of mobile
     *
     * @return string
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * Setter of mobile
     *
     * @param string $mobile
     */
    public function setMobile($mobile)
    {
        $this->mobile = $mobile;
    }

    /**
     * Getter of Country Code
     *
     * @return string
     */
    public function getCountryCode()
    {
        return $this->country_code;
    }

    /**
     * Setter of Country Code
     *
     * @param string $country_code
     */
    public function setCountryCode($country_code)
    {
        $this->country_code = $country_code;
    }

    /**
     * Getter of Salutation
     *
     * @return string
     */
    public function getSalutation()
    {
        return $this->salutation;
    }

    /**
     * Setter of Salutation
     *
     * @param string $salutation
     */
    public function setSalutation($salutation)
    {
        $this->salutation = $salutation;
    }

    /**
     * Getter of Special Note
     *
     * @return string
     */
    public function getSpecialNote()
    {
        return $this->special_note;
    }

    /**
     * Setter of Special Note
     *
     * @param string $country_code
     */
    public function setSpecialNote($special_note)
    {
        $this->special_note = $special_note;
    }

    /**
     * Setter of property
     *
     * @param bool $status
     */
    public function setIsFetchedData($status)
    {
        $this->is_fetched_data = $status;
    }

    /**
     * Return the current status
     *
     * @return bool
     */
    public function isFetchedData()
    {
        return $this->is_fetched_data;
    }

    /**
     * Get user's profile url
     *
     * @param unknown $url
     * @return string
     */
    public function getUrlProfile($url)
    {
        if (substr($url, strlen($url) - 1) != "/") {
            $url .= "/";
        }

        $slug = UtilHelper::slug($this->display_name);
        $hash_id = UtilHelper::idToUrlId($this->id);

        if ($slug != '') {
            return $url . $slug . '/id-' . $hash_id;
        } else {
            return $url . 'id-' . $hash_id;
        }
    }
}