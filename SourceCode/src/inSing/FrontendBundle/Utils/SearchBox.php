<?php

namespace inSing\FrontendBundle\Utils;

use Symfony\Component\DependencyInjection\Container;


class SearchBox
{
    private $_logger;
    private $_cache;
    private $_container;

    /**
     * @author Cuong Au
     * @param Container $this->_container
     */
    public function __construct(Container $container)
    {
        $this->_container = $container;
        $this->_cache = $this->_container->get('new.insing.cache');
        $this->_logger = $this->_container->get('monolog.logger.homepage');
    }
    /**
     * @return data
     * @athor Au Cuong
     */
    public function renderMovie_date()
    {
        $data['0']['date'] = date('Y-m-d');
        $data['0']['date_text'] = 'Today';
        $tomorrow_timestamp = strtotime('+1 day');
        $data['1']['date'] = date('Y-m-d',$tomorrow_timestamp);
        $data['1']['date_text'] = 'Tomorrow';
        $thirdday_timestamp = strtotime('+2 day');
        $data['2']['date'] = date('Y-m-d',$thirdday_timestamp);
        $data['2']['date_text'] = date('d-M-Y',$thirdday_timestamp);
        return $data;
    }
    /**
     * @return data
     * @athor Au Cuong
     */
    public function renderMovies_showtime()
    {
        $key = 'movies_showtime_cache';

        if($this->_cache->checkCache($key)){
            return $this->_cache->getCache($key);
        }
        else{
            /**
             * get Data
             */
            $movies_api = $this->_container->get('movies_api');
            $param = array(
                'sort_fields' => 'screenings',
                'filter_showtimes'=>"now showing",
            );
            $showtmes = $movies_api->search($param);
            if(isset($showtmes['status']) && $showtmes['status'] == "200"){
                if(is_array($showtmes['data'])){
                    foreach($showtmes['data'] as $item){
                        $data[]=  $item['title'];
                    }
                }
                $this->_cache->setCache($data, $key);
                return $data;
            }
        }
        return false;
    }

    /**
     * @return data
     * @athor Au Cuong
     */
    public function renderEvent_category()
    {
        $key = 'all_event_category';
//        $this->_cache->clearCache($key);
        if($this->_cache->checkCache($key)){
            return $this->_cache->getCache($key);
        }
        else{
            /**
             * get Data
             */
            $events_api = $this->_container->get('events_api');
            $categories = $events_api->getAllCategories();
            $event_category_search = $this->_container->getParameter('new_insing_event_category_search');
            foreach($categories as $k=>$item){
                if(in_array($item['url_fragment'],$event_category_search)){
                    $data['data'][$item['id']]['name']=$item['name'];
                    $data['data'][$item['id']]['url_fragment']=$item['url_fragment'];

                }
                $data["cat-search"][$item['url_fragment']]=$item['id'];
            }
            $data['data'][0]['name']='Free Events Only';
            $data['data'][0]['url_fragment']=$event_category_search[3];
            $this->_cache->setCache($data, $key);
            return $data;
        }
        return false;
    }

    /**
     * @return data
     * @athor Au Cuong
     */
    public function renderHgw_cuisine_neighborhood()
    {
        /**
         * define cache
         */
        $key = $this->_container->getParameter('new_insing_tabledb_cache_key').'hgw_cuisine';
        $lifetime = $this->_container->getParameter('new_insing_tabledb_cache_time');
//        $this->_cache->clearCache($key);
        if($this->_cache->checkCache($key)){

            return $this->_cache->getCache($key);
        }
        else{
            /**
             * get Data
             */
            $tabledb_deals_api = $this->_container->get('tabledb_deals_api');

            $response = $tabledb_deals_api->getFilterData(array());
            $data="";
            if(isset($response['response']['data']['0']['cuisineList'])){
                $data['cuisineList'] = $response['response']['data']['0']['cuisineList'];
            }
            if(isset($response['response']['data']['0']['neighbourhoodList'])){
                $data['neighbourhoodList'] = $response['response']['data']['0']['neighbourhoodList'];
            }
            $this->_cache->setCache($data, $key, $lifetime);
            return $data;
        }
        return false;
    }


}