<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace inSing\ApiAdapterBundle\Channels;

use inSing\ApiAdapterBundle\Channels\Helper\ApiAdapter;

/**
 * Description of ThirdPartyMovies
 *
 * @author linuxmint
 */
class Movies extends ApiAdapter
{
    public function __construct($container, $cache, $channelLogger)
    {        
        parent::__construct($container, $cache, $channelLogger, "movies");
    }

    public function autoComplete($params = array())
    {
        $url = $this->basicUrl . MoviesUriMapping::AUTOCOMPLETE;
        return $this->runApiByMethod($url, "GET", $params);
    }
    
    public function search($params = array())
    {
        $url = $this->basicUrl . MoviesUriMapping::SEARCH;
        return $this->runApiByMethod($url, "GET", $params);        
    }

    /**
     * @param array $params
     * @return array|Helper\Ambigous
     * @author Cuong Au
     */
    public function  getShowtmes($params = array())
    {
        $url = $this->basicUrl . MoviesUriMapping::SHOWTIME;
        return $this->runApiByMethod($url, "GET", $params);
    }

    public function getDetails($ids)
    {
        $url = $this->basicUrl . str_replace("{id}", $ids, MoviesUriMapping::DETAILS);
        $params = array();
        return $this->runApiByMethod($url, "GET", $params);
    }

    public function getNews($params = array())
    {
        $url = $this->basicUrl . MoviesUriMapping::NEWS;
        return $this->runApiByMethod($url, "GET", $params);
    }
}
