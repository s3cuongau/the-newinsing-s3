<?php
namespace inSing\ApiAdapterBundle\Channels;

class UserApi
{
    const PREFIX_CACHE = 'user_api_';

    protected $cache;
    protected $logger;
    protected $parameters;

    /**
     * @author Cuong.Bui
     */
    public function __construct($cache, $logger, $parameters)
    {
        $this->cache        = $cache;
        $this->logger       = $logger;
        $this->parameters   = $parameters;
        
        $this->cache->setCacheLifetime($this->parameters['cache_timeout']);
    }

    /**
     * @author Cuong.Bui
     */
    public function getSessionToken()
    {
        $cache_name = self::PREFIX_CACHE . 'session_token';

        if (!$this->cache->hasCache($cache_name) || !$this->cache->getCache($cache_name)) {
            $session_token = $this->authenticate();
            $this->cache->setCache($cache_name, $session_token);

            return $session_token;
        } else {
            return $this->cache->getCache($cache_name);
        }
    }

    /**
     * @author Cuong.Bui
     */
    private function authenticate()
    {
        $url = $this->parameters['user_api_auth_url'];
        $post = array(
            'deviceIdentifier' => array(
                'type'          => '0',
                'identifier'    => '0'
            ),
            'clientId' => $this->parameters['user_api_client_id'],
            'password' => $this->parameters['user_api_password']
        );

        $soap = new \SoapClient($url, array('exceptions' => 1));
        $response = $soap->login($post);
        $ret = true;

        if (!$response) {
            $ret = false;
        } else {
            if (isset($response->resultCode)) {
                if ($response->resultCode == 200 && isset($response->sessionToken)) {
                    return $response->sessionToken;
                } else {
                    if (isset($response->resultMessage)) {
                        return false;
                        //throw new \Exception($response->resultMessage, $response->resultCode);
                    } else {
                        $ret = false;
                    }
                }
            } else {
                $ret = false;
            }
        }

        if (!$ret) {
            //throw new \Exception('Error, can not get session token');
            return false;
        }

    }

    /**
     * @author Cuong.Bui
     */
    public function getUserProfile($user_id)
    {
        $url = $this->parameters['user_api_service_url'];
        $get = array(
            'sessionToken'  => $this->getSessionToken(),
            'userId'        => array($user_id)
        );

        $soap = new \SoapClient($url, array('exceptions' => 1));
        $response = $soap->getUserDetails($get);
        $ret = true;

        if (!$response) {
            $ret = false;
        } else {
            if (isset($response->resultCode)) {
                if ($response->resultCode == 200 && isset($response->users->user)) {
                    $user = $response->users->user;

                    $data = array(
                        'insing_user_id'        => isset($user->id) ? $user->id : null,
                        'first_name'            => isset($user->firstName) ? $user->firstName : '',
                        'last_name'             => isset($user->lastName) ? $user->lastName : '',
                        'insing_user_name'      => isset($user->insingUserName) ? $user->insingUserName : '',
                        'nick_name'             => isset($user->nickName) ? $user->nickName : '',
                        'avatar'                => (isset($user->avatar) && isset($user->avatar->url)) ? $user->avatar->url : '',
                        'email'                 => isset($user->emailAddress) ? $user->emailAddress : '',
                        'mobile'                => isset($user->mobile) ? $user->mobile : '',
                        'facebook_id'           => isset($user->facebookId) ? $user->facebookId : '',
                        'number_of_followers'   => isset($user->numberOfFollowers) ? (int) $user->numberOfFollowers : 0
                    );

                    return $data;
                } else {
                    if (isset($response->resultMessage)) {
                        return false;
                        //throw new \Exception($response->resultMessage, $response->resultCode);
                    } else {
                        $ret = false;
                    }
                }
            } else {
                $ret = false;
            }
        }

        if (!$ret) {
            return false;
            //throw new \Exception('Error, can not get user details');
        }
    }

    /**
     * Get user bookmarks
     *
     * @author Cuong.Bui
     */
    public function getUserBookmarks($user_id, $book_mark_type = '', $entity_id = array())
    {
        $url = $this->parameters['user_api_service_url'];
        $get = array(
            'sessionToken'  => $this->getSessionToken(),
            'userId'        => $user_id,
            'bookmarkType'  => $book_mark_type,
            'entityId'      => $entity_id
        );

        $soap = new \SoapClient($url, array('exceptions' => 1));
        $response = $soap->getUserBookmarks($get);
        $ret = true;

        if (!$response) {
            $ret = false;
        } else {
            if (isset($response->resultCode)) {
                if ($response->resultCode == 200 && isset($response->bookmarks->bookmark)) {
                    $bookmarks = $response->bookmarks->bookmark;
                    $data = array();

                    if (count($bookmarks) > 1) {
                        foreach ($bookmarks as $bookmark) {
                            $data[] = array(
                                'bookmarkId'            => isset($bookmark->bookmarkId) ? $bookmark->bookmarkId : '',
                                'bookmarkType'          => isset($bookmark->bookmarkType) ? $bookmark->bookmarkType : '',
                                'userId'                => isset($bookmark->userId) ? $bookmark->userId : '',
                                'note'                  => isset($bookmark->note) ? $bookmark->note : '',
                                'rating'                => isset($bookmark->rating) ? $bookmark->rating : '',
                                'reviewCount'           => isset($bookmark->reviewCount) ? $bookmark->reviewCount : '',
                                'createdOn'             => isset($bookmark->createdOn) ? $bookmark->createdOn : '',
                                'updatedOn'             => isset($bookmark->updatedOn) ? $bookmark->updatedOn : '',
                                'entityType'            => isset($bookmark->entityType) ? $bookmark->entityType : '',
                                'entityId'              => isset($bookmark->entityId) ? $bookmark->entityId : '',
                                'entityDescription'     => isset($bookmark->entityDescription) ? $bookmark->entityDescription : '',
                                'entityTitle'           => isset($bookmark->entityTitle) ? $bookmark->entityTitle : '',
                                'entityUrl'             => isset($bookmark->entityUrl) ? $bookmark->entityUrl : '',
                                'entityDefaultChannel'  => isset($bookmark->entityDefaultChannel) ? $bookmark->entityDefaultChannel : '',
                                'entityDisplayImage'    => array(
                                    'resourceUrl'   => isset($bookmark->entityDisplayImage->resourceUrl) ? $bookmark->entityDisplayImage->resourceUrl : '',
                                    'mediaId'       => isset($bookmark->entityDisplayImage->mediaId) ? $bookmark->entityDisplayImage->mediaId : '',
                                    'resourceId'    => isset($bookmark->entityDisplayImage->resourceId) ? $bookmark->entityDisplayImage->resourceId : '',
                                    'url'           => isset($bookmark->entityDisplayImage->url) ? $bookmark->entityDisplayImage->url : ''
                                )
                            );
                        }
                    } elseif (count($bookmarks) == 1) {
                        $data[] = array(
                            'bookmarkId'            => isset($bookmarks->bookmarkId) ? $bookmarks->bookmarkId : '',
                            'bookmarkType'          => isset($bookmarks->bookmarkType) ? $bookmarks->bookmarkType : '',
                            'userId'                => isset($bookmarks->userId) ? $bookmarks->userId : '',
                            'note'                  => isset($bookmarks->note) ? $bookmarks->note : '',
                            'rating'                => isset($bookmarks->rating) ? $bookmarks->rating : '',
                            'reviewCount'           => isset($bookmarks->reviewCount) ? $bookmarks->reviewCount : '',
                            'createdOn'             => isset($bookmarks->createdOn) ? $bookmarks->createdOn : '',
                            'updatedOn'             => isset($bookmarks->updatedOn) ? $bookmarks->updatedOn : '',
                            'entityType'            => isset($bookmarks->entityType) ? $bookmarks->entityType : '',
                            'entityId'              => isset($bookmarks->entityId) ? $bookmarks->entityId : '',
                            'entityDescription'     => isset($bookmarks->entityDescription) ? $bookmarks->entityDescription : '',
                            'entityTitle'           => isset($bookmarks->entityTitle) ? $bookmarks->entityTitle : '',
                            'entityUrl'             => isset($bookmarks->entityUrl) ? $bookmarks->entityUrl : '',
                            'entityDefaultChannel'  => isset($bookmarks->entityDefaultChannel) ? $bookmarks->entityDefaultChannel : '',
                            'entityDisplayImage'    => array(
                                'resourceUrl'   => isset($bookmarks->entityDisplayImage->resourceUrl) ? $bookmarks->entityDisplayImage->resourceUrl : '',
                                'mediaId'       => isset($bookmarks->entityDisplayImage->mediaId) ? $bookmarks->entityDisplayImage->mediaId : '',
                                'resourceId'    => isset($bookmarks->entityDisplayImage->resourceId) ? $bookmarks->entityDisplayImage->resourceId : '',
                                'url'           => isset($bookmarks->entityDisplayImage->url) ? $bookmarks->entityDisplayImage->url : ''
                            )
                        );
                    }

                    return $data;
                } else {
                    if (isset($response->resultMessage)) {
                        throw new \Exception($response->resultMessage, $response->resultCode);
                    } else {
                        $ret = false;
                    }
                }
            } else {
                $ret = false;
            }
        }

        if (!$ret) {
            throw new \Exception('Error, can not get user bookmarks');
        }
    }

    /**
     * Create bookmarks
     *
     * @author Cuong.Bui
     */
    public function createBookmark($user_id, $entity_type, $entity_id = '', $note = '')
    {
        $url = $this->parameters['user_api_service_url'];
        $get = array(
            'sessionToken'  => $this->getSessionToken(),
            'userId'        => $user_id,
            'entityType'    => $entity_type,
            'entityId'      => $entity_id,
            'note'          => $note
        );

        $soap = new \SoapClient($url, array('exceptions' => 1));
        $response = $soap->createBookmark($get);
        $ret = true;

        if (!$response) {
            $ret = false;
        } else {
            if (isset($response->resultCode)) {
                if ($response->resultCode == 200 || $response->resultCode == 626) {
                    $data = array();

                    if (isset($response->resultCode)) {
                        $data['resultCode'] = $response->resultCode;
                    }
                    if (isset($response->resultMessage)) {
                        $data['resultMessage'] = $response->resultMessage;
                    }
                    if (isset($response->bookmarkId)) {
                        $data['bookmarkId'] = $response->bookmarkId;
                    }

                    return $data;
                } else {
                    if (isset($response->resultMessage)) {
                        throw new \Exception($response->resultMessage, $response->resultCode);
                    } else {
                        $ret = false;
                    }
                }
            } else {
                $ret = false;
            }
        }

        if (!$ret) {
            throw new \Exception('Error, can not create bookmarks');
        }
    }

    /**
     * Delete bookmarks
     *
     * @author Cuong.Bui
     */
    public function deleteBookmark($bookmark_id)
    {
        $url = $this->parameters['user_api_service_url'];
        $get = array(
            'sessionToken'  => $this->getSessionToken(),
            'bookmarkId'    => $bookmark_id
        );

        $soap = new \SoapClient($url, array('exceptions' => 1));
        $response = $soap->deleteBookmark($get);
        $ret = true;

        if (!$response) {
            $ret = false;
        } else {
            if (isset($response->resultCode)) {
                if ($response->resultCode == 200) {
                    $data = array();

                    if (isset($response->resultCode)) {
                        $data['resultCode'] = $response->resultCode;
                    }
                    if (isset($response->resultMessage)) {
                        $data['resultMessage'] = $response->resultMessage;
                    }

                    return $data;
                } else {
                    if (isset($response->resultMessage)) {
                        throw new \Exception($response->resultMessage, $response->resultCode);
                    } else {
                        $ret = false;
                    }
                }
            } else {
                $ret = false;
            }
        }

        if (!$ret) {
            throw new \Exception('Error, can not delete bookmarks');
        }
    }
}
?>