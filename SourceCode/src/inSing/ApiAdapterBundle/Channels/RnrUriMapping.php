<?php

namespace inSing\ApiAdapterBundle\Channels;

/**
 * Class UriMapping
 *
 * @package inSing\ApiBundle\Channels
 */
class RnrUriMapping
{
    const RNR_GET_ITEM_LIST    = "/items";
}
