<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace inSing\ApiAdapterBundle\Channels;

use inSing\ApiAdapterBundle\Channels\Helper\TableDBApiAdapter;

/**
 * @author Vu.Luu
 */
class TableDBDeals extends TableDBApiAdapter {

    public function __construct($container, $cache, $channelLogger) {
        parent::__construct($container, $cache, $channelLogger, "tabledb");
    }

    public function searchPromotion(array $params, $latitude = 0, $longitude = 0) {
        //http://sg.tabledb.com/tabledb-web/promotion/search/0/0?perPage=5&displayInHGW=true&partnerCode%5B%5D=hgw&countryCode%5B%5D=SG&featuredDeal=1&sortField=createdDate&order=DESC
        $temp = str_replace("{latitude}", $latitude, TableDBUriMapping::SEARCH_PROMOTION);
        $temp = str_replace("{longitude}", $longitude, $temp);
        $url = $this->basicUrl . $temp;
        $result = $this->runApiByMethod($url, "GET", $params);
        return  $result;
    }

    /**
     * @param $params
     * @return data
     * author Cuong.Au
     */
    public function getFilterData($params) {
        $url = $this->basicUrl . TableDBUriMapping::PROMOTION;
//        $url = 'http://web.qa1.tabledb.com/tabledb-web/promotion/getFilterData';
        $result = $this->runApiByMethod($url, "GET", $params);
        return  $result;
    }

    public function getDetails($ids) {
        $url = $this->basicUrl . str_replace("{id}", $ids, TableDBUriMapping::DETAILS);
        $params = array();
        $result =  $this->runApiByMethod($url, "GET", $params);

        return $result;
    }

}
