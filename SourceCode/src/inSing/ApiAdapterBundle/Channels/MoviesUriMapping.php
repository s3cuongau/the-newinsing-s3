<?php

namespace inSing\ApiAdapterBundle\Channels;

/**
 * Class UriMapping
 *
 * @package inSing\ApiBundle\Channels
 */
class MoviesUriMapping
{
    const SEARCH  = "/2.0/movies";
    const DETAILS = "/2.0/movie/{id}";
    const AUTOCOMPLETE = "/2.0/movies/autocomplete";
    const NEWS  = "/2.0/news";
    const SHOWTIME  = "/2.0/showtimes";
}