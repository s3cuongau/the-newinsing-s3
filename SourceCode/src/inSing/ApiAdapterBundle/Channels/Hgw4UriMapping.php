<?php

namespace inSing\ApiAdapterBundle\Channels;

/**
 * Class UriMapping
 *
 * @package inSing\ApiBundle\Channels
 */
class Hgw4UriMapping
{
    const SG_CONTENT_ALL  = "/sg/content/all";
    const SG_CONTENT_ACTICLE_IDS  = "/sg/content/article/{ids}";
    const SG_MODULE_IDENTIFIER  = "/sg/module/{identifier}";
}
