<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace inSing\ApiAdapterBundle\Channels;

use inSing\ApiAdapterBundle\Channels\Helper\ApiAdapter;

/**
 * Description of ThirdPartyMovies
 *
 * @author linuxmint
 */
class NewInSing extends ApiAdapter {

    public function __construct($container, $cache, $channelLogger) {
        parent::__construct($container, $cache, $channelLogger, "newInSing");
    }    

    public function kenttest($params = array()) {
        $url = $this->basicUrl . NewInSingUriMapping::KENTTEST;
        $params['content_type'] = 'json';
        return $this->runApiByMethod($url, "GET", $params);
    }

    /**
     * @author Dat.Dao
     * @param array $params
     * @return array|Helper\Ambigous
     */
    public function getHomePageCarouselModule($params = array()) {
        $url = $this->basicUrl . NewInSingUriMapping::HOMEPAGE_CAROUSEL_MODULE;
        return $this->runApiByMethod($url, "GET", $params);
    }

    /**
     * @author Dat.Dao
     * @param array $params
     * @return array|Helper\Ambigous
     */
    public function getHomePageHgwModule($params = array()) {
        $url = $this->basicUrl . NewInSingUriMapping::HOMEPAGE_HGW_MODULE;
        return $this->runApiByMethod($url, "GET", $params);
    }
}
