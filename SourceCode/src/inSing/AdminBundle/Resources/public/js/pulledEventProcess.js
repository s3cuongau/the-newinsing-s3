/**
 * Created by dat on 7/23/15.
 */

$(document).ready(function() {
    $('#btnGetData').on("click", function() {
        var $btn = $(this);
        var link = $btn.data('url');
        var $txtEventId = $('#admin_pulled_event_eventId');
        var $txtEventName = $('#admin_pulled_event_eventName');
        var $txtareaDescription = $('#admin_pulled_event_eventDescription');
        var $txtPricing = $('#admin_pulled_event_eventPricing');
        var $txtTicketUrl = $('#admin_pulled_event_eventTicketUrl');
        var $txtDefaultImageUrl = $('#admin_pulled_event_eventDefaultImageUrl');

        if( (! $txtEventId.val().match(/^\d+$/)) )
        {
            $txtEventId.val('');
            $txtEventName.val('');
            $txtareaDescription.val('');
            $txtPricing.val('');
            $txtTicketUrl.val('');
            $txtDefaultImageUrl.val('');
            return;
        }

        $btn.button('loading');

        $.ajax({
            method: "POST",
            url: link,
            data: { event_id: $txtEventId.val() },
            dataType: 'json'
        })
            .done(function( response ) {
                if(response.status == 200 && response.result.data) {
                    //update input

                    $txtEventId.val(response.result.data[0].id);
                    $txtEventName.val(response.result.data[0].name);
                    $txtareaDescription.val(response.result.data[0].description);
                    $txtPricing.val(response.result.data[0].old_pricing);
                    $txtTicketUrl.val(response.result.data[0].ticket_url);
                    $txtDefaultImageUrl.val(response.result.data[0].default_image_resource_url);
                }
                $btn.button('reset');
            });
    });
});