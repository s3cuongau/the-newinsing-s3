<?php
namespace inSing\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use inSing\DataSourceBundle\Lib\UtilHelper;

class ContentController extends Controller
{

    const NUMBER_PAGE_ON_DEAL_LIST = 5;
    const HGW_CHANNEL = 'HungryGoWhere.com';

    /*
     * @author Vu.Luu
     * Get movies detail info
     */
    public function getMovies($id)
    {
        $response = array();
        $movies_api = $this->get('movies_api');
        $result = $movies_api->getDetails($id);

        if (count($result)) {
            if ($result['status'] == Response::HTTP_OK) {

                $details = $result['data'];

                $title = isset($details['title']) ? $details['title']: '';

                $slug = !empty($details['seo_slug']) ? $details['seo_slug'] : UtilHelper::slug($details['title']);
                $hexID = UtilHelper::idToUrlId($id);
                $contentUrl = str_replace(array('{slug}','{hexID}'), array($slug, $hexID),
                    $this->container->getParameter('insing_content_carousel_movies_detail')); //need to confirm it

                $imageUrl = isset($details['poster_image']['url']) ? $details['poster_image']['url']: '';

                $movie_taglines = explode('|', trim(UtilHelper::MOVIE_TAGLINES));

                $response = array(
                    'status'  => Response::HTTP_OK,
                    'message' => 'Success.',
                    'result'  => array(
                        'title'              => $title,
                        'content_url'        => $contentUrl,
                        'image_url'          => $imageUrl,
                        'channel'            => UtilHelper::CHANNEL_MAPPING_MOVIE,
                        'slug'               => $slug
                    ),
                    'taglines' => $movie_taglines
                );
            } else {
                $response = array(
                    'status'  => Response::HTTP_BAD_REQUEST,
                    'message' => 'No data found.'
                );
            }

        } else {
            $response = array(
                'status'  => Response::HTTP_BAD_REQUEST,
                'message' => 'No result found.'
                );
        }

        return $response;
    }


    /*
     * @author Vu.Luu
     * Get events detail ino
     */
    public function getEvents($id)
    {
        $response = array();
        $events_api = $this->get('events_api');
        $result = $events_api->getDetails($id);

        if (count($result)) {
            if ($result['status'] == Response::HTTP_OK) {

                $details = $result['data'][0];

                $name = isset($details['name']) ? $details['name']: '';

                $slug = UtilHelper::slug($details['name']);
                $hexID = UtilHelper::idToUrlId($id);
                $contentUrl = str_replace(array('{slug}','{hexID}'), array($slug, $hexID),
                    $this->container->getParameter('insing_content_carousel_events_detail')); //need to confirm it

                $imageUrl = isset($details['default_image_resource_url']) ? $details['default_image_resource_url']: '';

                $event_taglines = explode('|', trim(UtilHelper::EVENT_TAGLINES));

                $response = array(
                    'status'  => Response::HTTP_OK,
                    'message' => 'Success.',
                    'result'  => array(
                        'title'              => $name,
                        'content_url'        => $contentUrl,
                        'image_url'          => $imageUrl,
                        'channel'            => UtilHelper::CHANNEL_MAPPING_EVENT,
                        'slug'               => $slug,
                        'event_data'         => json_encode($details)
                    ),
                    'taglines' => $event_taglines
                );
            } else {
                $response = array(
                    'status'  => Response::HTTP_BAD_REQUEST,
                    'message' => 'No data found.'
                );
            }

        } else {
            $response = array(
                'status'  => Response::HTTP_BAD_REQUEST,
                'message' => 'No result found.'
            );
        }


        return $response;
    }

    /*
     * @author Vu Luu
     * Get business detail info
     */
    public function getRestaurant($id)
    {
        $response = array();
        $search_api = $this->get('search_api');
        $result = $search_api->getBusinessDetails($id);

        if (count($result)) {
            if ($result['status'] == Response::HTTP_OK) {

                $details = $result['data']['business'][0];

                $name = isset($details['trading_name']) ? $details['trading_name']: '';
                $description = isset($details['description']) ? $details['description']: '';

                $slug = isset($details['hgw']['hgw_url']) ? $details['hgw']['hgw_url'] : UtilHelper::slug($details['trading_name']);
                $contentUrl = str_replace(array('{restaurant_slug}'), array($slug),
                    $this->container->getParameter('insing_content_carousel_restaurant'));

                $imageUrl = '';
                $images = $details['images'];
                if (count($images) > 0) {
                    $imageUrl = UtilHelper::generatePhotoFromPath($this->container, $images[0]['media_path']);
                }

                $tabledbId = '';
                if (!empty($details['tabledb']['restaurant_id'])){
                    $tabledbId = $details['tabledb']['restaurant_id'];
                }

                $hgw_taglines = explode('|', trim(UtilHelper::RESTAURANT_TAGLINES));

                $response = array(
                    'status'  => Response::HTTP_OK,
                    'message' => 'Success.',
                    'result'  => array(
                        'title'              => $name,
                        'description'        => $description,
                        'content_url'        => $contentUrl,
                        'image_url'          => $imageUrl,
                        'tabledb_id'         => $tabledbId,
                        'channel'            => UtilHelper::CHANNEL_MAPPING_RESTAURANT,
                        'slug'               => $slug
                    ),
                    'taglines' => $hgw_taglines
                );
            } else {
                $response = array(
                    'status'  => Response::HTTP_BAD_REQUEST,
                    'message' => 'No data found.'
                );
            }

        } else {
            $response = array(
                'status'  => Response::HTTP_BAD_REQUEST,
                'message' => 'No data found.'
            );
        }


        return $response;
    }

    /*
     * @author Vu Luu
     * Get restaurant deal info
     */
    public function getRestaurantDeal($id, $moduleType)
    {
        $response = array();
        $search_api = $this->get('search_api');
        $result = $search_api->getBusinessDetails($id);

        if (count($result)) {
            if ($result['status'] == Response::HTTP_OK) {

                $details = $result['data']['business'][0];

                $tabledbId = '';
                if (!empty($details['tabledb']['restaurant_id'])){
                    $tabledbId = $details['tabledb']['restaurant_id'];
                }

                $imageUrl = '';
                $images = $details['images'];
                if (count($images) > 0) {
                    $imageUrl = UtilHelper::generatePhotoFromPath($this->container, $images[0]['media_path']);
                }

                $hotDeals = $details['hot_deals'];

                if (count($hotDeals) > 0) {
                    $results = array();
                    $slug = isset($details['hgw']['hgw_url']) ? $details['hgw']['hgw_url'] : UtilHelper::slug($details['business_name']);

                    foreach ($hotDeals as $deal) {

                        $results[] = array(
                            'id'            => $deal['id'],
                            'title'         => $deal['title'],
                            'description'   => $deal['description'],
                            'start_date'    => date_format(date_create($deal['start_date']), 'd M Y'),
                            'end_date'      => date_format(date_create($deal['end_date']), 'd M Y'),
                            'image_url'     => $deal['image_url'],
                            'channel'       => UtilHelper::CHANNEL_MAPPING_RESTAURANT_DEAL,
                            'content_url'   => str_replace(array('{restaurant_slug}'), array($slug),
                                $this->container->getParameter('insing_content_carousel_restaurant_deal'))
                        );

                    }

                    $paging = $this->dealsPaging($results, self::NUMBER_PAGE_ON_DEAL_LIST);
                    $result_html = $this->renderView('inSingAdminBundle:Paging:_deal.html.twig', array(
                        'restaurant_name' => $details['business_name'],
                        'deal_type' => 'Restaurant Deals',
                        'restaurant_image' => $imageUrl,
                        'data' => $results,
                        'paging' =>$paging,
                        'module' => $moduleType,
                        'tabledb_id' => $tabledbId,
                        'slug' => $slug
                    ));

                    $hgw_deals_taglines = explode('|', trim(UtilHelper::RESTAURANT_DEAL_TAGLINES));

                    $response = array(
                        'status'  => Response::HTTP_OK,
                        'message' => 'Success.',
                        'result_html'  => $result_html,
                        'result'  => $results,
                        'taglines' => $hgw_deals_taglines
                    );

                } else {
                    $response = array(
                        'status'  => Response::HTTP_BAD_REQUEST,
                        'message' => 'There is no deal in this restaurant.'
                    );
                }

            } else {
                $response = array(
                    'status'  => Response::HTTP_BAD_REQUEST,
                    'message' => 'No data found.'
                );
            }

        } else {
            $response = array(
                'status'  => Response::HTTP_BAD_REQUEST,
                'message' => 'No data found.'
            );
        }


        return $response;
    }

    /*
     * @author Vu Luu
     * Get restaurant hungry deal info
     */
    public function getRestaurantHungryDeal($id, $moduleType)
    {
        $response = array();
        $search_api = $this->get('search_api');
        $result = $search_api->getBusinessDetails($id);

        if (count($result)) {

            if ($result['status'] == Response::HTTP_OK) {

                $details = $result['data']['business'][0];

                $tabledbId = '';
                if (!empty($details['tabledb']['restaurant_id'])){
                    $tabledbId = $details['tabledb']['restaurant_id'];
                }

                $imageUrl = '';
                $images = $details['images'];
                if (count($images) > 0) {
                    $imageUrl = UtilHelper::generatePhotoFromPath($this->container, $images[0]['media_path']);
                }

                $restaurantId = $details['tabledb']['restaurant_id'];
                $slug = isset($details['hgw']['hgw_url']) ? $details['hgw']['hgw_url'] : UtilHelper::slug($details['business_name']);

                //call tabledb api to get all deals of restaurant
                $deals_api = $this->get('tabledb_deals_api');
                $dealResult = $deals_api->getDetails($restaurantId);

                $results = array();
                $totalPages = 0;

                if ( !empty($dealResult['response']['data'])) {
                    $dealResults = $dealResult['response']['data'];

                    foreach ($dealResults as $deal) {

                        $results[] = array(
                            'id' => $deal['id'],
                            'title' => $deal['title'],
                            'description' => $deal['description'],
                            'start_date' => date('d M Y', substr($deal['startTimestamp'], 0, -3)),
                            'end_date' => date('d M Y', substr($deal['endTimestamp'], 0, -3)),
                            'image_url' => $deal['promotionImage'],
                            'channel' => UtilHelper::CHANNEL_MAPPING_RESTAURANT_HUNGRYDEAL,
                            'content_url' => str_replace(array('{restaurant_slug}', '{deal_id}'), array($slug, $deal['id']),
                                $this->container->getParameter('insing_content_carousel_restaurant_hungrydeal'))
                        );
                    }

                    $totalPages = $dealResult['response']['totalPages'];
                }

                //get all rest datas of tabledb
                if ($totalPages > 1) {
                    for ( $i = 1; $i <= $totalPages; $i++) {
                        //pass page=? to call again tabledb api
                        $dealResult = $deals_api->getDetails($restaurantId."?page=$i");

                        if ( !empty($dealResult['response']['data'])) {
                            $dealResults = $dealResult['response']['data'];

                            foreach ($dealResults as $deal) {

                                $results[] = array(
                                    'id' => $deal['id'],
                                    'title' => $deal['title'],
                                    'description' => $deal['description'],
                                    'start_date' => date('d M Y', substr($deal['startTimestamp'], 0, -3)),
                                    'end_date' => date('d M Y', substr($deal['endTimestamp'], 0, -3)),
                                    'image_url' => $deal['promotionImage'],
                                    'channel' => UtilHelper::CHANNEL_MAPPING_RESTAURANT_HUNGRYDEAL,
                                    'content_url' => str_replace(array('{restaurant_slug}', '{deal_id}'), array($slug, $deal['id']),
                                        $this->container->getParameter('insing_content_carousel_restaurant_hungrydeal'))
                                );
                            }
                        }
                    }
                }

                if (count($results) > 0) {
                    $paging = $this->dealsPaging($results, self::NUMBER_PAGE_ON_DEAL_LIST);
                    $result_html = $this->renderView('inSingAdminBundle:Paging:_deal.html.twig', array(
                        'restaurant_name' => $details['business_name'],
                        'restaurant_image' => $imageUrl,
                        'deal_type' => 'Restaurant HungryDeals',
                        'data' => $results,
                        'paging' =>$paging,
                        'module' => $moduleType,
                        'tabledb_id' => $tabledbId,
                        'slug' => $slug
                    ));

                    $hgw_hungrydeals_taglines = explode('|', trim(UtilHelper::RESTAURANT_HUNGRYDEAL_TAGLINES));

                    $response = array(
                        'status'  => Response::HTTP_OK,
                        'message' => 'Success.',
                        'result_html'  => $result_html,
                        'result'  => $results,
                        'taglines' => $hgw_hungrydeals_taglines
                    );
                } else {
                    $response = array(
                        'status'  => Response::HTTP_BAD_REQUEST,
                        'message' => 'There is no deal in this restaurant.'
                    );
                }
            } else {
                $response = array(
                    'status'  => Response::HTTP_BAD_REQUEST,
                    'message' => 'No data found.'
                );
            }

        } else {
            $response = array(
                'status'  => Response::HTTP_BAD_REQUEST,
                'message' => 'No data found.'
            );
        }


        return $response;
    }

    /**
     * @author Vu.Luu
     * get details info
     * including movies, events, restaurant, restaurant deals, restaurant hungrydeal, article for carousel/hgw, gallery for carousel/hgw
     */
    public function getAction()
    {
        $request = $this->get('request');
        $response = array();
        if ($request->isXmlHttpRequest()) {
            $query = $request->query->all();

            $type = isset($query['type']) ? trim($query['type']) : ''; //article or gallery
            $id = isset($query['id']) ? trim($query['id']) : '';
            $module_type = isset($query['module']) ? trim($query['module']) : ''; //carousel or hgw

            if ($type != '' && $id != '') {
                switch ($type) {
                    default:
                    case 1:
                        $response = $this->getMovies($id);
                        break;

                    case 2:
                        $response = $this->getEvents($id);
                        break;

                    case 3:
                        $response = $this->getRestaurant($id);
                        break;

                    case 4:
                        $response = $this->getRestaurantDeal($id, $module_type);
                        break;

                    case 5:
                        $response = $this->getRestaurantHungryDeal($id, $module_type);
                        break;

                    case 6:
                        $response = $this->getArticleDetail($id, $module_type);
                        break;

                    case 7:
                        $response = $this->getGalleryDetail($id, $module_type);
                        break;
                }
            } else {
                $response = array(
                    'status' => Response::HTTP_BAD_REQUEST,
                    'message' => 'Invalid parameters.'
                );
            }
        } else {
            $response = array(
                'status' => Response::HTTP_BAD_REQUEST,
                'message' => 'Invalid AJAX request.'
            );
        }

        return self::outputJSON($response);
    }

    /**
     * @author Vu.Luu
     */
    private function getArticleDetail($id, $moduleType)
    {
        $response = array();
        $data_source = $this->container->get('insing.datasource.manager');
        $cms = $data_source->get('cms');

        $result = $cms->getCmsDetailsByArticleId($id);


        if ($result) {
            $details = $result;

            if ($details['status'] == UtilHelper::PUBLISH_STATUS) {

                if ($moduleType == 'hgw') {
                    if (isset($details['channel']) && self::HGW_CHANNEL != $details['channel']){
                        return  array(
                            'status'  => Response::HTTP_BAD_REQUEST,
                            'message' => 'Cannot find article from HGW.'
                        );
                    }
                }

	            $title = isset($details['title']) ? $details['title'] : '';

                $description = isset($details['summary']) ? $details['summary'] : '';
                

	            $slug = isset($details['seo_url_slug']) ? UtilHelper::slug(UtilHelper::getSlug($details['seo_url_slug'])) : '';
	            $author = isset($details['author']) ? $details['author'] : '';

	            if ($slug == '') {
	                $slug = UtilHelper::slug($title);
	            }

                $series_name = '';
                if (isset($details['series'][0]['seo_url'])) {
                    $series_name = $details['series'][0]['seo_url'];
                }

	            $image_url = '';
	            if (isset($details['body']) && !empty($details['body'])) {
	                foreach ($details['body'] as $item) {
	                    if ($item['module'] == 'default_image') {
	                        $image_url = $item['default_image'];
	                    }
	                }
	            }

                if (isset($details['channel']) && self::HGW_CHANNEL == $details['channel']){
                    $channel = UtilHelper::CHANNEL_MAPPING_RESTAURANT;
                } else {
                    $channel = isset($details['channel']) ? strtolower($details['channel']) : '';
                }

                if ($moduleType == 'hgw') {
                    $contentUrl = str_replace(array('{series_name}','{slug}','{hexID}'),
                        array($series_name, $slug, UtilHelper::idToUrlId($id)),
                        $this->container->getParameter('insing_content_hgw_article_detail') );
                } else {
                    $contentUrl = str_replace(array('{slug}','{hexID}'),
                        array( $slug, UtilHelper::idToUrlId($id)),
                        $this->container->getParameter('insing_content_carousel_article_detail') );
                }

                $article_taglines = explode('|', trim(UtilHelper::ARTICLE_TAGLINES));

	            $response = array(
                    'status'  => Response::HTTP_OK,
                    'message' => 'Success.',
                    'result'  => array(
                        'title'              => $title,
                        'description'        => $description,
                        'channel'            => $channel,
                        'content_url'        => $contentUrl,
                        'image_url'          => $image_url,
                        'editor'             => UtilHelper::generateArticleEditor($author),
                        'slug'               => $slug,
                        'series_seo_url'     => $series_name
                    ),
                    'taglines' => $article_taglines
	            );
            } else {
                $response = array(
                    'status'  => Response::HTTP_BAD_REQUEST,
                    'message' => 'No data found.'
                );
            }
        } else {
            $response = array(
                'status'  => Response::HTTP_BAD_REQUEST,
                'message' => 'No data found.'
            );
        }

        return $response;
    }

    /**
     * @author Vu.Luu
     */
    private function getGalleryDetail($id, $moduleType)
    {
        $response = array();
        $data_source = $this->container->get('insing.datasource.manager');
        $cms = $data_source->get('cms');

        $result = $cms->getCmsGalleryDetailsByArticleId($id);

        if ($result) {
            $details = $result;

            if ($details['status'] == UtilHelper::PUBLISH_STATUS) {

                if ($moduleType == 'hgw') {
                    if (isset($details['channel']) && 'HungryGoWhere.com' != $details['channel']){
                        return  array(
                            'status'  => Response::HTTP_BAD_REQUEST,
                            'message' => 'Cannot find gallery from HGW.'
                        );
                    }
                }

	            $title = isset($details['title']) ? $details['title'] : '';

                $description = isset($details['summary']) ? $details['summary'] : '';

	            $slug = isset($details['seo_url_slug']) ? UtilHelper::slug(UtilHelper::getSlug($details['seo_url_slug'])) : '';

	            $author = isset($details['author']) ? $details['author'] : '';

	            if ($slug == '') {
	                $slug = UtilHelper::slug($title);
	            }

	            $image_url   = '';

	            if (isset($details['images']) && !empty($details['images'])) {
                    $photo_count = count($details['images']);

	                if (isset($details['images'][0]['image_url'])) {
	                    $image_url = $details['images'][0]['image_url'];
	                }

	                foreach ($details['images'] as $item) {
	                    if ($item['is_main'] == 1 && $item['image_url'] != '') {
	                        $image_url = $item['image_url'];
	                        break;
	                    }
	                }
	            }

                if (isset($details['channel']) && self::HGW_CHANNEL == $details['channel']){
                    $channel = UtilHelper::CHANNEL_MAPPING_RESTAURANT;
                } else {
                    $channel = isset($details['channel']) ? strtolower($details['channel']) : '';
                }

                if ($moduleType == 'hgw') {
                    $contentUrl = str_replace(array('{gallery_slug}','{hexID}'),
                        array( $slug, UtilHelper::idToUrlId($id)),
                        $this->container->getParameter('insing_content_hgw_gallery_detail') );
                } else {
                    $contentUrl = str_replace(array('{slug}','{hexID}'),
                        array( $slug, UtilHelper::idToUrlId($id)),
                        $this->container->getParameter('insing_content_carousel_gallery_detail') );
                }

                $gallery_taglines = explode('|', trim(UtilHelper::GALLERY_TAGLINES));

                $response = array(
                    'status' => Response::HTTP_OK,
                    'message' => 'Success.',
                    'result' => array(
                        'title'              => $title,
                        'description'        => $description,
                        'channel'            => $channel,
                        'content_url'        => $contentUrl,
                        'image_url'          => $image_url,
                        'editor'             => UtilHelper::generateArticleEditor($author),
                        'slug'               => $slug
                    ),
                    'taglines' => $gallery_taglines
	            );
            } else {
                $response = array(
                    'status'  => Response::HTTP_BAD_REQUEST,
                    'message' => 'No data found.'
                );
            }
        } else {
            $response = array(
                'status'  => Response::HTTP_BAD_REQUEST,
                'message' => 'No data found.'
            );
        }

        return $response;
    }

    /*
     * @author Vu Luu
     * Get numper page on deal list
     */
    public function dealsPaging($results, $per_page)
    {
        $total = count($results);
        $paging = $total / $per_page;
        return ceil($paging);
    }

    /**
     * @editor Vu Luu
     */
    private function outputJSON($result)
    {
        $response = new Response();

        $response->headers->set('Content-Type', 'application/json');
        $response->setStatusCode(Response::HTTP_OK);
        $response->setContent(json_encode($result));

        return $response;
    }
}