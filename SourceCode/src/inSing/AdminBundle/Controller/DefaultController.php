<?php
namespace inSing\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $user_token = $this->container->get('security.context')->getToken();
        if (!$user_token->isAuthenticated()) {
            throw new \Exception('No permission.', 403);
        }

        return $this->render('inSingAdminBundle:Default:index.html.twig');
    }

    public function resetCachesPageAction()
    {
        $cache = $this->get('new.insing.cache');
        $key = 'STATUS_CMD_RESET_CACHE_HP';
        if ('running' == $cache->getCache($key)) {
            $commandRunningFlagged = true;
        } else {
            $commandRunningFlagged = false;
        }
        return $this->render('inSingAdminBundle:Default:reset-caches-page.html.twig', array(
            'command_running_flagged' => $commandRunningFlagged
        ));
    }

    public function resetHomepageCachesAction()
    {
        $server_project_root_path = $this->container->getParameter('server_project_root_path');
        $cmd = "cd $server_project_root_path && php app/console homepage:resetCache --env=frontend_prod 2> app/logs/hp_reset_cache_error.log >/dev/null &";
        $output = shell_exec($cmd);
        return new Response($cmd);
    }
}