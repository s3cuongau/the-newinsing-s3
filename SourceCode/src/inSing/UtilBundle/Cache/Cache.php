<?php

namespace inSing\UtilBundle\Cache;

/**
 * Cache wrapper class
 *
 * @author Vu Tran (re-edited by Trung Nguyen)
 */
class Cache
{
    // cache types
    const CACHE_TYPE_APC       = 'apc';
    const CACHE_TYPE_MEMCACHE  = 'memcache';
    const CACHE_TYPE_FILE      = 'filecache';

    /**
     * @var array
     */
    protected $config;
    
    /**
     * @var array
     */
    protected $cache;

    /**
     * @var string
     */
    protected $currentType;
    
    /**
     * @var string
     */
    protected $nameSpace = '';
    
    /**
     * Constructor
     *
     * @author Vu Tran
     * @param  array $config
     */
    public function __construct(array $config)
    {           
        if (!in_array(self::CACHE_TYPE_APC, $config['type']) && 
            !in_array(self::CACHE_TYPE_MEMCACHE, $config['type']) && 
            !in_array(self::CACHE_TYPE_FILE, $config['type']) ) {
            
            throw new \Exception('Invalid cache type');
        }        
        
        // init.
        if (isset($config['namespace']) && $config['namespace'] != '') {
            $this->nameSpace = $config['namespace'];
        }
        
        $this->currentType = null;
        $this->config      = $config;
    }

    /**
     * Magic call
     *
     * @param string $method
     * @param array $args
     * @throws \Exception
     * @return mixed
     */
    public function __call($method, $args)
    {
        try {
            // initialize Cache instance if needed
            if (!$this->init($this->currentType)) {
                throw new \Exception('Cant initialize cache instance');
            }
    
            if (is_callable(array($this->cache[$this->currentType], $method))) {
                $this->cache[$this->currentType]->setNamespace = $this->nameSpace;
                return call_user_func_array(array($this->cache[$this->currentType], $method), $args);
            } else {
                throw new \Exception('Call to undefined method');
            }
        } catch (\Exception $e) {
            throw $e;
        }
    }
    
    /**
     * Magic getter
     * 
     * @param string $name
     */
    public function __get($name) 
    {
        try {
            if ($name != self::CACHE_TYPE_APC && $name != self::CACHE_TYPE_MEMCACHE && $name != self::CACHE_TYPE_FILE) {
                throw new \Exception('Call to undefined property');
            }
            
            if (!isset($this->cache[$name])) {
                $this->init($name);
            }
            
            return $this->cache[$name];
            
        } catch (\Exception $e) {
            throw $e;
        }
    }
    
    /**
     * Initialize cache instance
     *
     * @author Trung Nguyen
     * @param  string $cache_type
     * @return bool
     */
    protected function init($cache_type)
    {        
        $ret = true;
        
        switch ($cache_type) {
            case self::CACHE_TYPE_APC:
                if (!isset($this->cache[self::CACHE_TYPE_APC])) {
                    $this->cache[self::CACHE_TYPE_APC] = new \inSing\UtilBundle\Cache\ApcCache();
                    $this->cache[self::CACHE_TYPE_APC]->setCacheLifetime($this->config['lifetime']);
                    $this->cache[self::CACHE_TYPE_APC]->setNamespace($this->nameSpace);
                    $this->cache[self::CACHE_TYPE_APC]->setPrefix($this->prefix);    // backward compatible
                }
                break;
    
            case self::CACHE_TYPE_MEMCACHE:
                if (!isset($this->cache[self::CACHE_TYPE_MEMCACHE])) {
                    // get memcache server info.
                    $memcacheList = $this->config['memcache'];
        
                    if (count($memcacheList) <= 0) {
                        throw new \Exception('No memcache server is configured');
                    } 
                    
                    $memcacheClass = class_exists('Memcached') ? '\Memcached' : '\Memcache';            
                    $memcache = new $memcacheClass();
                    
                    foreach ($memcacheList as $mc) {                
                        if (preg_match('/(?<ip>.+):(?<port>.+)/', $mc, $matches)) {
                            $memcache->addserver($matches['ip'], $matches['port']);
                        } else {
                            throw new \Exception('The info. of memcache server is invalid');
                        }
                    }
                    
                    $memcacheWrapperClass = class_exists('Memcached') ? '\inSing\UtilBundle\Cache\MemcachedCache' : '\inSing\UtilBundle\Cache\MemcacheCache';
                    
                    $this->cache[self::CACHE_TYPE_MEMCACHE] = new $memcacheWrapperClass($memcache);
                    $this->cache[self::CACHE_TYPE_MEMCACHE]->setCacheLifetime($this->config['lifetime']);
                    $this->cache[self::CACHE_TYPE_MEMCACHE]->setNamespace($this->nameSpace);
                    $this->cache[self::CACHE_TYPE_MEMCACHE]->setPrefix($this->prefix);    // backward compatible
                }
                break;
                
            case self::CACHE_TYPE_FILE:
                if (!isset($this->cache[self::CACHE_TYPE_FILE])) {
                    $this->cache[self::CACHE_TYPE_FILE] = new \inSing\UtilBundle\Cache\FileCache();
                    $this->cache[self::CACHE_TYPE_FILE]->setNamespace($this->nameSpace);
                    $this->cache[self::CACHE_TYPE_FILE]->setPath($this->config['path']);
                }
                break;
                
            default:
                $ret = false;
        }
        
        return $ret;
    }
    
    /**
     * Set the namespace to prefix all cache ids with
     *
     * @author Trung Nguyen
     * @param  string $namespace
     */
    public function setNamespace($namespace)
    {
        $this->nameSpace = $namespace;
    }
    
    /**
     * Register the cache type (e.g. apc, memcache, filecache) for use
     *
     * @param string $cache_type
     * @throws \Exception
     */
    public function setDefaultType($cache_type)
    {
        if (!in_array($cache_type, $this->config['type'])) {
            throw new \Exception("This '{$type}' has not supported yet");
        }
        
        $this->currentType = $cache_type;
    }
    
    /**
     * Create Name for Cache based on input params array
     *
     * @author Vu Tran (re-edited by Trung Nguyen)
     * @param  array/string $param
     * @return string
     */
    public function createCacheName($param)
    {
        $tmp = array();
    
        // slutify strings closure
        $slugify = function($text) {
            // replace non letter or digits by -
            $text = preg_replace('~[^\\pL\d]+~u', '-', $text);
    
            // trim
            $text = trim($text, '-');
    
            // transliterate
            if (function_exists('iconv')) {
                $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
            }
    
            // lowercase
            $text = strtolower($text);
    
            // remove unwanted characters
            $text = preg_replace('~[^-\w]+~', '', $text);
    
            // default output
            if (empty($text)) {
                return 'n-a';
            }
    
            return $text;
        };
    
        // array filter closure
        $callback = function($value, $key) use(&$tmp, $slugify) {
            if (trim($value) != '') {
                $tmp[] = $slugify($key).'_'.$slugify($value);
            }
        };
    
        if (is_array($param)) {
            array_walk_recursive($param, $callback);
            return implode('_', $tmp);
        } else {
            return $slugify($param);
        }
    }
    
    
    //=====================================================================================================
    // These are old methods will be not used in future. However, we still keep them here for 
    // backward compatible.    
    //=====================================================================================================
    /**
     * @var string
     */
    protected $prefix = null;
    
    /**
     * @var string
     */
    protected $defaultCacheType = null;
    
    /**
     * Get APC instance
     *
     * @author Trung Nguyen
     * @return inSing\UtilBundle\Cache\ApcCache
     * @throws Exception
     */
    public function getAPC() 
    {
        if (!in_array(self::CACHE_TYPE_APC, $this->config['type'])) {
            throw new \Exception(self::CACHE_TYPE_APC.' is not supported');
        }
        
        // initialize APC object if not available
        $this->init(self::CACHE_TYPE_APC);
        
        if (isset($this->cache[self::CACHE_TYPE_APC])) {            
            return $this->cache[self::CACHE_TYPE_APC];
        } else {
            throw new \Exception('Call undefined object');
        }
    }
    
    /**
     * Get Memcache instance
     *
     * @author Trung Nguyen
     * @return inSing\UtilBundle\Cache\MemcacheCache
     * @throws Exception
     */
    public function getMemcache() 
    {
        if (!in_array(self::CACHE_TYPE_MEMCACHE, $this->config['type'])) {
            throw new \Exception(self::CACHE_TYPE_MEMCACHE.' is not supported');
        }
        
        // initialize Memcache object if not available
        $this->init(self::CACHE_TYPE_MEMCACHE);
        
        if (isset($this->cache[self::CACHE_TYPE_MEMCACHE])) {
            return $this->cache[self::CACHE_TYPE_MEMCACHE];
        } else {
            throw new \Exception('Call undefined object');
        }
    }
    
    /**
     * Get FileCache instance
     *
     * @author Trung Nguyen
     * @return inSing\UtilBundle\Cache\FileCache
     * @throws Exception
     */
    public function getFilecache()
    {
        if (!in_array(self::CACHE_TYPE_FILE, $this->config['type'])) {
            throw new \Exception(self::CACHE_TYPE_FILE.' is not supported');
        }
        
        // initialize CacheFile object if not available
        $this->init(self::CACHE_TYPE_FILE);
    
        if (isset($this->cache[self::CACHE_TYPE_FILE])) {
            return $this->cache[self::CACHE_TYPE_FILE];
        } else {
            throw new \Exception('Call undefined object');
        }
    }
    
    /**
     * Get default cache object (priority is given to APC)
     * 
     * @author Trung Nguyen
     * @return object 
     */
    public function getDefault()
    {
        if (!is_null($this->defaultCacheType)) {
            
            switch (strtolower($this->defaultCacheType)) {
                case self::CACHE_TYPE_APC:
                    return $this->getAPC();
                    break;
                    
                case self::CACHE_TYPE_MEMCACHE:
                    return $this->getMemcache();
                    break;
                    
                case self::CACHE_TYPE_FILE:
                    return $this->getFilecache();
                    break;
                    
                default: 
                    return false;
            }
        } else {
            if (in_array(self::CACHE_TYPE_APC, $this->config['type'])) {            
                return $this->getAPC();
            } elseif (in_array(self::CACHE_TYPE_MEMCACHE, $this->config['type'])) {
                return $this->getMemcache();
            } elseif (in_array(self::CACHE_TYPE_FILE, $this->config['type'])) {
                return $this->getFilecache();
            }
             else {
                return false;
            }
        }
    }
    
    /**
     * Get cache object by name
     * 
     * @author Trung Nguyen
     * @param  string $cache_type
     * @return Object
     */
    public function getInstanceByName($cache_type = self::CACHE_TYPE_MEMCACHE)
    {
        switch (strtolower($cache_type)) {
            case self::CACHE_TYPE_APC:
                return $this->getAPC();
                break;
                
            case self::CACHE_TYPE_MEMCACHE:
                return $this->getMemcache();
                break;
                
            case self::CACHE_TYPE_FILE:
                return $this->getFilecache();
                break;
            default:
                return null;
        }
    }

    /**
     * Add prefix used to generate cache name
     * 
     * @author Trung Nguyen
     * @param  Request $request
     */
    public function setPrefix($env = '') 
    {
        $this->prefix = $env;
    }
    
    /**
     * Set the cache type will be returned by [getDefault] method
     *
     * @author Trung Nguyen
     * @param  Request $request
     */
    public function setDefaultCache($cache_type)
    {
        $this->defaultCacheType = $cache_type;
    }
}
