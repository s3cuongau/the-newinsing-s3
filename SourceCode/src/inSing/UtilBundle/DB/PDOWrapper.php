<?php

namespace inSing\UtilBundle\DB;

use \PDO;

/**
 * A PDO Wrapper Class
 * It also support Master/Slaves connections.
 * 
 * @author Trung Nguyen
 */
class PDOWrapper 
{
    /**
     * var array
     */
    private $connectionPool;
    
    /**
     * @var int
     */
    private $totalSlave;
    
	/**
	 * @var string
	 */
	private $sql;
	
	/**
	 * @var array
	 */
	private $config;

	/**
	 * @var inSing\Util\Logger 
	 */
	private $logger;
	
	/**
	 * Constructor
	 * 
	 * @param array $configs
	 * @param Logger $logger
	 */
	public function __construct(array $config, $logger = null) 
	{
	    // check if it support master/slave connection
	    $this->totalSlave = 0;
	    
	    while (isset($config['slave'.($this->totalSlave+ 1)])) {
	        $this->totalSlave++;
	    }
	    
	    // force to use master in case of having no slave 
	    if ($this->totalSlave == 0) {
	        $config['slave1'] = $config['master'];
	        $this->totalSlave++;
	    }
	    
	    $this->config = $config;
	    
	    if (!is_null($logger)) {
            $this->logger = $logger;
	    }
	}

	
	
	/**
	 * [logger] setter
	 * 
	 * @param inSing\Util\Logger $logger
	 */
	public function setLogger($logger) 
	{
	    $this->logger = $logger;
	}
	
	/**
	 * Delete command
	 * 
	 * @param  string $table
	 * @param  string $where
	 * @param  array $bind
	 * @return int
	 */
	public function delete($table, $where, $bind = '') 
	{
		$sql = "DELETE FROM $table WHERE $where;";
		return $this->run($sql, $bind);
	}

	/**
	 * Insert command
	 *
	 * @param  string $table
	 * @param  array $info
	 * @return int
	 */
	public function insert($table, $info)
	{	    
	    // generate query string
	    $trans = array(
	        '[TABLE]'        => $table,
	        '[COLUMNS]'      => implode(',', $this->getColumns($info)),
	        '[PLACEHOLDERS]' => implode(',', $this->getPlaceHolders($info)),
	    );

	    $this->sql = strtr('INSERT INTO [TABLE] ([COLUMNS]) VALUES ([PLACEHOLDERS])', $trans);	    
	    
	    return $this->run($this->sql, $this->getBindValues($info));
	}
	
	/**
	 * Update command
	 *
	 * @param string $table
	 * @param array  $info
	 * @param string $where
	 * @param array  $bind
	 * @return Ambigous <\inSing\Util\multitype:, number, boolean, multitype:>
	 */
	public function update($table, $info, $where, $bind = '')
	{
	    // generate update fields
	    $values = array();	    
	    foreach ($this->getColumns($info) as $field) {
	        $values[] = "$field = :$field";	        
	    }
	    
	    // generate query string
	    $trans = array(
    	    '[TABLE]'  => $table,
    	    '[VALUES]' => implode(',', $values),
    	    '[WHERE]'  => $where,
	    );
	    
	    // process param binding
	    $bind = $this->cleanup($bind);
	    foreach ($info as $key => $value) {
	        $bind["$key"] = $value;
	    }
	     
	    $this->sql = strtr('UPDATE [TABLE] SET [VALUES] WHERE [WHERE]', $trans);
        
	    return $this->run($this->sql, $bind);
	}
	
	/**
	 *
	 * @param unknown $table
	 * @param string $where
	 * @param string $bind
	 * @param string $fields
	 * @return Ambigous <\inSing\Util\multitype:, number, boolean, multitype:>
	 */
	public function select($table, $where = '', $bind = '', $fields = '*')
	{
	    $sql = "SELECT $fields FROM $table";
	
	    if (!empty($where)) {
	        $sql .= " WHERE $where";
	    }
	
	    $sql .= ";";
	
	    return $this->run($sql, $bind);
	}
	
	/**
	 *
	 * @param unknown $table
	 * @param string $where
	 * @param string $bind
	 * @return boolean
	 */
	public function count($table, $where = '', $bind = '')
	{
	    try {
	        $rs = $this->select($table, $where, $bind, 'COUNT(*) AS cnt');
	        return $rs[0]['cnt'];
	    } catch (\PDOException $e) {
	        $this->logger->addErrorToFile($e->getMessage());
	        throw $e;
	    }
	}
	
	/**
	 * Get last insert ID
	 *
	 * @return int
	 */
	public function getLastInsertID()
	{
	    try {
	        $conn = $this->getConnection('master');
	        return $conn->lastInsertId();
	    } catch (\PDOException $e) {
	        $this->logger->addErrorToFile($e->getMessage());
	        throw $e;
	    }
	}
	
	/**
	 * Generate the columns list in string based on params
	 * 
	 * @param  array $info
	 * @return array
	 */
	protected function getColumns(array $info)
	{
	    return is_array($info) ? array_keys($info) : array();
	}
	
	/**
	 * Generate the placeholder list in string based on params
	 *
	 * @param  array $info
	 * @return array
	 */
	protected function getPlaceHolders(array $info)
	{
	    $ret = array();
	    
	    if (is_array($info)) {
    	    foreach ($info as $key => $val) {
    	        $ret[] = ":$key";
    	    }
	    }
	    
	    return $ret;
	}
	
	/**
	 * Generate the binds param list
	 *
	 * @param  array $info
	 * @return array
	 */
	protected function getBindValues(array $info)
	{
	    $ret = array();
	     
	    if (is_array($info)) {
	        foreach ($info as $key => $val) {
	            $ret[":$key"] = $val;
	        }
	    }
	     
	    return $ret;
	}
	
	/**
	 * Convert binding params to array
	 * 
	 * @param  mixed $bind
	 * @return array
	 */
	private function cleanup($bind) 
	{
		if (!is_array($bind)) {
			$bind = !empty($bind) ? array($bind) : array();
		}
		
		return $bind;
	}
	
	/**
	 * Excute SQL
	 * 
	 * @param  string $sql
	 * @param  string $bind
	 * @return multitype:|number|boolean
	 * @throws \PDOException
	 */
	public function run($sql, $bind = '')
	{
	    try {
	        // add to log for debug
	        if ($this->config['debug_mode']) {
	            $this->logger->addInfoToFile(sprintf('%s - %s', $this->getCName($sql), $this->buildSQL($sql, $bind)));
	        }
	        
	        $conn = $this->getConnection($this->getCName($sql));
	        $stmt = $conn->prepare(trim($sql));
	        	
	        if ($stmt->execute($this->cleanup($bind)) !== false) {
	            if (preg_match("/^(" . implode("|", array("select", "describe", "pragma")) . ") /i", $sql)) {
	                return $stmt->fetchAll(PDO::FETCH_ASSOC);
	            } elseif (preg_match("/^(" . implode("|", array("delete", "insert", "update", "replace")) . ") /i", $sql)) {
	                return $stmt->rowCount();
	            }
	        }
	    } catch (\PDOException $e) {
	        $this->logger->addErrorToFile($e->getMessage());
	        throw $e;
	    }
	}
	
	/**
	 * Instance connection 
	 * 
	 * @author Trung Nguyen
	 * @param  string $type - Read/write
	 * @return PDO
	 * @throws \PDOException
	 */
	private function getConnection($name)
	{
	    if (!isset($this->connectionPool[$name]) ||
    	    !($this->connectionPool[$name] instanceof PDO) ||
    	    !($isConnected = $this->isConnected($this->connectionPool[$name]))) {
	        
	        try {
    	        $trans = array(
        	        '[HOST]'    => $this->config[$name]['host'],
        	        '[PORT]'    => $this->config[$name]['port'],
        	        '[DB_NAME]' => $this->config[$name]['dbname']
    	        );
    	        
	            $dsn = strtr('mysql:host=[HOST];port=[PORT];dbname=[DB_NAME]', $trans);
	             
	            $options = array(
    	            PDO::ATTR_PERSISTENT => true,
    	            PDO::ATTR_ERRMODE    => PDO::ERRMODE_EXCEPTION
	            );
	             
	            $this->connectionPool[$name] = new PDO($dsn, $this->config[$name]['user'], $this->config[$name]['password'], $options);
	
	            if (isset($isConnected) && !$isConnected) {
	                $this->logger->addInfoToFile("Renew connection [$name] successfully");
	            }
	            
	        } catch (\PDOException $e) {
	            $this->logger->addErrorToFile($e->getMessage());
	            throw $e;
	        }
	    }
	     
	    return $this->connectionPool[$name];
	}
	
	/**
	 * Check if connection is still available
	 *
	 * @author Trung Nguyen
	 * @return boolean
	 */
	private function isConnected($conn)
	{
	    $ret = true;
	    
	    try {
	        $conn->query('SELECT 1');
	    } catch (\PDOException $e) {
	        $ret = false;
	    }
	    
	    return $ret;
	}
	
	/**
	 * Get connection name based on query string
	 * 
	 * @author Trung Nguyen
	 * @param  string $sql
	 * @return string - Master/Slave[n]
	 */
	private function getCName($sql)
	{
	    $connName = 'master';
	    
	    // get connection type (read|write)
	    if (preg_match("/^(" . implode("|", array("select", "describe", "pragma")) . ") /i", $sql)) {
	        $connName = 'slave'.rand(1, $this->totalSlave);
	    } elseif (preg_match("/^(" . implode("|", array("delete", "insert", "update", "replace")) . ") /i", $sql)) {
	        $connName = 'master';
	    }
	    
	    return $connName;
	}
	
	/**
	 * Generate full SQL by combining SQL Placeholders and Binding Params
	 * 
	 * @author Trung Nguyen
	 * @param  string $sql
	 * @param  array  $bind
	 * @return string
	 */
	private function buildSQL($sql, $bind)
	{
	    $trans = array();
	    
	    if (is_array($bind)) {
    	    foreach ($bind as $key => $val) {
    	        $trans[":{$key}"] = $val;
    	    }
	    }
	    	    
	    return strtr($sql, $trans);
	}
}	
