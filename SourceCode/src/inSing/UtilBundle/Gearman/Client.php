<?php

namespace inSing\UtilBundle\Gearman;

use \GearmanClient;

/**
 * A Wrapper Class For Gearman Client
 * 
 * @author Trung Nguyen
 */
class Client
{
    /**
     * 
     * @var GearmanClient
     */
    private $client = null;
    
    /**
     * Constructor
     *
     * @author Trung Nguyen
     * @param  array $configs
     * @throws \Exception
     */
    public function __construct(array $configs)
    {
        try {
            $this->client = new GearmanClient();
            $this->client->setTimeout($configs['timeout']*1000);
            $this->client->addServers(implode(',', $configs['server']));
        } catch (\Exception $e) {
            throw $e;
        }
    }
    
    
    /**
     * Run a task in background
     * 
     * @param  string $task_name
     * @param  string $workload - JSON data
     * @return int
     * @throws \Exception
     */
    public function doBackground($task_name, $workload) 
    {
        try {
            $jobHandle = $this->client->doBackground($task_name, $workload);
            
            if ($this->client->returnCode() != GEARMAN_SUCCESS) {
                throw new \Exception($this->client->error());
            }
            
            return $jobHandle;
            
        } catch (\Exception $e) {
            throw $e;
        }
    }
    
    /**
     * Run a task and return result 
     *
     * @param  string $task_name
     * @param  string $workload - JSON data
     * @return mixed
     * @throws \Exception
     */
    public function doNormal($task_name, $workload)
    {
        try {
            return $this->client->doNormal($task_name, $workload);
        } catch (\Exception $e) {
            throw $e;
        }
    }
    
}
