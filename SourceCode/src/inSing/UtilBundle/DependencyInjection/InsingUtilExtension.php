<?php

namespace inSing\UtilBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 * 
 * @author Trung Nguyen
 */
class InsingUtilExtension extends Extension
{
    /**
     * {@inheritDoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        // initialize loader
        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);
        
        // configure Cache lib.
        if (isset($config['cache'])) {            
            $container->setParameter('insing_util.cache', $config['cache']);
            
            // load cache service
            $loader->load('cache.yml');
        }        
        
        // configure Logger lib.
        if (isset($config['log'])) {                                    
            $container->setParameter('insing_util.log', $config['log']);            
        
            // load log service
            $loader->load('log.yml');
        }
        
        // configure Gearman lib.
        if (isset($config['gearman'])) { 
            $container->setParameter('insing_util.gearman', $config['gearman']);
        
            // load log service
            $loader->load('gearman.yml');
        }
        
        // configure PDO Wrapper lib.
        if (isset($config['db'])) {
            if (count($config['db']['slave1']) == 0) {
                unset($config['db']['slave1']);
            }
            
            if (count($config['db']['slave2']) == 0) {
                unset($config['db']['slave2']);
            }
            
            if (count($config['db']['slave3']) == 0) {
                unset($config['db']['slave3']);
            }
            
            $container->setParameter('insing_util.db', $config['db']);
        
            // load log service
            $loader->load('db.yml');
        }
        
        // configure Mailer lib.
        if (isset($config['mailer'])) {
            $container->setParameter('insing_util.mailer', $config['cache']);
        
            // load cache service
            $loader->load('mailer.yml');
        }
        
    }
}
