<?php

/**
 * This class contains some functions which use for querying db
*  Connect Session Token Table 
* @author Khuong Dang
*/

namespace inSing\ApiBundle\Model;

use Doctrine\ORM\Query;
use inSing\ApiBundle\Model\Base\NewinSingTable;

class MoviesTable extends NewinSingTable {
    public function getMoviesContent(){
        $result = array();
        $query = "SELECT * FROM `pulled_movie`";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        $result = $stmt->fetchAll(Query::HYDRATE_ARRAY);
        return $result;

    }
}
