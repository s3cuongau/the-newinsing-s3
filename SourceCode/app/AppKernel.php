<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = array(
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Symfony\Bundle\AsseticBundle\AsseticBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),
            new inSing\UtilBundle\InsingUtilBundle(),
            new inSing\DataSourceBundle\inSingDataSourceBundle(),
            new Knp\Bundle\PaginatorBundle\KnpPaginatorBundle()
        );
        if (in_array($this->getEnvironment(), array('dev', 'test', 'frontend_dev', 'frontend_test', 'admin_dev', 'admin_test', 'api_dev', 'api_test'))) {
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
            $bundles[] = new Symfony\Bundle\DebugBundle\DebugBundle();
        }
        if (in_array($this->getEnvironment(), array('admin_dev', 'admin_test', 'admin_prod'))) {
            $bundles[] = new inSing\AdminBundle\inSingAdminBundle();
            $bundles[] = new inSing\ApiAdapterBundle\inSingApiAdapterBundle();
        }
        if (in_array($this->getEnvironment(), array('frontend_prod', 'frontend_dev'))) {
            $bundles[] = new inSing\FrontendBundle\inSingFrontendBundle();
            $bundles[] = new inSing\ApiAdapterBundle\inSingApiAdapterBundle();
            $bundles[] = new WhiteOctober\BreadcrumbsBundle\WhiteOctoberBreadcrumbsBundle();
        }

        if (in_array($this->getEnvironment(), array('api_dev', 'api_test', 'api_prod'))) {
            $bundles[] = new FOS\RestBundle\FOSRestBundle();
            $bundles[] = new JMS\SerializerBundle\JMSSerializerBundle($this);
            $bundles[] = new API\CoreBundle\APICoreBundle();
            $bundles[] = new API\ApiBundle\APIApiBundle();
            $bundles[] = new inSing\ApiAdapterBundle\inSingApiAdapterBundle();
            $bundles[] = new inSing\ApiBundle\inSingApiBundle();
        }

        return $bundles;
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $xp = explode('_', $this->getEnvironment());

        if (isset($xp) && count($xp) == 1)
        {
            $loader->load(__DIR__.'/config/config.yml');
        }
        else
        {
            $loader->load(__DIR__ . "/config/{$xp[0]}/config_{$xp[1]}.yml");
        }
    }
}
