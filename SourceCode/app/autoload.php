<?php

use Doctrine\Common\Annotations\AnnotationRegistry;
use Composer\Autoload\ClassLoader;

/**
 * @var ClassLoader $loader
 */
$loader = require __DIR__.'/../vendor/autoload.php';

// register API namespace
$loader->add('API', __DIR__ . '/../src');
$loader->add('Aws', __DIR__.'/../vendor/aws-sdk-php/src');
$loader->add('Guzzle', __DIR__.'/../vendor/guzzle/src');
$loader->add('WhiteOctober', __DIR__.'/../vendor/whiteoctober/breadcrumbs-bundle');

AnnotationRegistry::registerLoader(array($loader, 'loadClass'));

return $loader;